package reports;

public class BReportException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 281775618866613882L;

	public BReportException() {
		super();
	}

	public BReportException(String message, Throwable cause) {
		super(message, cause);
	}

	public BReportException(String message) {
		super(message);
	}

	public BReportException(Throwable cause) {
		super(cause);
	}

}
