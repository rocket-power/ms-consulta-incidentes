package com.promad.repository.dto;

public class ParametroDTO {

	String nombreParametro;
	String tipo;
	Object valor;

	public ParametroDTO(String nombreParametro, Object valor) {
		super();
		this.nombreParametro = nombreParametro;
		this.valor = valor;
	}

	public ParametroDTO(String nombreParametro, String tipo, Object valor) {
		super();
		this.nombreParametro = nombreParametro;
		this.valor = valor;
		this.tipo = tipo;
	}

	public ParametroDTO() {
	}

	public String getNombreParametro() {
		return nombreParametro;
	}

	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "ParametrosDto [nombreParametro=" + nombreParametro + ", tipo=" + tipo + ", valor=" + valor + "]";
	}
}
