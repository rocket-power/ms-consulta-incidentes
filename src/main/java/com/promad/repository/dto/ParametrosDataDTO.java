package com.promad.repository.dto;

import java.util.List;

public class ParametrosDataDTO {
	String nombrePaquete;
	String nombreStoreProcedure;
	String nombreMs;
	String tipo = "";
	List<ParametroDTO> param;

	public String getNombreMs() {
		return nombreMs;
	}

	public void setNombreMs(String nombreMs) {
		this.nombreMs = nombreMs;
	}

	public String getNombrePaquete() {
		return nombrePaquete;
	}

	public void setNombrePaquete(String nombrePaquete) {
		this.nombrePaquete = nombrePaquete;
	}

	public String getNombreStoreProcedure() {
		return nombreStoreProcedure;
	}

	public void setNombreStoreProcedure(String nombreStoreProcedure) {
		this.nombreStoreProcedure = nombreStoreProcedure;
	}

	public List<ParametroDTO> getParam() {
		return param;
	}

	public void setParam(List<ParametroDTO> param) {
		this.param = param;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "ParametrosDataDto [nombrePaquete=" + nombrePaquete + ", nombreStoreProcedure=" + nombreStoreProcedure
				+ ", NombreMS=" + getNombreMs() + ", param=" + param + ", getNombrePaquete()=" + getNombrePaquete()
				+ ", getNombreStoreProcedure()=" + getNombreStoreProcedure() + ", getParam()=" + getParam().toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
