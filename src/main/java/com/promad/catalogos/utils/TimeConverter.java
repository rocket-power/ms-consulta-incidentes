package com.promad.catalogos.utils;

public class TimeConverter {
	
	public static String segundosAHoras( Double tiempo ) {
		int segundos = 0;
		int minutos = 0;
		int horas = 0;
		
		if( tiempo != null ) {
			segundos = tiempo.intValue();
		}
		
		horas = segundos / 3600;
		minutos = ( segundos / 60 ) % 60;
		segundos = segundos % 60;
		return format( horas ) + ":" + format( minutos ) + ":" + format( segundos );
	}
	
	public static String segundosAHoras( String tiempo ) {
		if( tiempo != null ) {
			try {
				return segundosAHoras( Double.valueOf( tiempo.trim() ) );
			} catch (Exception e) {
				
			}
		}
		return segundosAHoras( 0d );
	}
	
	private static String format( int x ) {
		if( x < 10 ) {
			return "0" + x;
		}
		return "" + x;
	}
	
	public static void main( String args[]) {
		String tiempo = "164359.99";
		System.out.println(TimeConverter.segundosAHoras(tiempo ));
	}
}
