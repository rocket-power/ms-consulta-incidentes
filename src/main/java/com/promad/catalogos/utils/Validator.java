package com.promad.catalogos.utils;

/**
 * Factory
 */

//Lo que sea
public class Validator {
	
	private Validator() {}
	
	public static StringValidator validate( String val ) {
		return new StringValidator( val );
	}
	
	public static IntValidator validate( Integer val ) {
		return new IntValidator( val );
	}
	
	public static  DoubleValidator validate(Double val) {
		return new DoubleValidator (val);
	}
}
