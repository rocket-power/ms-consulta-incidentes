package com.promad.catalogos.utils;

import com.promad.catalogos.exception.CatalogoException;

public class IntValidator {
	
	private Integer val;
	
	/**
	 * Constructor
	 * @param val
	 */
	public IntValidator(Integer val) {
		this.val = val;
	}

	public IntValidator minimo(String message) {
		if(val == null || val <= 0) {
			throw new CatalogoException(message);
		}
		return this;
	}

	public IntValidator isnull(String message) {
		if(val == null) {
			throw new CatalogoException(message);
		}
		return this;
	}

}
