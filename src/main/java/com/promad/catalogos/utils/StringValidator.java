package com.promad.catalogos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.promad.catalogos.exception.CatalogoException;

public class StringValidator {
	private String val;

	/**
	 * Constructor
	 * @param val
	 */
	
	//Lo que sea
	public StringValidator(String val) {
		this.val = val;
	}

	public StringValidator isNull(String message) {
		if (val == null) {
			throw new CatalogoException(message);
		}
		return this;
	}
	public StringValidator isEmpty(String message) {
		if ( val == null || val.isEmpty() ) {
			throw new CatalogoException(message);
		}
		return this;
	}

	public StringValidator isEspecial(String message) {
		if(val == null || !val.matches("^[a-zA-Z0-9 \\u00e1\\u00e9\\u00ed\\u00f3\\u00fa\\u00c1\\u00c9\\u00cd\\u00d3\\u00da\\u00f1\\u00d1]*$")) {
			throw new CatalogoException (message);
		}
		return this;
	}
	public StringValidator isEmail(String message) {
		Pattern p = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		Matcher comprobacion = p.matcher(val);
		if (val == null || comprobacion.find() == false) {
			throw new CatalogoException(message);
		}
		return this;
	}
	//Lo que sea por 2

	public StringValidator minLength( int length, String message ) {
		if(val != null && val.length() < length ) {
			throw new CatalogoException (message);
		}
		return this;
	}
	
	public StringValidator maxLength( int length, String message ) {
		if(val != null && val.length() > length ) {
			throw new CatalogoException (message);
		}
		return this;
	}
	
	public StringValidator isNumeric(String message) {
		Pattern p = Pattern.compile("[a-z A-Z]");
		Matcher comprobacion = p.matcher(val);
		if(val != null && comprobacion.find() == false) {
			throw new CatalogoException(message);
		}
		return this;
	}
	
}