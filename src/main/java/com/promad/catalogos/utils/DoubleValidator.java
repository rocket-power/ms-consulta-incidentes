package com.promad.catalogos.utils;

import com.promad.catalogos.exception.CatalogoException;

public class DoubleValidator {
	private Double val;
	/**
	 * Constructor
	 * @param val2
	 */

	public DoubleValidator(Double val) {
             this.val = val;
	
	}

	public DoubleValidator minimo(String message) {
		if (val == null || val <= 0) {
			throw new CatalogoException(message);
		}
		return this;
	}
	
	public DoubleValidator isnull(String message) {
		if (val == null) {
			throw new CatalogoException(message);
		}
		return this;
	}
	
}
