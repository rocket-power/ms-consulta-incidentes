package com.promad.catalogos.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Base64;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FTPUploader {
	static Logger logger = LoggerFactory.getLogger(FTPUploader.class);
	
	private static FTPClient ftp = null;
	
	public static FTPClient connectFTP(String url, String user, String pwd) {
		try {
			ftp = new FTPClient();
			ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
			
			int reply;
			ftp.connect(InetAddress.getByName(url));
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				throw new RuntimeException( "Exception in connecting to FTP Server" );
			}
			ftp.login(user, pwd);
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();
			
			return ftp;
		}
		catch (Exception e) {
			throw new RuntimeException( "Exception in connecting to FTP Server", e );
		}
	}
	
	public static boolean uploadFile(String data, String fileName, String hostDir){
		byte[] buffer = Base64.getDecoder().decode(data);
		try(InputStream input = new ByteArrayInputStream(buffer))
		{
			if( ftp == null ) {
				return false;
			}
			boolean created = ftp.makeDirectory(File.separator + hostDir); 
			if(created) {
			}
			ftp.storeFile(File.separator + hostDir + File.separator + fileName, input);
			return true;
		}
		catch (Exception e) {
			logger.error("Error: ", e);
			return false;
		}
	}
	
	public static boolean deleteFile(String path)
	{
		try
		{
			if( ftp == null ) {
				return false;
			}
			if(path.isEmpty() || path == null)
			{
				return false;
			}
			if(ftp.deleteFile(path))
			{
				return true;
			}
			return true;
		}
		catch (Exception e) 
		{
			logger.error("Error: ", e);
			return false;
		}
	}

	public static boolean downloadFile(String rutaDestino, String path)
	{
		try
		{
			if( ftp == null ) {
				return false;
			}
			if(path.isEmpty() || path == null)
			{
				return false;
			}
			FileOutputStream fos = null; 
			fos = new FileOutputStream(rutaDestino);  
			fos.close();
			return true;
		}
		catch (Exception e) 
		{
			logger.error("Error: ", e);
			return false;
		}
	}
	
	
	
	public static void disconnect(){
		if ( ftp.isConnected()) {
			try {
				ftp.logout();
				ftp.disconnect();
				ftp = null;
			} catch (IOException e) {
				logger.error("Error: ", e);
				}
		}
	}
}