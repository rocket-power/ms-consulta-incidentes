package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class EstatusRastreoDTO extends BaseDTO {

	@Id
	@Element("id_estatus_rastreo")
	private Integer idEstatusRastreo;
	@Element("nombre_estatus_rastreo")
	private String nombreEstatusRastreo;

	public Integer getIdEstatusRastreo() {
		return idEstatusRastreo;
	}

	public void setIdEstatusRastreo(Integer idEstatusRastreo) {
		this.idEstatusRastreo = idEstatusRastreo;
	}

	public String getNombreEstatusRastreo() {
		return nombreEstatusRastreo;
	}

	public void setNombreEstatusRastreo(String nombreEstatusRastreo) {
		this.nombreEstatusRastreo = nombreEstatusRastreo;
	}

}
