package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ObjetosInvolucradosDTO {

	@Element("revision")
	private String revision;

	@Id
	@Element("id_objeto_involucrado")
	private Integer idObjetoInvolucrado;

	@Id
	@Element("id_objeto")
	private Integer idObjeto;
	@Element("nombre_objeto")
	private String nombreObjeto;

	@Id
	@Element("id_categoria")
	private Integer idCategoria;
	@Element("nombre_categoria")
	private String nombreCategoria;

	@Element("folio")
	private String folio;
	@Element("nombre_motivo")
	private String motivo;

	@Element("cantidad")
	private Integer cantidad;
	@Element("valor_estimado")
	private Integer estimado;
	@Element("observaciones")
	private String descripcion;

	@Element("id_tipo_moneda")
	private String idTipoMoneda;
	@Element("descripcion")
	private String descripciones;

	@Id
	@Element("id_evento")
	private Integer idEvento;
	@Element("uuid")
	private String uuid;
	@Element("creado_por")
	private Integer creadoPor;
	@Element("modificado_por")
	private Integer modificadoPor;

	public Integer getIdObjetoInvolucrado() {
		return idObjetoInvolucrado;
	}

	public void setIdObjetoInvolucrado(Integer idObjetoInvolucrado) {
		this.idObjetoInvolucrado = idObjetoInvolucrado;
	}

	public Integer getIdObjeto() {
		return idObjeto;
	}

	public void setIdObjeto(Integer idObjeto) {
		this.idObjeto = idObjeto;
	}

	public String getNombreObjeto() {
		return nombreObjeto;
	}

	public void setNombreObjeto(String nombreObjeto) {
		this.nombreObjeto = nombreObjeto;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getEstimado() {
		return estimado;
	}

	public void setEstimado(Integer estimado) {
		this.estimado = estimado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getIdTipoMoneda() {
		return idTipoMoneda;
	}

	public void setIdTipoMoneda(String idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}

	public String getDescripciones() {
		return descripciones;
	}

	public void setDescripciones(String descripciones) {
		this.descripciones = descripciones;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

}
