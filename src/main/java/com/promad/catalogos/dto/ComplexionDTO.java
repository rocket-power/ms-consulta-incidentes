package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ComplexionDTO {

	@Id
	@Element("id_complexion")
	private Integer idComplexion;
	@Element("complexion")
	private String complexion;
	private String uuid;
	private Integer modificadoPor;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getComplexion() {
		return complexion;
	}

	public void setComplexion(String complexion) {
		this.complexion = complexion;
	}

	public Integer getIdComplexion() {
		return idComplexion;
	}

	public void setIdComplexion(Integer idComplexion) {
		this.idComplexion = idComplexion;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
