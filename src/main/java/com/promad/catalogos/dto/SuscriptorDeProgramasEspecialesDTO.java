package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class SuscriptorDeProgramasEspecialesDTO extends BaseDTO {

	@Element("nombre_programa_seguridad")
	private String nombreProgramaSeguridad;
	@Element("nombre")
	private String nombre;
	@Element("paterno")
	private String apellidoPaterno;
	@Element("materno")
	private String apellidoMaterno;
	@Element("n_encargado")
	private String nombreEncargado;
	@Element("p_encargado")
	private String apellidoPaternoEncargado;
	@Element("m_encargado")
	private String apellidoMaternoEncargado;
	@Element("fecha_suscripcion")
	private String fechaSuscripcion;
	@Element("telefono")
	private String telefono;
	@Element("calle")
	private String calle;
	@Element("numero")
	private String numero;
	@Element("nombre_colonia")
	private String nombreColonia;
	@Element("NO_SUSCRIPTOR")
	private Integer numeroSuscriptor;
	private String nombreCompleto;
	private String nombreCompletoEncargado;

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getNombreProgramaSeguridad() {
		return nombreProgramaSeguridad;
	}

	public void setNombreProgramaSeguridad(String nombreProgramaSeguridad) {
		this.nombreProgramaSeguridad = nombreProgramaSeguridad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombreEncargado() {
		return nombreEncargado;
	}

	public void setNombreEncargado(String nombreEncargado) {
		this.nombreEncargado = nombreEncargado;
	}

	public String getApellidoPaternoEncargado() {
		return apellidoPaternoEncargado;
	}

	public void setApellidoPaternoEncargado(String apellidoPaternoEncargado) {
		this.apellidoPaternoEncargado = apellidoPaternoEncargado;
	}

	public String getApellidoMaternoEncargado() {
		return apellidoMaternoEncargado;
	}

	public void setApellidoMaternoEncargado(String apellidoMaternoEncargado) {
		this.apellidoMaternoEncargado = apellidoMaternoEncargado;
	}

	public String getFechaSuscripcion() {
		return fechaSuscripcion;
	}

	public void setFechaSuscripcion(String fechaSuscripcion) {
		this.fechaSuscripcion = fechaSuscripcion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	public Integer getNumeroSuscriptor() {
		return numeroSuscriptor;
	}

	public void setNumeroSuscriptor(Integer numeroSuscriptor) {
		this.numeroSuscriptor = numeroSuscriptor;
	}

	public String getNombreCompletoEncargado() {
		return nombreCompletoEncargado;
	}

	public void setNombreCompletoEncargado(String nombreCompletoEncargado) {
		this.nombreCompletoEncargado = nombreCompletoEncargado;
	}

}
