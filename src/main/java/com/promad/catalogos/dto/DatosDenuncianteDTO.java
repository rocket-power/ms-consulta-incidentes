package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class DatosDenuncianteDTO extends BaseDTO {

	@Element("id_contacto")
	private Integer idContacto;
	@Element("nombre")
	private String nombre;
	@Element("paterno")
	private String paterno;
	@Element("materno")
	private String materno;
	@Element("telefono")
	private String telefono;
	@Element("id_tipo_denunciante")
	private Integer idTipoDenunciante;
	@Element("tipo_denunciante")
	private String tipoDenunciante;
	@Element("id_estado")
	private Integer idEstado;
	@Element("nombre_estado")
	private String nombreEstado;
	@Element("id_municipio")
	private Integer idMunicipio;
	@Element("nombre_municipio")
	private String nombreMunicipio;
	@Element("id_localidad")
	private Integer idLocalidad;
	@Element("nombre_localidad")
	private String nombreLocalidad;
	@Element("id_colonia")
	private Integer idColonia;
	@Element("nombre_colonia")
	private String nombreColonia;
	@Element("id_direccion")
	private Integer idDireccion;
	@Element("calle")
	private String calle;
	@Element("numero")
	private String numero;
	@Element("latitud")
	private String latitud;
	@Element("longitud")
	private String longitud;
	@Element("codigo_postal")
	private String codigoPostal;
	@Element("edificio")
	private String edificio;
	@Element("piso")
	private String piso;
	@Element("entre_calle_1")
	private String entreCalle1;
	@Element("entre_calle_2")
	private String entreCalle2;
	@Element("id_punto_interes")
	private Integer idPuntoInteres;
	@Element("nombre_pi")
	private String nombrePuntoInteres;
	@Element("edad")
	private String edad;
	@Element("fecha_insercion")
	private String fechaInsercion;
	@Element("id_sexo")
	private Integer idSexo;
	@Element("sexo")
	private String sexo;
	@Element("id_tipo_telefono")
	private Integer idTipoTelefono;
	@Element("tipo_telefono")
	private String tipoTelefono;
	@Element("numero_interior")
	private String numeroInterior;

	private String direccionCompleta;

	public Integer getIdContacto() {
		return idContacto;
	}

	public void setIdContacto(Integer idContacto) {
		this.idContacto = idContacto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getNombreMunicipio() {
		return nombreMunicipio;
	}

	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getNombreLocalidad() {
		return nombreLocalidad;
	}

	public void setNombreLocalidad(String nombreLocalidad) {
		this.nombreLocalidad = nombreLocalidad;
	}

	public Integer getIdColonia() {
		return idColonia;
	}

	public void setIdColonia(Integer idColonia) {
		this.idColonia = idColonia;
	}

	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	public Integer getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(Integer idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getEdificio() {
		return edificio;
	}

	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getEntreCalle1() {
		return entreCalle1;
	}

	public void setEntreCalle1(String entreCalle1) {
		this.entreCalle1 = entreCalle1;
	}

	public String getEntreCalle2() {
		return entreCalle2;
	}

	public void setEntreCalle2(String entreCalle2) {
		this.entreCalle2 = entreCalle2;
	}

	public Integer getIdPuntoInteres() {
		return idPuntoInteres;
	}

	public void setIdPuntoInteres(Integer idPuntoInteres) {
		this.idPuntoInteres = idPuntoInteres;
	}

	public String getNombrePuntoInteres() {
		return nombrePuntoInteres;
	}

	public void setNombrePuntoInteres(String nombrePuntoInteres) {
		this.nombrePuntoInteres = nombrePuntoInteres;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getFechaInsercion() {
		return fechaInsercion;
	}

	public void setFechaInsercion(String fechaInsercion) {
		this.fechaInsercion = fechaInsercion;
	}

	public Integer getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getIdTipoTelefono() {
		return idTipoTelefono;
	}

	public void setIdTipoTelefono(Integer idTipoTelefono) {
		this.idTipoTelefono = idTipoTelefono;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}

	private String idEvento2;

	public String getIdEvento2() {
		return idEvento2;
	}

	public void setIdEvento2(String idEvento2) {
		this.idEvento2 = idEvento2;
	}

	public String getDireccionCompleta() {
		return direccionCompleta;
	}

	public void setDireccionCompleta(String direccionCompleta) {
		this.direccionCompleta = direccionCompleta;
	}

	public Integer getIdTipoDenunciante() {
		return idTipoDenunciante;
	}

	public void setIdTipoDenunciante(Integer idTipoDenunciante) {
		this.idTipoDenunciante = idTipoDenunciante;
	}

	public String getTipoDenunciante() {
		return tipoDenunciante;
	}

	public void setTipoDenunciante(String tipoDenunciante) {
		this.tipoDenunciante = tipoDenunciante;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

}
