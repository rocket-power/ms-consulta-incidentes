package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ObjetosDTO extends BaseDTO {
	@Id
	@Element("id_objeto")
	private Integer idObjeto;
	@Element("nombre_objeto")
	private String nombreObjeto;

	@Id
	@Element("id_categoria_objeto")
	private Integer idCategoria;
	@Element("nombre_categoria")
	private String nombreCategoria;
	@Id
	@Element("id_tipo_objeto")
	private Integer idTipoObjeto;
	@Element("nombre_tipo")
	private String nombreTipo;

	public Integer getIdObjeto() {
		return idObjeto;
	}

	public void setIdObjeto(Integer idObjeto) {
		this.idObjeto = idObjeto;
	}

	public String getNombreObjeto() {
		return nombreObjeto;
	}

	public void setNombreObjeto(String nombreObjeto) {
		this.nombreObjeto = nombreObjeto;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public Integer getIdTipoObjeto() {
		return idTipoObjeto;
	}

	public void setIdTipoObjeto(Integer idTipoObjeto) {
		this.idTipoObjeto = idTipoObjeto;
	}

	public String getNombreTipo() {
		return nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

}
