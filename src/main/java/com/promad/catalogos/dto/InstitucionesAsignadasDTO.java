package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class InstitucionesAsignadasDTO {
	@Id
	@Element("id_evento")
	private Integer idEvento;
	@Element("uuid")
	private String uuid;
	@Element("creado_por")
	private Integer creadoPor;
	@Element("modificado_por")
	private Integer modificadoPor;

	@Id
	@Element("id_inst_asignada")
	private Integer idInstitucionAsignada;
	@Element("id_institucion")
	private Integer idInstitucion;
	@Element("nombre_institucion")
	private String nombreInstitucion;
	@Element("url_imagen")
	private String urlImegen;
	@Element("id_tipo_cierre")
	private Integer idTipoCierre;
	@Element("nombre_tipo")
	private String nombreTipo;
	@Element("descripcion_cierre")
	private String descripcionCierre;

	@Element("folio")
	private String folio;

	private String idInstitucionS;

	@Element("fecha_transmision")
	private String fechaTransmision;
	@Element("fecha_razonamiento")
	private String fechaRazonamiento;
	@Element("detenidos")
	private Integer detenidos;
	@Element("id_razonamiento")
	private Integer idRazonamiento;
	@Element("razonamiento")
	private String razonamiento;
	@Element("id_motivo_ro")
	private Integer idMotivoRadioOperador;
	@Element("motivo_ro")
	private String motivoRadioOperador;
	@Element("id_ro")
	private Integer idRadioOperador;
	@Element("nombre_ro")
	private String nombreRadioOperador;
	@Element("id_motivo_telefonista")
	private Integer idMotivoTelefonista;
	@Element("motivo_telefonista")
	private String motivoTelefonista;
	@Element("observaciones")
	private String observaciones;

	@Element("id_zona_patrullaje")
	private Integer idZonaPatrullaje;
	@Element("clave_zona_patrullaje")
	private String claveZonaPatrullaje;

	@Element("id_recurso")
	private Integer idRecurso;
	@Element("nombre_recurso")
	private String nombreRecurso;
	@Element("detenidos_recibidos")
	private Integer detenidosRecibidos;
	@Element("detenidos_entregados")
	private Integer detenidosentregados;
	@Element("id_tipo_movimiento")
	private Integer idTipoMovimiento;
	@Element("nombre_movimiento")
	private String nombreMovimiento;
	@Element("hora")
	private String hora;
	@Element("nota_cierre")
	private String notaCierre;
	@Element("estatus")
	private String estatus;
	@Element("tiempo")
	private Integer tiempo;
	private String tiempoStr;

	@Element("cantidad_detenidos")
	private Integer cantidadDetenidos;
	@Element("cantidad_lesionados")
	private Integer cantidadLesionados;

	@Element("h_transmision")
	private String horaTransmision;
	@Element("numero_atencion")
	private Integer numeroAtencion;
	@Element("razon_no_atencion")
	private String razonNoAtencion;
	@Element("nombre_motivo")
	private String nombreMotivo;
	@Element("observacion")
	private String observacion;
	@Element("folio_parte_informativo")
	private String folioParteInformativo;
	@Element("zona")
	private String zona;
	@Element("subtipo")
	private String subTipo;

	@Element("detenidos_entregados")
	private Integer detenidosEntregados;
	@Element("lesionados_recibidos")
	private Integer lesionadosRecibidos;
	@Element("lesionados_entregados")
	private Integer lesionadosEntregados;

	public Integer getDetenidosEntregados() {
		return detenidosEntregados;
	}

	public void setDetenidosEntregados(Integer detenidosEntregados) {
		this.detenidosEntregados = detenidosEntregados;
	}

	public Integer getLesionadosRecibidos() {
		return lesionadosRecibidos;
	}

	public void setLesionadosRecibidos(Integer lesionadosRecibidos) {
		this.lesionadosRecibidos = lesionadosRecibidos;
	}

	public Integer getLesionadosEntregados() {
		return lesionadosEntregados;
	}

	public void setLesionadosEntregados(Integer lesionadosEntregados) {
		this.lesionadosEntregados = lesionadosEntregados;
	}

	public String getHoraTransmision() {
		return horaTransmision;
	}

	public void setHoraTransmision(String horaTransmision) {
		this.horaTransmision = horaTransmision;
	}

	public Integer getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Integer numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getRazonNoAtencion() {
		return razonNoAtencion;
	}

	public void setRazonNoAtencion(String razonNoAtencion) {
		this.razonNoAtencion = razonNoAtencion;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getFolioParteInformativo() {
		return folioParteInformativo;
	}

	public void setFolioParteInformativo(String folioParteInformativo) {
		this.folioParteInformativo = folioParteInformativo;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getSubTipo() {
		return subTipo;
	}

	public void setSubTipo(String subTipo) {
		this.subTipo = subTipo;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getIdInstitucionAsignada() {
		return idInstitucionAsignada;
	}

	public void setIdInstitucionAsignada(Integer idInstitucionAsignada) {
		this.idInstitucionAsignada = idInstitucionAsignada;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Integer getIdInstitucion() {
		return idInstitucion;
	}

	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getFechaTransmision() {
		return fechaTransmision;
	}

	public void setFechaTransmision(String fechaTransmision) {
		this.fechaTransmision = fechaTransmision;
	}

	public String getFechaRazonamiento() {
		return fechaRazonamiento;
	}

	public void setFechaRazonamiento(String fechaRazonamiento) {
		this.fechaRazonamiento = fechaRazonamiento;
	}

	public Integer getDetenidos() {
		return detenidos;
	}

	public void setDetenidos(Integer detenidos) {
		this.detenidos = detenidos;
	}

	public Integer getIdRazonamiento() {
		return idRazonamiento;
	}

	public void setIdRazonamiento(Integer idRazonamiento) {
		this.idRazonamiento = idRazonamiento;
	}

	public String getRazonamiento() {
		return razonamiento;
	}

	public void setRazonamiento(String razonamiento) {
		this.razonamiento = razonamiento;
	}

	public Integer getIdMotivoRadioOperador() {
		return idMotivoRadioOperador;
	}

	public void setIdMotivoRadioOperador(Integer idMotivoRadioOperador) {
		this.idMotivoRadioOperador = idMotivoRadioOperador;
	}

	public String getMotivoRadioOperador() {
		return motivoRadioOperador;
	}

	public void setMotivoRadioOperador(String motivoRadioOperador) {
		this.motivoRadioOperador = motivoRadioOperador;
	}

	public Integer getIdRadioOperador() {
		return idRadioOperador;
	}

	public void setIdRadioOperador(Integer idRadioOperador) {
		this.idRadioOperador = idRadioOperador;
	}

	public String getNombreRadioOperador() {
		return nombreRadioOperador;
	}

	public void setNombreRadioOperador(String nombreRadioOperador) {
		this.nombreRadioOperador = nombreRadioOperador;
	}

	public Integer getIdMotivoTelefonista() {
		return idMotivoTelefonista;
	}

	public void setIdMotivoTelefonista(Integer idMotivoTelefonista) {
		this.idMotivoTelefonista = idMotivoTelefonista;
	}

	public String getMotivoTelefonista() {
		return motivoTelefonista;
	}

	public void setMotivoTelefonista(String motivoTelefonista) {
		this.motivoTelefonista = motivoTelefonista;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Integer getIdTipoCierre() {
		return idTipoCierre;
	}

	public void setIdTipoCierre(Integer idTipoCierre) {
		this.idTipoCierre = idTipoCierre;
	}

	public String getNombreTipo() {
		return nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

	public String getDescripcionCierre() {
		return descripcionCierre;
	}

	public void setDescripcionCierre(String descripcionCierre) {
		this.descripcionCierre = descripcionCierre;
	}

	public Integer getIdRecurso() {
		return idRecurso;
	}

	public void setIdRecurso(Integer idRecurso) {
		this.idRecurso = idRecurso;
	}

	public String getNombreRecurso() {
		return nombreRecurso;
	}

	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}

	public Integer getIdTipoMovimiento() {
		return idTipoMovimiento;
	}

	public void setIdTipoMovimiento(Integer idTipoMovimiento) {
		this.idTipoMovimiento = idTipoMovimiento;
	}

	public String getNombreMovimiento() {
		return nombreMovimiento;
	}

	public void setNombreMovimiento(String nombreMovimiento) {
		this.nombreMovimiento = nombreMovimiento;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNotaCierre() {
		return notaCierre;
	}

	public void setNotaCierre(String notaCierre) {
		this.notaCierre = notaCierre;
	}

	public String getIdInstitucionS() {
		return idInstitucionS;
	}

	public void setIdInstitucionS(String idInstitucionS) {
		this.idInstitucionS = idInstitucionS;
	}

	public Integer getIdZonaPatrullaje() {
		return idZonaPatrullaje;
	}

	public void setIdZonaPatrullaje(Integer idZonaPatrullaje) {
		this.idZonaPatrullaje = idZonaPatrullaje;
	}

	public String getClaveZonaPatrullaje() {
		return claveZonaPatrullaje;
	}

	public void setClaveZonaPatrullaje(String claveZonaPatrullaje) {
		this.claveZonaPatrullaje = claveZonaPatrullaje;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Integer getTiempo() {
		return tiempo;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}

	public String getUrlImegen() {
		return urlImegen;
	}

	public void setUrlImegen(String urlImegen) {
		this.urlImegen = urlImegen;
	}

	public Integer getCantidadDetenidos() {
		return cantidadDetenidos;
	}

	public void setCantidadDetenidos(Integer cantidadDetenidos) {
		this.cantidadDetenidos = cantidadDetenidos;
	}

	public Integer getCantidadLesionados() {
		return cantidadLesionados;
	}

	public void setCantidadLesionados(Integer cantidadLesionados) {
		this.cantidadLesionados = cantidadLesionados;
	}

	public Integer getDetenidosRecibidos() {
		return detenidosRecibidos;
	}

	public void setDetenidosRecibidos(Integer detenidosRecibidos) {
		this.detenidosRecibidos = detenidosRecibidos;
	}

	public Integer getDetenidosentregados() {
		return detenidosentregados;
	}

	public void setDetenidosentregados(Integer detenidosentregados) {
		this.detenidosentregados = detenidosentregados;
	}

	public String getTiempoStr() {
		return tiempoStr;
	}

	public void setTiempoStr(String tiempoStr) {
		this.tiempoStr = tiempoStr;
	}
}
