package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class CatalogoTipoNarizDTO {
	
	@Id
	@Element("id_tipo_nariz")
	private Integer idTipoNariz;
    @Element("nombre_tipo_nariz")
	private String nombreTipoNariz;
	private String uuid;
	private Integer creadoPor;
	private Integer modificadoPor;

	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Integer getIdTipoNariz() {
		return idTipoNariz;
	}
	public void setIdTipoNariz(Integer idTipoNariz) {
		this.idTipoNariz = idTipoNariz;
	}
	public String getNombreTipoNariz() {
		return nombreTipoNariz;
	}
	public void setNombreTipoNariz(String nombreTipoNariz) {
		this.nombreTipoNariz = nombreTipoNariz;
	}
	public Integer getCreadoPor() {
		return creadoPor;
	}
	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}
	public Integer getModificadoPor() {
		return modificadoPor;
	}
	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

}
