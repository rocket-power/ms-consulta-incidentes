package com.promad.catalogos.dto;

import java.util.List;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class MediosDTO extends BaseDTO {

	@Id
	@Element("id_medio_asociado")
	private Integer idMedioAsociado;
	@Element("nombre")
	private String nombre;
	@Element("content_type")
	private String contentType;
	@Element("tamanio_archivo")
	private String tamanioArchivo;
	@Element("comentario")
	private String descripcion;
	@Element("fecha")
	private String fecha;
	@Element("path")
	private String path;
	@Element("extension")
	private String extension;
	private List<MediosDTO> medios;
	private String ipServer;
	private String rutaArchivo;
	private String rutaDestino;
	private String data;

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Integer getIdMedioAsociado() {
		return idMedioAsociado;
	}

	public void setIdMedioAsociado(Integer idMedioAsociado) {
		this.idMedioAsociado = idMedioAsociado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getTamanioArchivo() {
		return tamanioArchivo;
	}

	public void setTamanioArchivo(String tamanioArchivo) {
		this.tamanioArchivo = tamanioArchivo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<MediosDTO> getMedios() {
		return medios;
	}

	public void setMedios(List<MediosDTO> medios) {
		this.medios = medios;
	}

	public String getIpServer() {
		return ipServer;
	}

	public void setIpServer(String ipServer) {
		this.ipServer = ipServer;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getRutaDestino() {
		return rutaDestino;
	}

	public void setRutaDestino(String rutaDestino) {
		this.rutaDestino = rutaDestino;
	}
}
