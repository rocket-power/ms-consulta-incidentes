package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class PersonasIncidenteDTO extends BaseDTO {
	@Element("revision")
	private String revision;

	@Element("ID_TIPO_PERSONA")
	private Integer idTipoPersona;
	@Element("NOMBRE_TIPO_PERSONA")
	private String tipoPersona;
	@Element("nombre")
	private String nombre;
	@Id
	@Element("id_persona_involucrada")
	private Integer idPersonaInvolucrada;
	@Element("edad")
	private Integer edad;

	@Element("apellido_paterno")
	private String apellidoPaterno;
	@Element("apellido_materno")
	private String apellidoMaterno;
	@Element("direccion_ip")
	private String dirrecionIp;
	@Element("estatus_lectura")
	private String estatusLectura;
	@Element("fecha_lectura")
	private String fechaLectura;
	@Element("id_boletin")
	private Integer idBoletin;
	@Element("id_color_cabello")
	private Integer idColorCabello;
	@Element("color_cabello")
	private String colorCabello;
	@Element("id_color_ojos")
	private Integer idColorOjos;
	@Element("nombre_color_ojos")
	private String nombreColorOjos;
	@Element("id_color_tez")
	private Integer idColorTez;
	@Element("color_tez")
	private String colorTez;
	@Element("id_estatura")
	private Integer idEstatura;
	@Element("estatura")
	private String estatura;
	@Element("id_forma_cara")
	private Integer idFormaCara;
	@Element("nombre_forma_cara")
	private String formaCara;
	@Element("id_tipo_cabello")
	private Integer idTipoCabello;
	@Element("tipo_cabello")
	private String tipoCabello;
	@Element("id_tipo_nariz")
	private Integer idTipoNariz;
	@Element("nombre_tipo_nariz")
	private String nombreTipoNariz;
	@Element("id_vestimenta_cabeza")
	private Integer idVestimentaCabeza;
	@Element("cabeza")
	private String cabeza;
	@Element("id_color_vestimenta_cabeza")
	private Integer idColorVestimentaCabeza;
	@Element("color_cabeza")
	private String colorCabeza;
	@Element("id_vestimenta_calzado")
	private Integer idVestimentaCalzado;
	@Element("calzado")
	private String calzado;
	@Element("id_color_vestimenta_calzado")
	private Integer idColorVestimentaCalzado;
	@Element("color_calzado")
	private String colorCalzado;
	@Element("id_vestimenta_inferior")
	private Integer idVestimentaInferior;
	@Element("inferior")
	private String inferior;
	@Element("id_color_vestimenta_inferior")
	private Integer idColorVestimentaInferior;
	@Element("color_inferior")
	private String colorInferior;
	@Element("id_vestimenta_talle")
	private Integer idVestimentaTalle;
	@Element("id_color_vestimenta_talle")
	private Integer idColorVestimentaTalle;
	@Element("talle")
	private String talle;
	@Element("color_talle")
	private String colorTallle;
	@Element("sexo")
	private Integer sexo;
	@Element("id_complexion")
	private Integer idComplexion;
	@Element("complexion")
	private String complexion;
	@Element("alias")
	private String alias;
	@Element("nombre_columna")
	private String nombreColumna;
	@Element("id_objetos")
	private String idObjetos;
	@Element("sexo")
	private Integer clave;
	@Element("senia_particular")
	private String seniaParticular;

	@Element("nombre_tipo_identificacion")
	private String tipoIdentificacion;
	@Element("nombre_color_ojos")
	private String colorOjos;
	@Element("nombre_tipo_nariz")
	private String tipoNariz;
	@Element("descripcion_vestimenta")
	private String descripcionVestimenta;

	@Element("ESTATURA_PROMEDIO")
	private String estaturaPromedio;

	@Id
	@Element("id_objeto")
	private Integer idObjeto;
	@Element("nombre_objeto")
	private String nombreObjeto;

	@Element("folio")
	private String folio;
	@Element("nombre_motivo")
	private String motivo;

	public String getSeniaParticular() {
		return seniaParticular;
	}

	public void setSeniaParticular(String senisParticular) {
		this.seniaParticular = senisParticular;
	}

	public Integer getIdPersonaInvolucrada() {
		return idPersonaInvolucrada;
	}

	public void setIdPersonaInvolucrada(Integer idPersonaInvolucrada) {
		this.idPersonaInvolucrada = idPersonaInvolucrada;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Integer getIdColorCabello() {
		return idColorCabello;
	}

	public void setIdColorCabello(Integer idColorCabello) {
		this.idColorCabello = idColorCabello;
	}

	public String getColorCabello() {
		return colorCabello;
	}

	public void setColorCabello(String colorCabello) {
		this.colorCabello = colorCabello;
	}

	public Integer getIdColorOjos() {
		return idColorOjos;
	}

	public void setIdColorOjos(Integer idColorOjos) {
		this.idColorOjos = idColorOjos;
	}

	public String getNombreColorOjos() {
		return nombreColorOjos;
	}

	public void setNombreColorOjos(String nombreColorOjos) {
		this.nombreColorOjos = nombreColorOjos;
	}

	public Integer getIdColorTez() {
		return idColorTez;
	}

	public void setIdColorTez(Integer idColorTez) {
		this.idColorTez = idColorTez;
	}

	public String getColorTez() {
		return colorTez;
	}

	public void setColorTez(String colorTez) {
		this.colorTez = colorTez;
	}

	public Integer getIdEstatura() {
		return idEstatura;
	}

	public void setIdEstatura(Integer idEstatura) {
		this.idEstatura = idEstatura;
	}

	public String getEstatura() {
		return estatura;
	}

	public void setEstatura(String estatura) {
		this.estatura = estatura;
	}

	public Integer getIdFormaCara() {
		return idFormaCara;
	}

	public void setIdFormaCara(Integer idFormaCara) {
		this.idFormaCara = idFormaCara;
	}

	public String getFormaCara() {
		return formaCara;
	}

	public void setFormaCara(String formaCara) {
		this.formaCara = formaCara;
	}

	public Integer getIdTipoCabello() {
		return idTipoCabello;
	}

	public void setIdTipoCabello(Integer idTipoCabello) {
		this.idTipoCabello = idTipoCabello;
	}

	public String getTipoCabello() {
		return tipoCabello;
	}

	public void setTipoCabello(String tipoCabello) {
		this.tipoCabello = tipoCabello;
	}

	public Integer getIdTipoNariz() {
		return idTipoNariz;
	}

	public void setIdTipoNariz(Integer idTipoNariz) {
		this.idTipoNariz = idTipoNariz;
	}

	public String getNombreTipoNariz() {
		return nombreTipoNariz;
	}

	public void setNombreTipoNariz(String nombreTipoNariz) {
		this.nombreTipoNariz = nombreTipoNariz;
	}

	public Integer getIdTipoPersona() {
		return idTipoPersona;
	}

	public void setIdTipoPersona(Integer idTipoPersona) {
		this.idTipoPersona = idTipoPersona;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Integer getIdVestimentaCabeza() {
		return idVestimentaCabeza;
	}

	public void setIdVestimentaCabeza(Integer idVestimentaCabeza) {
		this.idVestimentaCabeza = idVestimentaCabeza;
	}

	public String getCabeza() {
		return cabeza;
	}

	public void setCabeza(String cabeza) {
		this.cabeza = cabeza;
	}

	public Integer getIdColorVestimentaCabeza() {
		return idColorVestimentaCabeza;
	}

	public void setIdColorVestimentaCabeza(Integer idColorVestimentaCabeza) {
		this.idColorVestimentaCabeza = idColorVestimentaCabeza;
	}

	public String getColorCabeza() {
		return colorCabeza;
	}

	public void setColorCabeza(String colorCabeza) {
		this.colorCabeza = colorCabeza;
	}

	public Integer getIdVestimentaCalzado() {
		return idVestimentaCalzado;
	}

	public void setIdVestimentaCalzado(Integer idVestimentaCalzado) {
		this.idVestimentaCalzado = idVestimentaCalzado;
	}

	public String getCalzado() {
		return calzado;
	}

	public void setCalzado(String calzado) {
		this.calzado = calzado;
	}

	public Integer getIdColorVestimentaCalzado() {
		return idColorVestimentaCalzado;
	}

	public void setIdColorVestimentaCalzado(Integer idColorVestimentaCalzado) {
		this.idColorVestimentaCalzado = idColorVestimentaCalzado;
	}

	public String getColorCalzado() {
		return colorCalzado;
	}

	public void setColorCalzado(String colorCalzado) {
		this.colorCalzado = colorCalzado;
	}

	public Integer getIdVestimentaInferior() {
		return idVestimentaInferior;
	}

	public void setIdVestimentaInferior(Integer idVestimentaInferior) {
		this.idVestimentaInferior = idVestimentaInferior;
	}

	public String getInferior() {
		return inferior;
	}

	public void setInferior(String inferior) {
		this.inferior = inferior;
	}

	public Integer getIdColorVestimentaInferior() {
		return idColorVestimentaInferior;
	}

	public void setIdColorVestimentaInferior(Integer idColorVestimentaInferior) {
		this.idColorVestimentaInferior = idColorVestimentaInferior;
	}

	public String getColorInferior() {
		return colorInferior;
	}

	public void setColorInferior(String colorInferior) {
		this.colorInferior = colorInferior;
	}

	public String getTalle() {
		return talle;
	}

	public void setTalle(String talle) {
		this.talle = talle;
	}

	public String getColorTallle() {
		return colorTallle;
	}

	public void setColorTallle(String colorTallle) {
		this.colorTallle = colorTallle;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getSexo() {
		return sexo;
	}

	public void setSexo(Integer sexo) {
		this.sexo = sexo;
	}

	public Integer getIdComplexion() {
		return idComplexion;
	}

	public void setIdComplexion(Integer idComplexion) {
		this.idComplexion = idComplexion;
	}

	public String getComplexion() {
		return complexion;
	}

	public void setComplexion(String complexion) {
		this.complexion = complexion;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getEstatusLectura() {
		return estatusLectura;
	}

	public void setEstatusLectura(String estatusLectura) {
		this.estatusLectura = estatusLectura;
	}

	public String getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(String fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	public Integer getIdBoletin() {
		return idBoletin;
	}

	public void setIdBoletin(Integer idBoletin) {
		this.idBoletin = idBoletin;
	}

	public Integer getIdVestimentaTalle() {
		return idVestimentaTalle;
	}

	public void setIdVestimentaTalle(Integer idVestimentaTalle) {
		this.idVestimentaTalle = idVestimentaTalle;
	}

	public String getNombreColumna() {
		return nombreColumna;
	}

	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	public Integer getIdObjeto() {
		return idObjeto;
	}

	public void setIdObjeto(Integer idObjeto) {
		this.idObjeto = idObjeto;
	}

	public String getNombreObjeto() {
		return nombreObjeto;
	}

	public void setNombreObjeto(String nombreObjeto) {
		this.nombreObjeto = nombreObjeto;
	}

	public Integer getIdColorVestimentaTalle() {
		return idColorVestimentaTalle;
	}

	public void setIdColorVestimentaTalle(Integer idColorVestimentaTalle) {
		this.idColorVestimentaTalle = idColorVestimentaTalle;
	}

	public String getDirrecionIp() {
		return dirrecionIp;
	}

	public void setDirrecionIp(String dirrecionIp) {
		this.dirrecionIp = dirrecionIp;
	}

	public String getIdObjetos() {
		return idObjetos;
	}

	public void setIdObjetos(String idObjetos) {
		this.idObjetos = idObjetos;
	}

	public Integer getClave() {
		return clave;
	}

	public void setClave(Integer clave) {
		this.clave = clave;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getEstaturaPromedio() {
		return estaturaPromedio;
	}

	public void setEstaturaPromedio(String estaturaPromedio) {
		this.estaturaPromedio = estaturaPromedio;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getColorOjos() {
		return colorOjos;
	}

	public void setColorOjos(String colorOjos) {
		this.colorOjos = colorOjos;
	}

	public String getTipoNariz() {
		return tipoNariz;
	}

	public void setTipoNariz(String tipoNariz) {
		this.tipoNariz = tipoNariz;
	}

	public String getDescripcionVestimenta() {
		return descripcionVestimenta;
	}

	public void setDescripcionVestimenta(String descripcionVestimenta) {
		this.descripcionVestimenta = descripcionVestimenta;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}
}
