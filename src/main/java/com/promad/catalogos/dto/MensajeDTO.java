package com.promad.catalogos.dto;

public class MensajeDTO {

	private String mensaje;
	private String estatus;

	public MensajeDTO() {

	}

	public MensajeDTO(String mensaje, String estatus) {
		this.mensaje = mensaje;
		this.estatus = estatus;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

}
