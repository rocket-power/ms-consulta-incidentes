package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class ContadorPavoDTO extends BaseDTO {

	@Element("personas")
	private Integer totalPersonas;
	@Element("armas")
	private Integer totalArmas;
	@Element("vehiculos")
	private Integer totalVehiculos;
	@Element("objetos")
	private Integer totalObjetos;

	public Integer getTotalPersonas() {
		return totalPersonas;
	}

	public void setTotalPersonas(Integer totalPersonas) {
		this.totalPersonas = totalPersonas;
	}

	public Integer getTotalArmas() {
		return totalArmas;
	}

	public void setTotalArmas(Integer totalArmas) {
		this.totalArmas = totalArmas;
	}

	public Integer getTotalVehiculos() {
		return totalVehiculos;
	}

	public void setTotalVehiculos(Integer totalVehiculos) {
		this.totalVehiculos = totalVehiculos;
	}

	public Integer getTotalObjetos() {
		return totalObjetos;
	}

	public void setTotalObjetos(Integer totalObjetos) {
		this.totalObjetos = totalObjetos;
	}

}
