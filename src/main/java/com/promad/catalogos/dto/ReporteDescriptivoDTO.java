package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ReporteDescriptivoDTO {
	@Id
	@Element("id_evento")
	private Integer idEvento;
	@Element("uuid")
	private String uuid;
	@Element("folio")
	private String folio;
	@Element("recepcion")
	private String recepcion;
	@Element("prioridad")
	private String prioridad;
	@Element("motivo")
	private String motivo;
	@Element("lugar")
	private String lugar;
	@Element("comunidad")
	private String comunidad;
	@Element("ciudad")
	private String ciudad;
	@Element("descripcion")
	private String descripcion;
	@Element("denunciate")
	private String denunciante;
	@Element("telefono")
	private String telefono;
	@Element("hora_ocurre")
	private String horaOcurre;
	@Element("origen")
	private String origen;
	@Element("recibida_por")
	private String recibidaPor;
	@Element("h_recepcion")
	private String horaRecepcion;
	@Element("h_transmision")
	private String horaTransmision;
	@Element("t_transmision")
	private String tiempoTransmision;
	@Element("h_captura")
	private String horaCaptura;
	@Element("t_captura")
	private String tiempoCaptura;

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getRecepcion() {
		return recepcion;
	}

	public void setRecepcion(String recepcion) {
		this.recepcion = recepcion;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getComunidad() {
		return comunidad;
	}

	public void setComunidad(String comunidad) {
		this.comunidad = comunidad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDenunciante() {
		return denunciante;
	}

	public void setDenunciante(String denunciante) {
		this.denunciante = denunciante;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getHoraOcurre() {
		return horaOcurre;
	}

	public void setHoraOcurre(String horaOcurre) {
		this.horaOcurre = horaOcurre;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getRecibidaPor() {
		return recibidaPor;
	}

	public void setRecibidaPor(String recibidaPor) {
		this.recibidaPor = recibidaPor;
	}

	public String getHoraRecepcion() {
		return horaRecepcion;
	}

	public void setHoraRecepcion(String horaRecepcion) {
		this.horaRecepcion = horaRecepcion;
	}

	public String getHoraTransmision() {
		return horaTransmision;
	}

	public void setHoraTransmision(String horaTransmision) {
		this.horaTransmision = horaTransmision;
	}

	public String getTiempoTransmision() {
		return tiempoTransmision;
	}

	public void setTiempoTransmision(String tiempoTransmision) {
		this.tiempoTransmision = tiempoTransmision;
	}

	public String getHoraCaptura() {
		return horaCaptura;
	}

	public void setHoraCaptura(String horaCaptura) {
		this.horaCaptura = horaCaptura;
	}

	public String getTiempoCaptura() {
		return tiempoCaptura;
	}

	public void setTiempoCaptura(String tiempoCaptura) {
		this.tiempoCaptura = tiempoCaptura;
	}
}
