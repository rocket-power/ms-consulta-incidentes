package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class CatalogoSupervisoraDTO extends BaseDTO {

	@Id
	@Element("id_motivo_cambio_rastreo")
	private Integer idMotivoCambioRastreo;
	@Element("nombre_motivo")
	private String cambioEstatusRastreo;

	public Integer getIdMotivoCambioRastreo() {
		return idMotivoCambioRastreo;
	}

	public void setIdMotivoCambioRastreo(Integer idMotivoCambioRastreo) {
		this.idMotivoCambioRastreo = idMotivoCambioRastreo;
	}

	public String getCambioEstatusRastreo() {
		return cambioEstatusRastreo;
	}

	public void setCambioEstatusRastreo(String cambioEstatusRastreo) {
		this.cambioEstatusRastreo = cambioEstatusRastreo;
	}

}
