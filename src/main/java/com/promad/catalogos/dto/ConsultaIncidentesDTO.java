package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ConsultaIncidentesDTO {

	@Id
	@Element("id_rol_usuario")
	private Integer idRolUsuario;
	@Id
	@Element("id_evento")
	private Integer idEvento;
	private String uuid;
	private String a;
	private String b;
	private String c;
	private String d;
	@Element("folio")
	private String folio;
	private Integer pageNumber;
	private Integer pageSize;
	@Element("r__")
	private Integer rowNum;
	@Element("folio_desde")
	private String folioDesde;
	@Element("folio_hasta")
	private String folioHasta;

	private String tiempo;
	@Element("fecha_inicio")
	private String fechaDesde;
	@Element("fecha_fin")
	private String fechaHasta;

	@Element("fecha")
	private String fecha;
	@Element("cuenta")
	private String usuario;

	@Element("colonia")
	private String nombreColonia;
	@Element("calle")
	private String calle;
	@Id
	@Element("id_motivo")
	private Integer idMotivo;
	@Element("nombre_motivo")
	//
	private String nombreMotivo;
	@Element("clave_operativa")
	private String claveOperativa;
	@Element("id_origen")
	private Integer idOrigen;
	@Element("nombre_origen")
	private String nombreOrigen;
	@Element("prioridad")
	private String prioridad;
	@Element("telefono")
	private String telefono;
	@Id
	@Element("id_institucion")
	private Integer idInstitucion;
	@Id
	@Element("id_municipio")
	private Integer idMunicipio;
	@Element("municipio")
	private String nombreMunicipio;
	@Element("id_localidad")
	private Integer idLocalidad;
	@Element("nombre_localidad")
	private String nombreLocalidad;
	@Element("estatus")
	private String estatus;
	@Element("lugar")
	private String lugar;
	@Element("descripcion")
	private String descripcion;

	@Element("nombre")
	private String nombre;
	@Element("materno")
	private String apellidoMaterno;
	@Element("paterno")
	private String apellidoPaterno;
	@Element("alias_pi")
	private String alias;
	@Element("edad")
	private String edad;
	@Element("estatura")
	private String estatura;
	@Element("id_tipo_persona")
	private Integer idTipoPersona;
	@Id
	@Element("complexion")
	private Integer idComplexion;
	@Id
	@Element("tipo_cabello")
	private Integer idTipoCabello;
	@Id
	@Element("color_cabello")
	private Integer idColorCabello;

	@Element("sexo")
	private String genero;
	@Element("forma_cara")
	private Integer idFormaCara;

	@Id
	@Element("color_tez")
	private Integer idColorTez;

	@Id
	@Element("color_ojos")
	private Integer idColorOjos;

	@Id
	@Element("tipo_nariz")
	private Integer idTipoNariz;
	@Element("senia_p")
	private String seniasParticulares;

	private Integer idVestimenta;
	private String nombreVestimenta;
	private String porcionVestimenta;
	@Element("cabeza_tipo")
	private String cabezaTipo;
	@Element("cabeza_color")
	private String idCabezaColor;
	@Element("talle_tipo")
	private String talleTipo;
	@Element("talle_color")
	private String idTalleColor;
	@Element("inferior_tipo")
	private String inferiorTipo;
	@Element("inferior_color")
	private String idInferiorColor;
	@Element("calzado_tipo")
	private String calzadoTipo;
	@Element("calzado_color")
	private String idCalzadoColor;

	@Element("id_tipo_vehiculo")
	private Integer idTipoVehiculo;
	@Element("id_marca")
	private Integer idMarca;

	@Element("id_modelo")
	private Integer idModelo;
	@Element("modelo")
	private String modelo;
	@Element("numero_placa")
	private String numeroPlaca;
	@Element("id_origen_v")
	private String idEstado;
	@Element("numero_serie")
	private String numeroSerie;
	@Element("numero_motor")
	private String numeroMotor;
	@Element("vin_num")
	private String niv;
	@Element("id_color")
	private Integer idColor;
	@Element("anio")
	private String anio;
	@Id
	@Element("id_estatus_rastreo")
	private Integer idEstatusRastreo;
	@Element("observaciones")
	private String observacionesveh;
	@Element("lista_negra")
	private String listaNegra;

	@Element("tipo")
	private Integer idCategoria;
	@Element("sub_tipo")
	private Integer idTipoObjeto;
	@Element("id_objeto")
	private String idObjeto;
	@Element("cantidad")
	private Integer cantidad;
	@Element("estimado")
	private Integer estimado;
	@Element("historico")
	private String historico;
	@Element("NUMERO_AVERIGUACION")
	private String numeroAveriguacion;

	public Integer getIdTipoObjeto() {
		return idTipoObjeto;
	}

	public void setIdTipoObjeto(Integer idTipoObjeto) {
		this.idTipoObjeto = idTipoObjeto;
	}

	public String getCabezaTipo() {
		return cabezaTipo;
	}

	public void setCabezaTipo(String cabezaTipo) {
		this.cabezaTipo = cabezaTipo;
	}

	public String getIdCabezaColor() {
		return idCabezaColor;
	}

	public void setIdCabezaColor(String idCabezaColor) {
		this.idCabezaColor = idCabezaColor;
	}

	public String getTalleTipo() {
		return talleTipo;
	}

	public void setTalleTipo(String talleTipo) {
		this.talleTipo = talleTipo;
	}

	public String getIdTalleColor() {
		return idTalleColor;
	}

	public void setIdTalleColor(String idTalleColor) {
		this.idTalleColor = idTalleColor;
	}

	public String getInferiorTipo() {
		return inferiorTipo;
	}

	public void setInferiorTipo(String inferiorTipo) {
		this.inferiorTipo = inferiorTipo;
	}

	public String getIdInferiorColor() {
		return idInferiorColor;
	}

	public void setIdInferiorColor(String idInferiorColor) {
		this.idInferiorColor = idInferiorColor;
	}

	public String getCalzadoTipo() {
		return calzadoTipo;
	}

	public void setCalzadoTipo(String calzadoTipo) {
		this.calzadoTipo = calzadoTipo;
	}

	public String getIdCalzadoColor() {
		return idCalzadoColor;
	}

	public void setIdCalzadoColor(String idCalzadoColor) {
		this.idCalzadoColor = idCalzadoColor;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolioDesde() {
		return folioDesde;
	}

	public void setFolioDesde(String folioDesde) {
		this.folioDesde = folioDesde;
	}

	public String getFolioHasta() {
		return folioHasta;
	}

	public void setFolioHasta(String folioHasta) {
		this.folioHasta = folioHasta;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Integer idMotivo) {
		this.idMotivo = idMotivo;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public String getClaveOperativa() {
		return claveOperativa;
	}

	public void setClaveOperativa(String claveOperativa) {
		this.claveOperativa = claveOperativa;
	}

	public Integer getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(Integer idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getNombreOrigen() {
		return nombreOrigen;
	}

	public void setNombreOrigen(String nombreOrigen) {
		this.nombreOrigen = nombreOrigen;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getIdInstitucion() {
		return idInstitucion;
	}

	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getNombreMunicipio() {
		return nombreMunicipio;
	}

	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getNombreLocalidad() {
		return nombreLocalidad;
	}

	public void setNombreLocalidad(String nombreLocalidad) {
		this.nombreLocalidad = nombreLocalidad;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getEstatura() {
		return estatura;
	}

	public void setEstatura(String estatura) {
		this.estatura = estatura;
	}

	public Integer getIdTipoPersona() {
		return idTipoPersona;
	}

	public void setIdTipoPersona(Integer idTipoPersona) {
		this.idTipoPersona = idTipoPersona;
	}

	public Integer getIdComplexion() {
		return idComplexion;
	}

	public void setIdComplexion(Integer idComplexion) {
		this.idComplexion = idComplexion;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Integer getIdTipoCabello() {
		return idTipoCabello;
	}

	public void setIdTipoCabello(Integer idTipoCabello) {
		this.idTipoCabello = idTipoCabello;
	}

	public Integer getIdColorCabello() {
		return idColorCabello;
	}

	public void setIdColorCabello(Integer idColorCabello) {
		this.idColorCabello = idColorCabello;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Integer getIdFormaCara() {
		return idFormaCara;
	}

	public void setIdFormaCara(Integer idFormaCara) {
		this.idFormaCara = idFormaCara;
	}

	public Integer getIdColorTez() {
		return idColorTez;
	}

	public void setIdColorTez(Integer idColorTez) {
		this.idColorTez = idColorTez;
	}

	public Integer getIdColorOjos() {
		return idColorOjos;
	}

	public void setIdColorOjos(Integer idColorOjos) {
		this.idColorOjos = idColorOjos;
	}

	public Integer getIdTipoNariz() {
		return idTipoNariz;
	}

	public void setIdTipoNariz(Integer idTipoNariz) {
		this.idTipoNariz = idTipoNariz;
	}

	public String getSeniasParticulares() {
		return seniasParticulares;
	}

	public void setSeniasParticulares(String seniasParticulares) {
		this.seniasParticulares = seniasParticulares;
	}

	public Integer getIdVestimenta() {
		return idVestimenta;
	}

	public void setIdVestimenta(Integer idVestimenta) {
		this.idVestimenta = idVestimenta;
	}

	public String getNombreVestimenta() {
		return nombreVestimenta;
	}

	public void setNombreVestimenta(String nombreVestimenta) {
		this.nombreVestimenta = nombreVestimenta;
	}

	public String getPorcionVestimenta() {
		return porcionVestimenta;
	}

	public void setPorcionVestimenta(String porcionVestimenta) {
		this.porcionVestimenta = porcionVestimenta;
	}

	public String getNumeroPlaca() {
		return numeroPlaca;
	}

	public void setNumeroPlaca(String numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdModelo() {
		return idModelo;
	}

	public void setIdModelo(Integer idModelo) {
		this.idModelo = idModelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getIdColor() {
		return idColor;
	}

	public void setIdColor(Integer idColor) {
		this.idColor = idColor;
	}

	public Integer getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	public void setIdTipoVehiculo(Integer idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	public String getObservacionesveh() {
		return observacionesveh;
	}

	public void setObservacionesveh(String observacionesveh) {
		this.observacionesveh = observacionesveh;
	}

	public Integer getIdEstatusRastreo() {
		return idEstatusRastreo;
	}

	public void setIdEstatusRastreo(Integer idEstatusRastreo) {
		this.idEstatusRastreo = idEstatusRastreo;
	}

	public String getNiv() {
		return niv;
	}

	public void setNiv(String niv) {
		this.niv = niv;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getListaNegra() {
		return listaNegra;
	}

	public void setListaNegra(String listaNegra) {
		this.listaNegra = listaNegra;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getEstimado() {
		return estimado;
	}

	public void setEstimado(Integer estimado) {
		this.estimado = estimado;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getIdObjeto() {
		return idObjeto;
	}

	public void setIdObjeto(String idObjeto) {
		this.idObjeto = idObjeto;
	}

	public Integer getIdRolUsuario() {
		return idRolUsuario;
	}

	public void setIdRolUsuario(Integer idRolUsuario) {
		this.idRolUsuario = idRolUsuario;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getNumeroAveriguacion() {
		return numeroAveriguacion;
	}

	public void setNumeroAveriguacion(String numeroAveriguacion) {
		this.numeroAveriguacion = numeroAveriguacion;
	}
	

}
