package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class DatosIncidenteDTO extends BaseDTO {

	@Element("clave_operativa")
	private String claveOperativa;
	@Id
	@Element("id_motivo")
	private Integer idMotivo;
	@Element("nombre_motivo")
	private String nombreMotivo;
	@Element("prioridad")
	private String prioridad;
	@Element("id_origen")
	private Integer idOrigen;
	@Element("nombre_origen")
	private String nombreOrigen;
	@Element("estatus")
	private String estatus;
	@Element("calle")
	private String calle;
	@Element("nombre_colonia")
	private String nombreColonia;
	@Element("nombre_municipio")
	private String nombreMunicipio;
	@Element("nombre_estado")
	private String nombreEstado;
	@Element("lugar")
	private String lugar;
	@Element("descripcion_lugar")
	private String descripcionLugar;
	@Element("descripcion")
	private String descripcion;
	@Element("id_direccion_evento")
	private Integer idDireccionEvento;
	@Element("nombre_motivo_cambio_direccion")
	private String NomMotCambioDireccion;
	@Element("fecha")
	private String fecha;
	@Element("cuenta")
	private String cuenta;
	@Element("entre_calle_1")
	private String entreCalle1;
	@Element("entre_calle_2")
	private String EntreCalle2;
	@Element("numero")
	private String numero;
	@Element("id_localidad")
	private Integer idLocalidad;
	@Element("nombre_localidad")
	private String nombreLocalidad;
	@Element("folio")
	private String folio;
	@Element("Telefono")
	private String telefono;
	@Element("fecha_recepcion")
	private String fechaRecepcion;

	@Element("datos_incidente")
	private String datosIncidente;
	@Element("fecha_inicio")
	private String fechaInicio;
	@Element("fecha_fin")
	private String fechaFin;
	@Element("recibida_por")
	private String recibidaPor;
	@Element("denunciante")
	private String denunciante;
	@Element("direccion")
	private String direccion;

	public String getDatosIncidente() {
		return datosIncidente;
	}

	public void setDatosIncidente(String datosIncidente) {
		this.datosIncidente = datosIncidente;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getRecibidaPor() {
		return recibidaPor;
	}

	public void setRecibidaPor(String recibidaPor) {
		this.recibidaPor = recibidaPor;
	}

	public String getDenunciante() {
		return denunciante;
	}

	public void setDenunciante(String denunciante) {
		this.denunciante = denunciante;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getNombreLocalidad() {
		return nombreLocalidad;
	}

	public void setNombreLocalidad(String nombreLocalidad) {
		this.nombreLocalidad = nombreLocalidad;
	}

	public String getEntreCalle1() {
		return entreCalle1;
	}

	public void setEntreCalle1(String entreCalle1) {
		this.entreCalle1 = entreCalle1;
	}

	public String getEntreCalle2() {
		return EntreCalle2;
	}

	public void setEntreCalle2(String entreCalle2) {
		EntreCalle2 = entreCalle2;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getIdDireccionEvento() {
		return idDireccionEvento;
	}

	public void setIdDireccionEvento(Integer idDireccionEvento) {
		this.idDireccionEvento = idDireccionEvento;
	}

	public String getNomMotCambioDireccion() {
		return NomMotCambioDireccion;
	}

	public void setNomMotCambioDireccion(String nomMotCambioDireccion) {
		NomMotCambioDireccion = nomMotCambioDireccion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getClaveOperativa() {
		return claveOperativa;
	}

	public void setClaveOperativa(String claveOperativa) {
		this.claveOperativa = claveOperativa;
	}

	public Integer getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Integer idMotivo) {
		this.idMotivo = idMotivo;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public Integer getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(Integer idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getNombreOrigen() {
		return nombreOrigen;
	}

	public void setNombreOrigen(String nombreOrigen) {
		this.nombreOrigen = nombreOrigen;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	public String getNombreMunicipio() {
		return nombreMunicipio;
	}

	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getDescripcionLugar() {
		return descripcionLugar;
	}

	public void setDescripcionLugar(String descripcionLugar) {
		this.descripcionLugar = descripcionLugar;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

}
