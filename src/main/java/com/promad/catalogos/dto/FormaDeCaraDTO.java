package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class FormaDeCaraDTO {

	private String uuid;
	@Id
	@Element("nombre_forma_cara")
	private String formaCara;
	@Element("id_forma_cara")
	private Integer idFormaCara;
	private Integer modificadoPor;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFormaCara() {
		return formaCara;
	}

	public void setFormaCara(String formaCara) {
		this.formaCara = formaCara;
	}

	public Integer getIdFormaCara() {
		return idFormaCara;
	}

	public void setIdFormaCara(Integer idFormaCara) {
		this.idFormaCara = idFormaCara;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
