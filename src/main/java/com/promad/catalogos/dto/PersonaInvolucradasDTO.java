package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class PersonaInvolucradasDTO {
	@Id
	@Element("id_accesorio")
	private Integer idAccesorio;
	@Element("nombre_accesorio")
	private String nombreAccesorio;
	private String uuid;
	private Integer creadoPor;
	private Integer modificadoPor;

	public PersonaInvolucradasDTO() {
		super();
	}

	public Integer getIdAccesorio() {
		return idAccesorio;
	}

	public void setIdAccesorio(Integer idAccesorio) {
		this.idAccesorio = idAccesorio;
	}

	public String getNombreAccesorio() {
		return nombreAccesorio;
	}

	public void setNombreAccesorio(String nombreAccesorio) {
		this.nombreAccesorio = nombreAccesorio;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
