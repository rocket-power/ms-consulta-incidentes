package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class BitacoraDTO extends BaseDTO {

	@Element("folio")
	private String folio;
	@Element("nombre_institucion")
	private String nombreInstitucion;
	@Element("cuenta")
	private String cuenta;
	@Element("tipo_atencion")
	private String tipoAtencion;
	@Element("fecha")
	private String fecha;
	@Element("descripcion")
	private String descripcion;
	@Element("nombre_municipio")
	private String municipio;
	@Element("id_bitacora_evento")
	private Integer idBitacora;
	@Element("id_municipio")
	private Integer idMunicipio;

	@Element("recibida_por")
	private String recibidaPor;
	@Element("h_recepcion")
	private String horaRecepcion;
	@Element("h_transmision")
	private String horaTransmision;
	@Element("t_transmision")
	private String tiempoTransmision;
	@Element("h_captura")
	private String horaCaptura;
	@Element("t_captura")
	private String tiempoCaptura;
	@Element("nombre_origen")
	private String origen;
	@Element("zona")
	private String zona;
	@Element("subconjunto")
	private String subConjunto;

	public String getRecibidaPor() {
		return recibidaPor;
	}

	public void setRecibidaPor(String recibidaPor) {
		this.recibidaPor = recibidaPor;
	}

	public String getHoraRecepcion() {
		return horaRecepcion;
	}

	public void setHoraRecepcion(String horaRecepcion) {
		this.horaRecepcion = horaRecepcion;
	}

	public String getHoraTransmision() {
		return horaTransmision;
	}

	public void setHoraTransmision(String horaTransmision) {
		this.horaTransmision = horaTransmision;
	}

	public String getTiempoTransmision() {
		return tiempoTransmision;
	}

	public void setTiempoTransmision(String tiempoTransmision) {
		this.tiempoTransmision = tiempoTransmision;
	}

	public String getHoraCaptura() {
		return horaCaptura;
	}

	public void setHoraCaptura(String horaCaptura) {
		this.horaCaptura = horaCaptura;
	}

	public String getTiempoCaptura() {
		return tiempoCaptura;
	}

	public void setTiempoCaptura(String tiempoCaptura) {
		this.tiempoCaptura = tiempoCaptura;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getTipoAtencion() {
		return tipoAtencion;
	}

	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Integer getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(Integer idBitacora) {
		this.idBitacora = idBitacora;
	}

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getSubConjunto() {
		return subConjunto;
	}

	public void setSubConjunto(String subConjunto) {
		this.subConjunto = subConjunto;
	}

}
