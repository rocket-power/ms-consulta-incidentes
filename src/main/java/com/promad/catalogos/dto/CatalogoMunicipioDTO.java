package com.promad.catalogos.dto;

import java.util.List;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class CatalogoMunicipioDTO {

	@Id
	@Element("id_municipio")
	private Integer idMunicipio;
	@Element("nombre_municipio")
	private String nombreMunicipio;
	@Element("id_estado")
	private Integer idEstado;
	@Element("nombre_estado")
	private String nombreEstado;
	@Element("nombre")
	private String nombre;
	@Element("nombre_corto")
	private String nombreCorto;

	private List<CatLocalidadDTO> listaLocalidad;

	@Element("id_evento")
	private Integer idEvento;
	@Element("uuid")
	private String uuid;
	@Element("creado_por")
	private Integer creadoPor;
	@Element("modificado_por")
	private Integer modificadoPor;

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getNombreMunicipio() {
		return nombreMunicipio;
	}

	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public List<CatLocalidadDTO> getListaLocalidad() {
		return listaLocalidad;
	}

	public void setListaLocalidad(List<CatLocalidadDTO> listaLocalidad) {
		this.listaLocalidad = listaLocalidad;
	}

	public JRBeanCollectionDataSource getLocalidadDataSource() {
		return new JRBeanCollectionDataSource(listaLocalidad, false);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

}
