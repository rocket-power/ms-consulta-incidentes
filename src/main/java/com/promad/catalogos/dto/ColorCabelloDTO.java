package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ColorCabelloDTO {

	@Id
	@Element("id_color_cabello")
	private Integer idColorCabello;
	@Element("color_cabello")
	private String colorCabello;
	private String uuid;
	private Integer creadoPor;
	private Integer modificadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getColorCabello() {
		return colorCabello;
	}

	public void setColorCabello(String colorCabello) {
		this.colorCabello = colorCabello;
	}

	public Integer getIdColorCabello() {
		return idColorCabello;
	}

	public void setIdColorCabello(Integer idColorCabello) {
		this.idColorCabello = idColorCabello;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

}
