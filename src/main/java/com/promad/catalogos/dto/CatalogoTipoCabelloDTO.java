package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class CatalogoTipoCabelloDTO {
	private String uuid;
	private Integer creadoPor;
	private Integer modificadoPor;

	@Id
	@Element("id_tipo_cabello")
	private Integer idTipoCabello;
	@Element("tipo_cabello")
	private String tipoCabello;

	public Integer getIdTipoCabello() {
		return idTipoCabello;
	}

	public void setIdTipoCabello(Integer idTipoCabello) {
		this.idTipoCabello = idTipoCabello;
	}

	public String getTipoCabello() {
		return tipoCabello;
	}

	public void setTipoCabello(String tipoCabello) {
		this.tipoCabello = tipoCabello;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

}
