package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class ReporteTemplateDTO extends BaseDTO {

	@Element("nombre_template")
	private String nombreTemplate;
	@Element("contenido")
	private String contenido;
	private Integer creadoPor;

	public String getNombreTemplate() {
		return nombreTemplate;
	}

	public void setNombreTemplate(String nombreTemplate) {
		this.nombreTemplate = nombreTemplate;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
