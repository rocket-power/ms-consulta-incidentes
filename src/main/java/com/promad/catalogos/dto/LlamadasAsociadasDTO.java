package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class LlamadasAsociadasDTO extends BaseDTO {

	@Element("id_evento_recurrente")
	private Integer idEventoRecurrente;
	@Element("fecha")
	private String fecha;
	@Element("folio")
	private String folio;
	@Element("nombre_razon_evento_recurrente")
	private String nombreEventoRecurrente;
	@Element("tipo")
	private String tipo;
	@Element("lugar")
	private String lugar;
	@Element("motivo")
	private String motivo;
	@Element("numero_telefono")
	private String numeroTelefonico;
	@Element("fecha")
	private String fechaInicio;
	@Element("fecha_fin")
	private String fechaFin;
	@Element("nombre_razon_evento_recurrente")
	private String razon;
	@Element("recibida_por")
	private String recibidaPor;
	@Element("t_captura")
	private String tiempoCaptura;
	@Element("nombre_motivo")
	private String nombreMotivo;
	@Element("tipo_recurrente")
	private String tipoRecurrente;

	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}

	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getRazon() {
		return razon;
	}

	public void setRazon(String razon) {
		this.razon = razon;
	}

	public String getRecibidaPor() {
		return recibidaPor;
	}

	public void setRecibidaPor(String recibidaPor) {
		this.recibidaPor = recibidaPor;
	}

	public String getTiempoCaptura() {
		return tiempoCaptura;
	}

	public void setTiempoCaptura(String tiempoCaptura) {
		this.tiempoCaptura = tiempoCaptura;
	}

	public Integer getIdEventoRecurrente() {
		return idEventoRecurrente;
	}

	public void setIdEventoRecurrente(Integer idEventoRecurrente) {
		this.idEventoRecurrente = idEventoRecurrente;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getNombreEventoRecurrente() {
		return nombreEventoRecurrente;
	}

	public void setNombreEventoRecurrente(String nombreEventoRecurrente) {
		this.nombreEventoRecurrente = nombreEventoRecurrente;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public String getTipoRecurrente() {
		return tipoRecurrente;
	}

	public void setTipoRecurrente(String tipoRecurrente) {
		this.tipoRecurrente = tipoRecurrente;
	}

}
