package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ColorTezDTO {

	@Id
	@Element("id_color_tez")
	private Integer idColorTez;
	@Element("color_tez")
	private String colorTez;

	private String uuid;
	private Integer modificadoPor;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getColorTez() {
		return colorTez;
	}

	public void setColorTez(String colorTez) {
		this.colorTez = colorTez;
	}

	public Integer getIdColorTez() {
		return idColorTez;
	}

	public void setIdColorTez(Integer idColorTez) {
		this.idColorTez = idColorTez;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
