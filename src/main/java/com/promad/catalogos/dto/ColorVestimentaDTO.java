package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ColorVestimentaDTO {

	@Id
	@Element("id_color")
	private Integer idColorVestimenta;
	@Element("nombre_color")
	private String colorVestimenta;
	private Integer modificadoPor;
	private String uuid;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getColorVestimenta() {
		return colorVestimenta;
	}

	public void setColorVestimenta(String colorVestimenta) {
		this.colorVestimenta = colorVestimenta;
	}

	public Integer getIdColorVestimenta() {
		return idColorVestimenta;
	}

	public void setIdColorVestimenta(Integer idColorVestimenta) {
		this.idColorVestimenta = idColorVestimenta;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
