package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class CatalogoMotivoDTO extends BaseDTO {
	@Id
	@Element("id_motivo")
	private Integer idMotivo;
	@Element("nombre_motivo")
	private String nombreMotivo;
	@Element("id_clasificacion")
	private Integer idClasificacion;
	@Element("nombre_clasificacion")
	private String nombreClasificacion;
	@Element("id_catalogo_nacional")
	private Integer idCatalogoNacional;
	@Element("nombre_motivo_nal")
	private String nombreMotivoNal;
	@Element("prioridad")
	private String prioridad;
	@Element("clave_operativa")
	private String claveOperativa;

	public Integer getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Integer idMotivo) {
		this.idMotivo = idMotivo;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public Integer getIdClasificacion() {
		return idClasificacion;
	}

	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}

	public String getNombreClasificacion() {
		return nombreClasificacion;
	}

	public void setNombreClasificacion(String nombreClasificacion) {
		this.nombreClasificacion = nombreClasificacion;
	}

	public Integer getIdCatalogoNacional() {
		return idCatalogoNacional;
	}

	public void setIdCatalogoNacional(Integer idCatalogoNacional) {
		this.idCatalogoNacional = idCatalogoNacional;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getClaveOperativa() {
		return claveOperativa;
	}

	public void setClaveOperativa(String claveOperativa) {
		this.claveOperativa = claveOperativa;
	}

	public String getNombreMotivoNal() {
		return nombreMotivoNal;
	}

	public void setNombreMotivoNal(String nombreMotivoNal) {
		this.nombreMotivoNal = nombreMotivoNal;
	}

}
