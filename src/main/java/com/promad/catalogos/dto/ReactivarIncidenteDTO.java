package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class ReactivarIncidenteDTO {

	@Element("id_institucion")
	private Integer idInstitucion;
	@Element("nombre_institucion")
	private String nombreInstitucion;
	@Element("direccion_ip")
	private String direccionIp;
	@Element("observaciones")
	private String observaciones;
	@Element("id_tipo_mov_bitacora")
	private Integer idTipoMovimientoBitacora;
	@Element("uuid")
	private String uuid;
	@Element("id_evento")
	private Integer idEvento;
	@Element("modificado_por")
	private Integer modificadoPor;
	@Element("creado_por")
	private Integer creadoPor;
	@Element("nombre_tipo_mov_bitacora")
	private String nombreTipoMovimientoBitacora;
	@Element("url_imagen")
	private String urlImagen;

	private String idInstitucionesX;

	public Integer getIdInstitucion() {
		return idInstitucion;
	}

	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getDireccionIp() {
		return direccionIp;
	}

	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getIdTipoMovimientoBitacora() {
		return idTipoMovimientoBitacora;
	}

	public void setIdTipoMovimientoBitacora(Integer idTipoMovimientoBitacora) {
		this.idTipoMovimientoBitacora = idTipoMovimientoBitacora;
	}

	public String getNombreTipoMovimientoBitacora() {
		return nombreTipoMovimientoBitacora;
	}

	public void setNombreTipoMovimientoBitacora(String nombreTipoMovimientoBitacora) {
		this.nombreTipoMovimientoBitacora = nombreTipoMovimientoBitacora;
	}

	public String getIdInstitucionesX() {
		return idInstitucionesX;
	}

	public void setIdInstitucionesX(String idInstitucionesX) {
		this.idInstitucionesX = idInstitucionesX;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

}
