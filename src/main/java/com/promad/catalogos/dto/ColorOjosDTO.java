package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class ColorOjosDTO {

	@Id
	@Element("id_color_ojos")
	private Integer idColorOjos;
	@Element("nombre_color_ojos")
	private String nombreColorOjos;
	private String uuid;
	private Integer modificadoPor;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNombreColorOjos() {
		return nombreColorOjos;
	}

	public void setNombreColorOjos(String nombreColorOjos) {
		this.nombreColorOjos = nombreColorOjos;
	}

	public Integer getIdColorOjos() {
		return idColorOjos;
	}

	public void setIdColorOjos(Integer idColorOjos) {
		this.idColorOjos = idColorOjos;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
