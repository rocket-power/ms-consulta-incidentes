package com.promad.catalogos.dto;

import com.promad.annotations.Element;

public class TipoTelefonoDTO extends BaseDTO {

	@Element("id_tipo_telefono")
	private Integer idTipoTelefono;
	@Element("nombre_tipo_telefono")
	private String tipoTelefono;

	public Integer getIdTipoTelefono() {
		return idTipoTelefono;
	}

	public void setIdTipoTelefono(Integer idTipoTelefono) {
		this.idTipoTelefono = idTipoTelefono;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}

}
