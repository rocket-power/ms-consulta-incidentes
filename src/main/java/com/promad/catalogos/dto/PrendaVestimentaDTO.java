package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class PrendaVestimentaDTO {

	@Id
	@Element("id_vestimenta")
	private Integer idVestimenta;
	@Element("nombre_vestimenta")
	private String nombreVestimenta;
	@Element("porcion_vestimenta")
	private String porcionVestimenta;
	private Integer modificadoPor;
	private String uuid;
	private Integer creadoPor;

	public Integer getIdVestimenta() {
		return idVestimenta;
	}

	public void setIdVestimenta(Integer idVestimenta) {
		this.idVestimenta = idVestimenta;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNombreVestimenta() {
		return nombreVestimenta;
	}

	public void setNombreVestimenta(String nombreVestimenta) {
		this.nombreVestimenta = nombreVestimenta;
	}

	public String getPorcionVestimenta() {
		return porcionVestimenta;
	}

	public void setPorcionVestimenta(String porcionVestimenta) {
		this.porcionVestimenta = porcionVestimenta;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
