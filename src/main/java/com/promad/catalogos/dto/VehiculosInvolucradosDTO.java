package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class VehiculosInvolucradosDTO {
	@Element("revision")
	private String revision;

	@Id
	@Element("id_evento")
	private Integer idEvento;
	@Id
	@Element("id_vehiculo_involucrado")
	private Integer idVehiculoInvolucrado;
	@Element("numero_placa")
	private String numeroPlaca;
	@Element("numero_serie")
	private String numeroSerie;
	@Element("anio")
	private String anio;
	@Element("id_marca")
	private Integer idMarca;
	@Element("marca")
	private String marca;
	@Element("id_modelo")
	private Integer idModelo;
	@Element("modelo")
	private String modelo;
	@Element("id_color")
	private Integer idColor;
	@Element("nombre_color")
	private String color;
	@Element("id_tipo_vehiculo")
	private Integer idTipoVehiculo;
	@Element("nombre_tipo")
	private String nombreTipo;
	@Element("observacionesveh")
	private String observacionesVeh;
	@Id
	@Element("id_estatus_rastreo")
	private Integer idEstatusRastreo;
	@Element("nombre_estatus_rastreo")
	private String nombreEstatusRastreo;
	@Element("vin_vehicle_identification_num")
	private String niv;
	@Element("numero_motor")
	private String numeroMotor;
	@Element("id_estado")
	private Integer idEstado;
	@Element("nombre_estado")
	private String nombreEstado;
	@Element("id_motivo_cambio_rastreo")
	private Integer idMotivoCambioRastreo;
	@Element("nombre_motivo")
	private String nombreMotivo;
	@Element("lista_negra")
	private String listaNegra;
	@Element("folio")
	private String folio;
	@Element("motivo")
	private String motivo;
	@Element("uuid")
	private String uuid;
	@Element("creado_por")
	private Integer creadoPor;
	@Element("modificado_por")
	private Integer modificadoPor;
	@Element("senias_particulares")
	private String seniasParticulares;
	@Element("NUMERO_AVERIGUACION")
	private String numeroAveriguacion;

	public String getNumeroAveriguacion() {
		return numeroAveriguacion;
	}

	public void setNumeroAveriguacion(String numeroAveriguacion) {
		this.numeroAveriguacion = numeroAveriguacion;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getListaNegra() {
		return listaNegra;
	}

	public void setListaNegra(String listaNegra) {
		this.listaNegra = listaNegra;
	}

	public Integer getIdMotivoCambioRastreo() {
		return idMotivoCambioRastreo;
	}

	public void setIdMotivoCambioRastreo(Integer idMotivoCambioRastreo) {
		this.idMotivoCambioRastreo = idMotivoCambioRastreo;
	}

	public String getNombreMotivo() {
		return nombreMotivo;
	}

	public void setNombreMotivo(String nombreMotivo) {
		this.nombreMotivo = nombreMotivo;
	}

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNiv() {
		return niv;
	}

	public void setNiv(String niv) {
		this.niv = niv;
	}

	public Integer getIdEstatusRastreo() {
		return idEstatusRastreo;
	}

	public void setIdEstatusRastreo(Integer idEstatusRastreo) {
		this.idEstatusRastreo = idEstatusRastreo;
	}

	public String getNombreEstatusRastreo() {
		return nombreEstatusRastreo;
	}

	public void setNombreEstatusRastreo(String nombreEstatusRastreo) {
		this.nombreEstatusRastreo = nombreEstatusRastreo;
	}

	public String getNombreTipo() {
		return nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

	public Integer getIdVehiculoInvolucrado() {
		return idVehiculoInvolucrado;
	}

	public void setIdVehiculoInvolucrado(Integer idVehiculoInvolucrado) {
		this.idVehiculoInvolucrado = idVehiculoInvolucrado;
	}

	public String getNumeroPlaca() {
		return numeroPlaca;
	}

	public void setNumeroPlaca(String numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getIdModelo() {
		return idModelo;
	}

	public void setIdModelo(Integer idModelo) {
		this.idModelo = idModelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getIdColor() {
		return idColor;
	}

	public void setIdColor(Integer idColor) {
		this.idColor = idColor;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	public void setIdTipoVehiculo(Integer idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	public String getObservacionesVeh() {
		return observacionesVeh;
	}

	public void setObservacionesVeh(String observacionesVeh) {
		this.observacionesVeh = observacionesVeh;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getSeniasParticulares() {
		return seniasParticulares;
	}

	public void setSeniasParticulares(String seniasParticulares) {
		this.seniasParticulares = seniasParticulares;
	}

}
