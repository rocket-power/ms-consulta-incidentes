package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class EstaturaDTO {

	@Id
	@Element("id_estatura")
	private Integer idEstatura;
	@Element("estatura")
	private String estatura;
	private Integer modificadoPor;
	private String uuid;
	private Integer creadoPor;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEstatura() {
		return estatura;
	}

	public void setEstatura(String estatura) {
		this.estatura = estatura;
	}

	public Integer getIdEstatura() {
		return idEstatura;
	}

	public void setIdEstatura(Integer idEstatura) {
		this.idEstatura = idEstatura;
	}

	public Integer getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(Integer modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public Integer getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(Integer creadoPor) {
		this.creadoPor = creadoPor;
	}

}
