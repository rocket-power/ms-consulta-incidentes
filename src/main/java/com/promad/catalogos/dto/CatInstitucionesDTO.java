package com.promad.catalogos.dto;

import com.promad.annotations.Element;
import com.promad.annotations.Id;

public class CatInstitucionesDTO extends BaseDTO {
	@Id
	@Element("id_institucion")
	private Integer idInstitucion;
	@Element("nombre_institucion")
	private String nombreInstitucion;

	@Element("url_imagen")
	private String urlImagen;

	@Element("atiende_eventos")
	private String atiendeEventos;
	@Element("atiende_lesionados")
	private String atiendeLesionados;
	@Element("procesa_detenidos")
	private String procesaDetenidos;
	@Element("traslada_personas")
	private String trasladaPersonas;
	@Element("rastrea_vehiculos")
	private String rastreaVehiculos;
	@Element("nombre_municipio")
	private String nombreMunicipio;
	@Element("nombre_estado")
	private String nombreEstado;
	@Element("nombre_localidad")
	private String nombreLocalidad;
	@Element("entre_calle_1")
	private String entreCalle1;
	@Element("entre_calle_2")
	private String entreCalle2;
	@Element("numero_interior")
	private String numeroInterior;
	@Element("id_municipio")
	private Integer idMunicipio;
	@Element("id_localidad")
	private Integer idLocalidad;
	@Element("id_estado")
	private Integer idEstado;

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public Integer getIdInstitucion() {
		return idInstitucion;
	}

	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public String getAtiendeEventos() {
		return atiendeEventos;
	}

	public void setAtiendeEventos(String atiendeEventos) {
		this.atiendeEventos = atiendeEventos;
	}

	public String getAtiendeLesionados() {
		return atiendeLesionados;
	}

	public void setAtiendeLesionados(String atiendeLesionados) {
		this.atiendeLesionados = atiendeLesionados;
	}

	public String getProcesaDetenidos() {
		return procesaDetenidos;
	}

	public void setProcesaDetenidos(String procesaDetenidos) {
		this.procesaDetenidos = procesaDetenidos;
	}

	public String getTrasladaPersonas() {
		return trasladaPersonas;
	}

	public void setTrasladaPersonas(String trasladaPersonas) {
		this.trasladaPersonas = trasladaPersonas;
	}

	public String getRastreaVehiculos() {
		return rastreaVehiculos;
	}

	public void setRastreaVehiculos(String rastreaVehiculos) {
		this.rastreaVehiculos = rastreaVehiculos;
	}

	public String getNombreMunicipio() {
		return nombreMunicipio;
	}

	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}

	public String getNombreEstado() {
		return nombreEstado;
	}

	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}

	public String getNombreLocalidad() {
		return nombreLocalidad;
	}

	public void setNombreLocalidad(String nombreLocalidad) {
		this.nombreLocalidad = nombreLocalidad;
	}

	public String getEntreCalle1() {
		return entreCalle1;
	}

	public void setEntreCalle1(String entreCalle1) {
		this.entreCalle1 = entreCalle1;
	}

	public String getEntreCalle2() {
		return entreCalle2;
	}

	public void setEntreCalle2(String entreCalle2) {
		this.entreCalle2 = entreCalle2;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

}
