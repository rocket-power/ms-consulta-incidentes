package com.promad.catalogos.dto;

import java.util.List;

public class TotalDTO {

	private List<?> total;

	public TotalDTO(List<?> total) {
		super();
		this.total = total;
	}

	public List<?> getTotal() {
		return total;
	}

	public void setTotal(List<?> total) {
		this.total = total;
	}

}
