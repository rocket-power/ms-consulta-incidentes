package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.DatosIncidenteDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.IDatosIncidenteService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "datos Incidente")
@RequestMapping("api/consultaIncidentes")
public class DatosIncidenteController {

	@Autowired
	IDatosIncidenteService iDatosIncidente;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Me9todo que obtiene datos del incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/datosIncidente-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerDatosIncidente(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id del evento es requerido");
			return this.iDatosIncidente.obtenerDatosIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene dtos del incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/descripcion-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarDatosIncidente(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iDatosIncidente.insertarDatosIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));

		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que actualiza los datos del incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/datosIncidente-actualizar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarDatosIncidente(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id evento es requerido");
			return this.iDatosIncidente.actualizarDatosIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos historial de georeferencias", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/historialGeoreferencias-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerHistorialGeoreferencias(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id del evento es requerido");
			return this.iDatosIncidente.obtenerHistorialGeoreferencias(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos del tipo de origen del evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/origen-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerOrigen(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iDatosIncidente.obtenerOrigen(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos del incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/resultadoIncidente-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerResultadoIncidente(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id evento no debe ser nulo");
			return this.iDatosIncidente.obtenerResultadoIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que complementa descripciones de el lugar del incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/descripcionLugar-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarDescripcionLugarIncidente(@RequestBody DatosIncidenteDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id evento no debe ser nulo");
			return this.iDatosIncidente.insertarDescripcionLugarIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
