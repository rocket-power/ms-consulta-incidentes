package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatalogoMunicipioDTO;
import com.promad.catalogos.service.ICatalogoMunicipioService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value ="catalogo Municipio")
@RequestMapping("api/consultaIncidentes")
public class CatalogoMunicipioController {

	@Autowired
	ICatalogoMunicipioService iCatalogoMunicipioService;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de municipio", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/municipio-obtener")
	@RequestMapping(value = "/municipio-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerMunicipio(@RequestBody CatalogoMunicipioDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iCatalogoMunicipioService.obtenerCatMunicipio(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de municipio por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/municipio-idBusqueda")
	@RequestMapping(value = "/municipio-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> busquedaIdMunicipio(@RequestBody CatalogoMunicipioDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMunicipio()).isnull("El id municipio es requerido");
			return this.iCatalogoMunicipioService.busquedaIdCatMunicipio(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de Estado", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/estado-obtener")
	@RequestMapping(value = "/estado-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerEstado(@RequestBody CatalogoMunicipioDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iCatalogoMunicipioService.obtenerCatEstado(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de centro", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/centro-obtener")
	@RequestMapping(value = "/centro-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatalogoCentro(@RequestBody CatalogoMunicipioDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iCatalogoMunicipioService.obtenerCatalogoCentro(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
