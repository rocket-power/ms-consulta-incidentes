package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ContadorPavoDTO;
import com.promad.catalogos.service.IContadorPavoService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "contador Pavo")
@RequestMapping("api/consultaIncidentes")
public class ContadorPavoController {

	@Autowired
	private IContadorPavoService iContadorPavoService;

	@ApiOperation(value = "Metodo que Devuele el total de personas, armas, vehiculos y objetos involucrados, relacionado a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	
	@RequestMapping(value = "/contadorPavo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public  Mono<ResponseEntity<String>> obtenerContadorPavo(@RequestBody ContadorPavoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty("El atributo uuid es invalido");
			Validator.validate(dto.getIdEvento()).isnull("El id del evento es requerido");
			return this.iContadorPavoService.obtenerContadorPavo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
}
}
