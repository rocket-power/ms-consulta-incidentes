package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.VehiculosInvolucradosDTO;
import com.promad.catalogos.service.IVehiculosInvolucradosService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "vehiculos Involucrados")
@RequestMapping("api/consultaIncidentes")
public class VehiculosInvolucradosController {

	@Autowired
	IVehiculosInvolucradosService iVehiculoInvolucrado;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene datos del vehiculo involucrado", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/vehiculoInvolucrado-obtener")
	@RequestMapping(value = "/vehiculoInvolucrado-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerVehiculoInvolucrado(@RequestBody VehiculosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iVehiculoInvolucrado.obtenerVehiculoInvolucrado(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos del vehiculo involucrado", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/vehiculoInvolucrado-insertar")
	@RequestMapping(value = "/vehiculoInvolucrado-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarVehiculoInvolucrado(@RequestBody VehiculosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iVehiculoInvolucrado.insertarVehiculoInvolucrado(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que busca un parametro de vehiculo involucrado por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/vehiculoInvolucrado-idBusqueda")
	@RequestMapping(value = "/vehiculoInvolucrado-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerVehiculoInvolucradoId(@RequestBody VehiculosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdVehiculoInvolucrado()).isnull("El id vehiculo involucrado es requerido");
			return this.iVehiculoInvolucrado.busquedaIdVehiculoInvolucrado(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene un arreglo de audios", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/vehiculoInvolucradoAnios-obtener")
	@RequestMapping(value = "/vehiculoInvolucradoAnios-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerAniosVehiculoInvolucrado(@RequestBody VehiculosInvolucradosDTO dto) {
		try {
			return this.iVehiculoInvolucrado.obtenerAnios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo para actualizar vehiculos involucrados por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/vehiculoInvolucrado-actualizar")
	@RequestMapping(value = "/vehiculoInvolucrado-actualizar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarVehiculoInvolucrado(@RequestBody VehiculosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdVehiculoInvolucrado()).isnull("El id vehiculo involucrado es requerido");
			return this.iVehiculoInvolucrado.actualizarVehiculoInvolucrado(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
