package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ObjetosInvolucradosDTO;
import com.promad.catalogos.service.IObjetosInvolucradosService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "objetos Involucrados")
@RequestMapping("api/consultaIncidentes")
public class ObjetosInvolucradosController {

	@Autowired
	private IObjetosInvolucradosService objetoInvolucrado;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Servicio que obtiene los objetos involucrados en un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/objetosInvolucrados-obtener")
	@RequestMapping(value = "/objetosInvolucrados-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerObjetosInvolucrados(@RequestBody ObjetosInvolucradosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.objetoInvolucrado.obtenerObjetosInvolucrados(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Servicio que inserta objetos involucrados a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/objetosInvolucrados-insertar")
	@RequestMapping(value = "/objetosInvolucrados-insertar",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarObjetosInvolucrados(@RequestBody ObjetosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.objetoInvolucrado.insertarObjetosInvolucrados(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Servicio que inserta objetos involucrados a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/objetosInvolucrados-actualizar")
	@RequestMapping(value = "/objetosInvolucrados-actualizar",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarObjetosInvolucrados(@RequestBody ObjetosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.objetoInvolucrado.actualizarObjetosInvolucrados(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Servicio que obtiene un objeto involucrado en un evento por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/objetosInvolucrados-idBusqueda")
	@RequestMapping(value = "/objetosInvolucrados-idBusqueda",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerObjetosInvolucradosId(@RequestBody ObjetosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.objetoInvolucrado.obtenerObjetosInvolucradosId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Servicio que obtiene el catalogo de tipo moneda", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/catalogoTipoMoneda-obtener")
	@RequestMapping(value = "/catalogoTipoMoneda-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)

	public Mono<ResponseEntity<String>> obtenerCatalogoTipoMoneda(@RequestBody ObjetosInvolucradosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.objetoInvolucrado.obtenerCatalogoTipoMoneda(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
