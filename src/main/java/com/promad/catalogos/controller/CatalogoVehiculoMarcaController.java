package com.promad.catalogos.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatalogoVehiculoMarcaDTO;
import com.promad.catalogos.service.ICatalogoMarcaVehiculo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "catalogo Vehiculo Marca")
@RequestMapping("api/consultaIncidentes")
public class CatalogoVehiculoMarcaController {

	@Autowired
	private ICatalogoMarcaVehiculo iCatalogoMarcaVehiculo;

	protected static final String UUID = "Uuid no puede ser nulo";

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de marca de vehiculo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/marcaVehiculo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerAntiguedadVehiculo(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatalogoVehiculoMarcaDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatalogoMarcaVehiculo.obtenerCatalogoMarcaVehiculo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de modelo de vehiculo por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/marcaVehiculo-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatalogoMarcaVehiculoPorId(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatalogoVehiculoMarcaDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatalogoMarcaVehiculo.obtenerCatalogoMarcaVehiculoPorId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
