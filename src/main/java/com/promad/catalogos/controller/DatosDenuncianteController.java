package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.promad.catalogos.dto.DatosDenuncianteDTO;
import com.promad.catalogos.service.IDatosDenuncianteService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "datos Denunciante")
@RequestMapping("api/consultaIncidentes")
public class DatosDenuncianteController {

	@Autowired
	private IDatosDenuncianteService iDatosDenuncianteService;

	String mensaje = "El atributo es invalido";

	@ApiOperation(value = "Metodo que obtine los datos de los denunciantes relacionados a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/datosDenunciante-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerDatosDenunciante(@RequestBody DatosDenuncianteDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty("El atributo uuid es invalido");
			return this.iDatosDenuncianteService.obtenerDatosDenunciante(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que actualiza los datos de los denunciantes relacionados a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/datosDenunciante-actualizar")
	@RequestMapping(value = "/datosDenunciante-actualizar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarDatosDenunciante(@RequestBody DatosDenuncianteDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty("El atributo uuid es invalido");
			Validator.validate(dto.getIdContacto()).isnull("El atributo idContacto no puede ser nulo");
			return this.iDatosDenuncianteService.actualizarDatosDenunciante(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
