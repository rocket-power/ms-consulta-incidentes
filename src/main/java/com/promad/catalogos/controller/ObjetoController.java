package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ObjetosDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.ICatalogoObjetosService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "objeto")
@RequestMapping("api/consultaIncidentes")
public class ObjetoController {

	@Autowired
	private ICatalogoObjetosService iObjetosService;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Obtener objeto", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/objeto-obtener")
	public Mono<ResponseEntity<String>> obtenerObjeto(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iObjetosService.obtenerObjeto(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Obtener Objeto por Id", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/objeto-idBusqueda")
	public Mono<ResponseEntity<String>> obtenerObjetoId(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdObjeto()).isnull("El atributo id objeto es requerido");
			return this.iObjetosService.obtenerObjetoId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Obtener tipo objeto", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/tipoObjeto-obtener")
	public Mono<ResponseEntity<String>> obtenerTipoObjeto(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iObjetosService.obtenerTipoObjeto(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Obtener tipo Objeto por Id", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/tipoObjeto-idBusqueda")
	public Mono<ResponseEntity<String>> tipoObjetoId(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdTipoObjeto()).isnull("El atributo id  objeto es requerido");
			return this.iObjetosService.obtenerIdTipoObjeto(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Obtener categor\u00eda", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/categoria-obtener")
	public Mono<ResponseEntity<String>> obtenerCategoria(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iObjetosService.obtenerCategoria(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Obtener tipo Objeto por Id", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/categoria-idBusqueda")
	public Mono<ResponseEntity<String>> obtenerIdCategoria(
			@RequestBody ObjetosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdCategoria()).isnull("El atributo id  categoria es requerido");
			return this.iObjetosService.obtenerIdCategoria(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
