package com.promad.catalogos.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.MediosDTO;
import com.promad.catalogos.dto.ServidorDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.IMediosService;
import com.promad.catalogos.utils.FTPUploader;
import com.promad.catalogos.utils.Validator;
import com.promad.service.AnnotationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "medios")
@RequestMapping("api/consultaIncidentes")
public class MediosController {

	@Autowired
	IMediosService iMediosService;

	protected static final String UUID = " El atributo uuid es invalido";
	protected static final String IDMEDIO = " El id del medio es requerido ";

	@ApiOperation(value = "Metodo que obtiene datos de medios de imagen, audio y video", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/servidor-obtener")
	public Mono<ResponseEntity<String>> obtenerServidor(
			@RequestBody ServidorDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iMediosService.obtenerServidor(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos de medios de imagen, audio y video", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-obtener")
	public Mono<ResponseEntity<String>> obtenerMedios(
			@RequestBody MediosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iMediosService.obtenerMedios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que descarga un medio", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-descarga")
	public Mono<ResponseEntity<String>> descargaMedio(
			@RequestBody MediosDTO dto,HttpServletResponse response) throws IOException {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Mono<String> json = iMediosService.busquedaIdMedios(dto);
			ServidorDTO ser = new ServidorDTO();
			ser.setUuid(dto.getUuid());
			Mono<String> server = iMediosService.obtenerServidor(ser);
			List<MediosDTO> list = AnnotationService.getListData(json.toString(), MediosDTO.class);
			List<ServidorDTO> lista = AnnotationService.getListData(server.toString(), ServidorDTO.class);
			FTPClient ftp = FTPUploader.connectFTP(lista.get(0).getUrl(), lista.get(0).getUser(),
					lista.get(0).getPwd());
			byte[] bFile = null;
			ByteArrayOutputStream myBarray = new ByteArrayOutputStream();
			if (ftp.retrieveFile(list.get(0).getPath(), myBarray)) {
				bFile = myBarray.toByteArray();
				response.setContentLengthLong(bFile.length);
			} else {
				json = Mono.just("Error : al descargar el archivo");
			}
			response.getOutputStream().write(bFile);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			return null;
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene datos de medios de imagen, audio y video", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-insertar")
	public Mono<ResponseEntity<String>> insertarMedios(
			@RequestBody MediosDTO dto) throws IOException {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iMediosService.insertarMedios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que actualiza los datos de medios de imagen, audio y video", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-actualizar")
	public Mono<ResponseEntity<String>> actualizarMedios(
			@RequestBody MediosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMedioAsociado()).isnull(IDMEDIO);
			Validator.validate(dto.getNombre()).isEmpty("El nombre es requerido");
			return this.iMediosService.actualizarMedios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que elimina los datos de medios de audio, imagen o video por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-eliminar")
	public Mono<ResponseEntity<String>> eliminarMedios(
			@RequestBody MediosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMedioAsociado()).isnull(IDMEDIO);
			return this.iMediosService.eliminarMedios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que busca un parametro de los datos de medios de audio, imagen o video por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/medios-idBusqueda")
	public Mono<ResponseEntity<String>> obtenerMediosPorId(
			@RequestBody MediosDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMedioAsociado()).isnull("El id del medio es requerido");
			return this.iMediosService.busquedaIdMedios(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
