package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.promad.catalogos.dto.InstitucionesAsignadasDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.IInstitucionesAsignadasService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "instituciones Asignadas")
@RequestMapping("api/consultaIncidentes")
public class InstitucionesAsignadasController {

	@Autowired
	private IInstitucionesAsignadasService iInstitucionesAsignadasService;
	
	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtine toda la informacion de las instituciones asignadas a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/institucionesAsignadas-obtener")
	public Mono<ResponseEntity<String>> obtenerInstitucionesAsignadas(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iInstitucionesAsignadasService.obtenerInstitucionesAsignadas(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtine toda la informacion detallada de una institucion asignada a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/detalleInstitucion-obtener")
	public Mono<ResponseEntity<String>> obtenerDetalleInstitucion(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iInstitucionesAsignadasService.obtenerDetalleInstitucion(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtine todos los recursos asignados a una institucion de un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/recursosAsignados-obtener")
	public Mono<ResponseEntity<String>> obtenerRecursosAsignados(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iInstitucionesAsignadasService.obtenerRecursosAsignados(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que crea una nota de cierre a una institucion asignada a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/notasCierre-insertar")
	public Mono<ResponseEntity<String>> insertarNotasCierre(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iInstitucionesAsignadasService.insertarNotasCierre(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtine un recurso asignado a una institucion de un evento por id de institucion asignada", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/recursosAsignadosId-obtener")
	public Mono<ResponseEntity<String>> obtenerRecursosAsignadosId(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdInstitucionAsignada()).minimo("El Id institucion Asignada es Requerido");
			return this.iInstitucionesAsignadasService.obtenerRecursosAsignadosId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que asigna una nueva institucion a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/institucionesAsignadas-insertar")
	public Mono<ResponseEntity<String>> insertarInstitucionEvento(
			@RequestBody InstitucionesAsignadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdInstitucionS()).isEmpty("El Id institucion es requerido");
			return this.iInstitucionesAsignadasService.insertarInstitucionEvento(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtine toda la informacion de las instituciones asignadas a un evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/institucionesNoAsignadas-obtener")
	public Mono<ResponseEntity<String>> obtenerInstitucionesNoAsignadas(@RequestBody InstitucionesAsignadasDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iInstitucionesAsignadasService.obtenerInstitucionesNoAsignadas(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
