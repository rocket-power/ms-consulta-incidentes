package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.promad.catalogos.dto.ReporteDescriptivoDTO;
import com.promad.catalogos.service.IReporteDescriptivoService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "reporte Descriptivo")
@RequestMapping("api/consultaIncidentes")
public class ReporteDescriptivoController {

	@Autowired
	private IReporteDescriptivoService iDescriptivoService;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene datos del catalogo turno", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/datosIncidenteReporte-obtener")
	public Mono<ResponseEntity<String>> obtenerReporteDescriptivo(@RequestBody ReporteDescriptivoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.iDescriptivoService.obtenerReporteDescriptivo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
		
	}

	@ApiOperation(value = "Metodo que obtiene datos del catalogo datos Incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tiemposIncidednte-obtener")
	public Mono<ResponseEntity<String>> obtenerTiemposIncidente(@RequestBody ReporteDescriptivoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID).isEspecial(UUID);
			return this.iDescriptivoService.obtenerTiemposIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
		
	}

}