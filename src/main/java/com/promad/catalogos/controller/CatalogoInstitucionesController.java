
package com.promad.catalogos.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatInstitucionesDTO;
import com.promad.catalogos.service.ICatInstitucionesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value="catalogo Instituciones")
@RequestMapping("api/consultaIncidentes")
public class CatalogoInstitucionesController {

	@Autowired
	private ICatInstitucionesService iCatInstitucionesService;

	protected static final String UUID = "Uuid no puede ser nulo";

	@ApiOperation(value = "Metodo que obtiene datos del catalogo turno", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catInstituciones-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatalogoInstituciones(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatInstitucionesDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatInstitucionesService.obtenerCatalogoInstituciones(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que inserta datos al catalogo de Instituciones", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catInstituciones-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarCatalogoInstituciones(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatInstitucionesDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatInstitucionesService.insertarCatalogoInstituciones(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que actualiza datos al catalogo de Instituciones", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catInstituciones-actualizar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarCatalogoInstituciones(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatInstitucionesDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatInstitucionesService.actualizarCatalogoInstituciones(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que busca la institucion por Id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catInstituciones-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatalogoInstitucionesId(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatInstitucionesDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatInstitucionesService.obtenerCatInstitucionId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene las instituciones que atienden Lesionados, detenidos, enfermos, etc.", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catInstitucionesAtienden-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatalogoInstitucionesAtienden(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatInstitucionesDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatInstitucionesService.obtenerCatalogoInstitucionesAtienden(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
