package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatalogoSupervisoraDTO;
import com.promad.catalogos.dto.CatalogoTipoVehiculoDTO;
import com.promad.catalogos.dto.CatalogosVehiculosDTO;
import com.promad.catalogos.dto.EstatusRastreoDTO;
import com.promad.catalogos.service.ICatalogoMotCambioEstatusService;
import com.promad.catalogos.service.ICatalogoTipoVehiculoService;
import com.promad.catalogos.service.ICatalogoVehiculoService;
import com.promad.catalogos.service.IEstatusRastreoService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value ="catalogo Vehiculo")
@RequestMapping("api/consultaIncidentes")
public class CatalogoVehiculoController {

	@Autowired
	ICatalogoVehiculoService iCatalogoVehiculoService;
	@Autowired
	IEstatusRastreoService iEstatusRastreoService;
	@Autowired
	ICatalogoTipoVehiculoService tipoVehiculoService;
	@Autowired
	ICatalogoMotCambioEstatusService cambioRastreo;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de color de vehiculo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catColorVehiculo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatColorVehiculo(@RequestBody CatalogosVehiculosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iCatalogoVehiculoService.obtenerCatColorVehiculo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de color de vehiculo por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catColorVehiculo-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerColorVehiculosPorId(@RequestBody CatalogosVehiculosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdColor()).isnull("El id color de vehiculo es requerido");
			return this.iCatalogoVehiculoService.obtenerCatColorVehiculoPorId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de tipo de vehiculo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "catTipoVehiculo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatTipoVehiculo(@RequestBody CatalogoTipoVehiculoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.tipoVehiculoService.obtenerCatTipoVehiculo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de tipo de vehiculo por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catTipoVehiculo-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatTipoVehiculoPorId(@RequestBody CatalogoTipoVehiculoDTO dto) {

		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdTipoVehiculo()).isnull("El id tipo vehiculo es requerido");
			return this.tipoVehiculoService.obtenerCatTipoVehiculoPorId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de estatus rastreo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catEstatusRastreo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatEstatusRastreo(@RequestBody EstatusRastreoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iEstatusRastreoService.obtenerCatEstatusRastreo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de estatus rastreo por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/catEstatusRastreo-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerestatusRastreoPorId(@RequestBody EstatusRastreoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEstatusRastreo()).isnull("El id estatus rastreo es requerido");

			return this.iEstatusRastreoService.obtenerCatEstatusRastreoPorId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los motivos de cambio rastreo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/motivoCambioRastreo-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCmbiorastreoRastreo(@RequestBody CatalogoSupervisoraDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.cambioRastreo.obtenerCatalogoMotCambioEst(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los motivos de cambio rastreo por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/motivoCambioRastreo-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCambioRastreoId(@RequestBody CatalogoSupervisoraDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMotivoCambioRastreo()).isnull("El id estatus rastreo es requerido");

			return this.cambioRastreo.obtenerMotCambioEstatRastreoId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}

	}
}