
package com.promad.catalogos.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.BitacoraDTO;
import com.promad.catalogos.dto.WebhookMessageDTO;
import com.promad.catalogos.service.IBitacoraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value ="Bitacora")
@RequestMapping("")
public class BitacoraController {

	@Autowired
	IBitacoraService iBitacoraService;

	protected static final String UUID = "Uuid no puede ser nulo";

	@ApiOperation(value = "Metodo que obtiene todos los datos de bitacora", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/bitacora-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerBitacora(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody BitacoraDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iBitacoraService.obtenerBitacora(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los tiempos del Evento", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/tiemposEvento-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerTiemposEvento(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody BitacoraDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iBitacoraService.obtenerTiemposEvento(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
	
	@GetMapping("/test/json")
	@RequestMapping(value = "/test/json", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public WebhookMessageDTO test() {
			WebhookMessageDTO mensaje = new WebhookMessageDTO();			
			mensaje.setMensaje("hola Desde el servidor");
			mensaje.setNombre("Emmanuel");
			mensaje.setEdad(23);
			return mensaje;
		
	}
	

}
