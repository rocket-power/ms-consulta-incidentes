package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.LlamadasAsociadasDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.LlamadasAsociadasService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "llamadas Asociadas")
@RequestMapping("api/consultaIncidentes")
public class LlamadasAsociadasController {

	@Autowired
	LlamadasAsociadasService llamadas;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene todas las llamadas asociadas a un evento", notes = "Requiere uuid es requerido", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/llamadasAsociadas-obtener")
	public Mono<ResponseEntity<String>> obtenerLlamadasAsociadas(
			@RequestBody LlamadasAsociadasDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdEvento()).isnull("El id del evento es requerido");
			return this.llamadas.obtenerLlamadasAsociadas(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException ex) {
			Mono<String> error = Mono.just(ex.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
