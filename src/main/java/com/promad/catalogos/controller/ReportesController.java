package com.promad.catalogos.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.promad.catalogos.service.IParametrosReporteService;
import com.promad.catalogos.service.impl.FactoryReport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

@CrossOrigin()
@Api()
@RestController(value = "reportes")
@RequestMapping("api/consultaIncidentes")
public class ReportesController {
	static Logger logger = LoggerFactory.getLogger(ReportesController.class);
	@Autowired
	private IParametrosReporteService iParametrosReporteService;

	@Autowired
	private FactoryReport factoryReport;

	@ApiOperation(value = "Metodo para la descarga reporte pdf", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@GetMapping("/reporte")
	public ResponseEntity<byte[]> downloadPdf(@RequestParam String nombreReporte, @RequestParam String uuid,
			@RequestParam Integer idEvento, @RequestParam String tipoReporte) {

		byte[] pdfFile = null;
		byte[] excel = null;

		Map<String, Object> param = iParametrosReporteService.parametrosReportes(nombreReporte, uuid, idEvento);

		Map<String, Object> parametrosFinales = factoryReport.parametrosReporte(nombreReporte, param);

		try {
			InputStream in = getClass().getResourceAsStream("/ReporteDescriptivo.jasper");
			JasperReport report = (JasperReport) JRLoader.loadObject(in);

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, (Map<String, Object>) parametrosFinales,
					new JREmptyDataSource());

			if (tipoReporte.equals("pdf")) {
				pdfFile = JasperExportManager.exportReportToPdf(jasperPrint);
				return new ResponseEntity<>(pdfFile,
						getHttpHeaders(nombreReporte + ".pdf", pdfFile.length, MediaType.APPLICATION_PDF),
						HttpStatus.OK);
			} else if (tipoReporte.equals("excel")) {
				JRXlsExporter xlsExporter = new JRXlsExporter();
				ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
				xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));
				SimpleXlsReportConfiguration xlsReportConfiguration = new SimpleXlsReportConfiguration();
				xlsReportConfiguration.setRemoveEmptySpaceBetweenColumns(true);
				xlsReportConfiguration.setDetectCellType(false);
				xlsReportConfiguration.setOnePagePerSheet(false);
				xlsExporter.setConfiguration(xlsReportConfiguration);
				xlsExporter.exportReport();
				excel = xlsReport.toByteArray();
				return new ResponseEntity<>(excel,
						getHttpHeaders(nombreReporte + ".xls", excel.length, MediaType.APPLICATION_PDF), HttpStatus.OK);
			} else {
				return null;
			}

		} catch (JRException ex) {
			throw new RuntimeException("Error :", ex);
		}
	}

	private HttpHeaders getHttpHeaders(String fileName, int length, MediaType mediaType) {
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentLength(length);
		respHeaders.setContentType(mediaType);
		respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
		return respHeaders;
	}

}
