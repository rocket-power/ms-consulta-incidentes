
package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ConsultaIncidentesDTO;
import com.promad.catalogos.service.IConsultaIncidentesService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value="consulta Incidentes")
@RequestMapping("api/consultaIncidentes")
public class ConsultaIncidentesController {

	@Autowired
	IConsultaIncidentesService iConsultaIncidentes;


	@ApiOperation(value = "Metodo que obtiene datos de la consulta de incidentes", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/consutaIncidentes-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerConsultaIncidentes(@RequestBody ConsultaIncidentesDTO dto) {
		try {

			Validator.validate(dto.getTelefono()).minLength(4, "Se debe ingresar al menos 4 digitos del telefono");

			return this.iConsultaIncidentes.obtenerConsultaIncidentes(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
}
}
