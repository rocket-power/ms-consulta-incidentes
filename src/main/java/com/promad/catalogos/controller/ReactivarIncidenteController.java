package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ReactivarIncidenteDTO;
import com.promad.catalogos.service.IReactivarIncidenteService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "reactivar Incidente")
@RequestMapping("api/consultaIncidentes")
public class ReactivarIncidenteController {

	@Autowired
	IReactivarIncidenteService iReactivarIncidenteService;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene todos los datos de reactivar Incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/reactivarIncidente-obtener")
	@RequestMapping(value = "/reactivarIncidente-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerReactivarIncidente(@RequestBody ReactivarIncidenteDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iReactivarIncidenteService.obtenerReactivarIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que inserta los datos del reactivar Incidente", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/reactivarIncidente-insertar")
	@RequestMapping(value = "/reactivarIncidente-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarReactivarIncidente(@RequestBody ReactivarIncidenteDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.iReactivarIncidenteService.insertarReactivarIncidente(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene todos los datos de tipo movimiento bitacora", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tipoMovimientoBitacora-obtener")
	@RequestMapping(value = "/tipoMovimientoBitacora-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)

	public Mono<ResponseEntity<String>> obtenerTipoBitacora(@RequestBody ReactivarIncidenteDTO dto) {
		try {

			return this.iReactivarIncidenteService.obtenerTipoBitacora(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
