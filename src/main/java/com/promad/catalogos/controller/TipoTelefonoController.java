package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.TipoTelefonoDTO;
import com.promad.catalogos.service.ITipoTelefonoService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "tipo Telefono")
@RequestMapping("api/consultaIncidentes")
public class TipoTelefonoController {

	@Autowired
	ITipoTelefonoService tipo;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Obtener los tipos de tel\u00e9fono", notes = "Requiere el uuid", response = ResponseEntity.class, responseReference = "200 OK, 400 ERROR")
	@PostMapping("/tipoTelefono-obtener")
	@RequestMapping(value = "/tipoTelefono-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerTipoTelefonos(@RequestBody TipoTelefonoDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.tipo.obtenerTipoTelefono(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
	
}
