package com.promad.catalogos.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatLocalidadDTO;
import com.promad.catalogos.service.ICatLocalidadService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value ="catalogo Localidad")
@RequestMapping("api/consultaIncidentes")
public class CatalogoLocalidadController {

	@Autowired
	ICatLocalidadService iCatLocalidadService;

	protected static final String UUID = "Uuid no puede ser nulo";

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de localidad", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/localidad-obtener", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerLocalidad(
			@ApiParam(value = "Json de parametros para obtener un dato en turno", required = true) @Valid @RequestBody CatLocalidadDTO dto) {
		Mono<String> errorUuid = Mono.just(UUID);
		if (dto.getUuid() == null) {
			return errorUuid.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.BAD_REQUEST));
		}
		try {
			return this.iCatLocalidadService.obtenerCatLocalidad(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	//
	@ApiOperation(value = "Metodo que inserta  los datos del catalogo de localidad", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/localidad-insertar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> insertarLocalidad(@RequestBody CatLocalidadDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMunicipio()).isnull("El id municipio es requerido");
			return this.iCatLocalidadService.insertarCatLocalidad(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que actualiza los datos del catalogo de Localidad", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/localidad-actualizar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> actualizarLocalidad(@RequestBody CatLocalidadDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdMunicipio()).isnull("El id municipio es requerido");
			Validator.validate(dto.getIdLocalidad()).isnull("El id localidad requerido");

			return this.iCatLocalidadService.actualizarCatLocalidad(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que elimina catalogo de localidad", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@RequestMapping(value = "/localidad-eliminar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> eliminarMunicipio(@RequestBody CatLocalidadDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdLocalidad()).isnull("El id localidad requerido");

			return this.iCatLocalidadService.eliminarCatLocalidad(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

// 
	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo de localidad por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/localidad-idBusqueda")
	@RequestMapping(value = "/localidad-idBusqueda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> busquedaIdLocalidad(@RequestBody CatLocalidadDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);

			return this.iCatLocalidadService.busquedaIdCatLocalidad(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
}
