package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.CatalogoTipoCabelloDTO;
import com.promad.catalogos.dto.CatalogoTipoIdentificacionDTO;
import com.promad.catalogos.dto.CatalogoTipoNarizDTO;
import com.promad.catalogos.dto.CatalogoTipoPersonaInvolucradaDTO;
import com.promad.catalogos.dto.ColorCabelloDTO;
import com.promad.catalogos.dto.ColorOjosDTO;
import com.promad.catalogos.dto.ColorTezDTO;
import com.promad.catalogos.dto.ColorVestimentaDTO;
import com.promad.catalogos.dto.ComplexionDTO;
import com.promad.catalogos.dto.EstaturaDTO;
import com.promad.catalogos.dto.FormaDeCaraDTO;
import com.promad.catalogos.dto.PersonaInvolucradasDTO;
import com.promad.catalogos.dto.PrendaVestimentaDTO;
import com.promad.catalogos.dto.SexoDTO;
import com.promad.catalogos.service.IPersonasInvolucradasService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value="catalogo Persona Involucrada")
@RequestMapping("api/consultaIncidentes")
public class CatalogoPersonaInvolucradaController {

	@Autowired
	private IPersonasInvolucradasService personaservice;
	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtine los datos del catalogo accesorios", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/accesorio-obtener")
	@RequestMapping(value = "/accesorio-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerCatPersonasInvolucradas(@RequestBody PersonaInvolucradasDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obteneAccesorio(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
	
	@ApiOperation(value = "Metodo que obtiene los datos del catalogo colos de ojos", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/colorOjos-obtener")
	@RequestMapping(value = "/colorOjos-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerColorOjos(@RequestBody ColorOjosDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerColorOjos(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo color de tez", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/colorTez-obtener")
	@RequestMapping(value = "/colorTez-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerColorTez(@RequestBody ColorTezDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerColorTez(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo color de cabello", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/colorCabello-obtener")
	@RequestMapping(value = "/colorCabello-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerColorCabello(@RequestBody ColorCabelloDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerColorCabello(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo color de vestimenta", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/colorVestimenta-obtener")
	@RequestMapping(value ="/colorVestimenta-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerColorVestimenta(@RequestBody ColorVestimentaDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerColorVestimenta(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo complexion", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/complexion-obtener")
	@RequestMapping(value = "/complexion-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerComplexion(@RequestBody ComplexionDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerComplexion(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo estatura", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/estatura-obtener")
	@RequestMapping(value = "/estatura-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerEstatura(@RequestBody EstaturaDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerEstatura(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo forma de cara", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/formaCara-obtener")
	@RequestMapping(value = "/formaCara-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerFormaCara(@RequestBody FormaDeCaraDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerFormaCara(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo prenda", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/prenda-obtener")
	@RequestMapping(value = "/prenda-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerPrenda(@RequestBody PrendaVestimentaDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerPrenda(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo genero", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/genero-obtener")
	@RequestMapping(value = "/genero-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerSexo(@RequestBody SexoDTO dto) {
		try {
			return this.personaservice.obtenerSexo()
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogotipo de cabello", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tipoCabello-obtener")
	@RequestMapping(value = "/tipoCabello-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)

	public Mono<ResponseEntity<String>> obtenerTipoCabello(@RequestBody CatalogoTipoCabelloDTO dto) {
		try {

			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerTipoCabello(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogotipo de nariz", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tipoNariz-obtener")
	@RequestMapping(value = "/tipoNariz-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerTipoNariz(@RequestBody CatalogoTipoNarizDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerTipoNariz(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo tipo de identificacion", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tipoIdentificacion-obtener")
	@RequestMapping(value = "/tipoIdentificacion-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerTipoIdentificacion(@RequestBody CatalogoTipoIdentificacionDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerTipoIdentificacion(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}


	@ApiOperation(value = "Metodo que obtiene los datos del catalogo tipo de persona involucrada", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/tipoPersonaInvolucrada-obtener")
	@RequestMapping(value = "/tipoPersonaInvolucrada-obtener",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<String>> obtenerTipoPersonaInvolucrada(@RequestBody CatalogoTipoPersonaInvolucradaDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.personaservice.obtenerTipoPersonaInvolucrada(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (Exception e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}
	
}
