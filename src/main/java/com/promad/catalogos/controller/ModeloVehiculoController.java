package com.promad.catalogos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.promad.catalogos.dto.ModeloVehiculoDTO;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.catalogos.service.IModeloVehiculoService;
import com.promad.catalogos.utils.Validator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import reactor.core.publisher.Mono;

@CrossOrigin()
@Api()
@RestController(value = "modelo Vehiculo")
@RequestMapping("api/consultaIncidentes")
public class ModeloVehiculoController {

	@Autowired
	private IModeloVehiculoService modeloVehiculoService;

	protected static final String UUID = "El atributo uuid es invalido";

	@ApiOperation(value = "Metodo que obtiene todos los datos del catalogo modelo de vehiculo", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/modeloVehiculo-obtener")
	public Mono<ResponseEntity<String>> obtenerModeloVehiculo(@RequestBody ModeloVehiculoDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			return this.modeloVehiculoService.obtenerModeloVehiculo(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

	@ApiOperation(value = "Metodo que obtiene los datos del catalogo de modelo de vehiculo por id", notes = "Requiere uuid", response = ResponseEntity.class, responseReference = "Estatus: 200 - OK, 400 - Error")
	@PostMapping("/modeloVehiculo-idBusqueda")
	public Mono<ResponseEntity<String>> obtenerModeloVehiculoPorId(@RequestBody ModeloVehiculoDTO dto) {
		try {
			Validator.validate(dto.getUuid()).isEmpty(UUID);
			Validator.validate(dto.getIdModelo()).isnull("El id modelo es requerido");
			return this.modeloVehiculoService.obtenerModeloVehiculoId(dto)
					.map(saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.OK))
					.defaultIfEmpty(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
		} catch (CatalogoException e) {
			Mono<String> error = Mono.just(e.getMessage());
			return error.map(
					saveInformcion -> new ResponseEntity<String>(saveInformcion, HttpStatus.INTERNAL_SERVER_ERROR));
		}
	}

}
