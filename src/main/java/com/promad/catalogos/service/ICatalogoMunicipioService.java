package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.CatalogoMunicipioDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoMunicipioService {

	public Mono<String> obtenerCatMunicipio(CatalogoMunicipioDTO dto);

	public Mono<String> busquedaIdCatMunicipio(CatalogoMunicipioDTO dto);

	public Mono<String> obtenerCatEstado(CatalogoMunicipioDTO dto);

	public Mono<List<CatalogoMunicipioDTO>> obtenerCatMunicipioReporte(CatalogoMunicipioDTO dto);

	public Mono<String> obtenerCatalogoCentro(CatalogoMunicipioDTO dto);

}
