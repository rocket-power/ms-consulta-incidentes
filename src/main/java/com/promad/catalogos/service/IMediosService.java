package com.promad.catalogos.service;

import java.io.IOException;

import com.promad.catalogos.dto.MediosDTO;
import com.promad.catalogos.dto.ServidorDTO;

import reactor.core.publisher.Mono;

public interface IMediosService {

	public Mono<String> obtenerMedios(MediosDTO dto);

	public Mono<String> insertarMedios(MediosDTO dto) throws IOException;

	public Mono<String> actualizarMedios(MediosDTO dto);

	public Mono<String> eliminarMedios(MediosDTO dto);

	public Mono<String> busquedaIdMedios(MediosDTO dto);

	public Mono<String> descargaMedio(MediosDTO dto);

	public Mono<String> obtenerServidor(ServidorDTO uuid);

}
