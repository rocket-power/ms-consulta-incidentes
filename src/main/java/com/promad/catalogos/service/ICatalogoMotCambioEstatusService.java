package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatalogoSupervisoraDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoMotCambioEstatusService {
	public Mono< String> obtenerCatalogoMotCambioEst(CatalogoSupervisoraDTO dto);

	public  Mono<String> obtenerMotCambioEstatRastreoId(CatalogoSupervisoraDTO dto);
}
