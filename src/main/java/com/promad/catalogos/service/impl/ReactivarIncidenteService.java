package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.ReactivarIncidenteDTO;
import com.promad.catalogos.service.IReactivarIncidenteService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ReactivarIncidenteService extends BaseService implements IReactivarIncidenteService {
	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.reactivar_incidente}")
	private String reactivarIncidente;

	@Value("${nombre.store.procedure.cat_tipo_mov_bitacora}")
	private String catTipoMovBitacora;

	@Override
	public Mono<String> obtenerReactivarIncidente(ReactivarIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, reactivarIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ReactivarIncidenteDTO.class);
	}

	@Override
	public Mono<String> insertarReactivarIncidente(ReactivarIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, reactivarIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		listaParametros.add(getParametro("observaciones", STRING, dto.getObservaciones()));
		listaParametros.add(getParametro("direccion_ip", STRING, dto.getDireccionIp()));
		listaParametros.add(getParametro("id_Evento", INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro("id_institucion", STRING, dto.getIdInstitucionesX()));
		listaParametros.add(getParametro("id_tipo_mov_bitacora", INTEGER, dto.getIdTipoMovimientoBitacora()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerTipoBitacora(ReactivarIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catTipoMovBitacora);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ReactivarIncidenteDTO.class);
	}
}
