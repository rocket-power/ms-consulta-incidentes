package com.promad.catalogos.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.promad.catalogos.service.IReporteContextDataService;

@Service
@PropertySource("classpath:application.properties")
public class FactoryReport {

	@Autowired
	private IReporteContextDataService iReporteContextDataService;

	public Map<String, Object> parametrosReporte(String nombreReporte, Map<String, ?> param) {

		Map<String, Object> params = iReporteContextDataService.parametrosDataReporte(nombreReporte, param);

		return params;
	}
}
