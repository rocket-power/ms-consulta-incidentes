package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.cliente.ClienteWeb;
import com.promad.catalogos.dto.PersonasIncidenteDTO;
import com.promad.catalogos.service.IConsultaPersonaService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ConsultaPersonaInvolucradaImpl extends BaseService implements IConsultaPersonaService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;
	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;
	@Value("${nombre.paquete.telefonista}")
	private String telefonista;
	@Value("${nombre.store.procedure.personas_involucradas}")
	private String personaEvento;
	@Value("${nombre.store.procedure.persona_involucrada}")
	private String personaInvolucrada;
	@Value("${nombre.store.procedure.cat_accesorios}")
	private String catAccesorios;
	@Value("${nombre.store.procedure.accesorios_persona_inv}")
	private String accesoriosPersonaInv;

	@Value("${spring.application.name}")
	private String nombreMS;
	
	@Autowired
	ClienteWeb clienteWeb;

	private static final String IDEVENTO = "id_evento"; 
	private static final String IDPERSONAINVOLUCRADA = "id_persona_involucrada"; 

	@Override
	public Mono<String> obtenerPersonaEvento(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, personaEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, PersonasIncidenteDTO.class);
	}

	@Override
	public Mono<String> obtenerPersonaInvolucradas(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, personaEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDPERSONAINVOLUCRADA, INTEGER, dto.getIdPersonaInvolucrada()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, PersonasIncidenteDTO.class);
	}

	@Override
	public Mono<String> insertarPersonaInvolucradas(PersonasIncidenteDTO dto) {
		String json;
		
		ParametrosDataDTO parametrosDataDTO = createParametrosData(telefonista, personaInvolucrada);
		List<ParametroDTO> listaParametros = new ArrayList<>();

		if (dto.getRevision() != null) {
			listaParametros.add(getParametro("revision", INTEGER, dto.getRevision()));
		}
		listaParametros.add(getParametro("UUID", STRING, dto.getUuid()));
		if (dto.getDirrecionIp() != null) {
			listaParametros.add(getParametro("direccion_ip", STRING, dto.getDirrecionIp()));
		}
		if (dto.getIdEvento() != null) {

			listaParametros.add(getParametro("ID_EVENTO", INTEGER, dto.getIdEvento()));
		}
		listaParametros.add(getParametro("CREADO_POR", INTEGER, dto.getCreadoPor()));
		if (dto.getNombre() != null) {
			listaParametros.add(getParametro("NOMBRE", STRING, dto.getNombre()));
		}
		if (dto.getApellidoPaterno() != null) {
			listaParametros.add(getParametro("APELLIDO_PATERNO", STRING, dto.getApellidoPaterno()));
		}
		if (dto.getApellidoMaterno() != null) {
			listaParametros.add(getParametro("APELLIDO_MATERNO", STRING, dto.getApellidoMaterno()));
		}
		if (dto.getEdad() != null) {
			listaParametros.add(getParametro("EDAD", INTEGER, dto.getEdad()));
		}
		if (dto.getIdColorCabello() != null) {
			listaParametros.add(getParametro("ID_COLOR_CABELLO", INTEGER, dto.getIdColorCabello()));
		}
		if (dto.getIdColorOjos() != null) {
			listaParametros.add(getParametro("ID_COLOR_OJOS", INTEGER, dto.getIdColorOjos()));
		}
		if (dto.getIdColorTez() != null) {
			listaParametros.add(getParametro("ID_COLOR_TEZ", INTEGER, dto.getIdColorTez()));
		}
		if (dto.getIdEstatura() != null) {
			listaParametros.add(getParametro("ID_ESTATURA", INTEGER, dto.getIdEstatura()));
		}
		if (dto.getIdComplexion() != null) {
			listaParametros.add(getParametro("ID_COMPLEXION", INTEGER, dto.getIdComplexion()));
		}
		if (dto.getEstatura() != null) {
			listaParametros.add(getParametro("ESTATURA", STRING, dto.getEstatura()));
		}
		if (dto.getIdFormaCara() != null) {
			listaParametros.add(getParametro("ID_FORMA_CARA", INTEGER, dto.getIdFormaCara()));
		}
		if (dto.getIdTipoCabello() != null) {
			listaParametros.add(getParametro("ID_TIPO_CABELLO", INTEGER, dto.getIdTipoCabello()));
		}
		if (dto.getIdTipoNariz() != null) {
			listaParametros.add(getParametro("ID_TIPO_NARIZ", INTEGER, dto.getIdTipoNariz()));
		}
		if (dto.getIdTipoPersona() != null) {
			listaParametros.add(getParametro("ID_TIPO_PERSONA", INTEGER, dto.getIdTipoPersona()));
		}
		if (dto.getClave() != null) {
			listaParametros.add(getParametro("SEXO", INTEGER, dto.getClave()));
		}
		if (dto.getAlias() != null) {
			listaParametros.add(getParametro("ALIAS", STRING, dto.getAlias()));
		}
		if (dto.getIdVestimentaTalle() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_TALLE", INTEGER, dto.getIdVestimentaTalle()));
		}
		if (dto.getIdColorVestimentaTalle() != null) {
			listaParametros.add(getParametro("ID_COLOR_VESTIMENTA_TALLE", INTEGER, dto.getIdColorVestimentaTalle()));
		}
		if (dto.getIdVestimentaCabeza() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_CABEZA", INTEGER, dto.getIdVestimentaCabeza()));
		}
		if (dto.getIdColorVestimentaCabeza() != null) {
			listaParametros.add(getParametro("ID_COLOR_VESTIMENTA_CABEZA", INTEGER, dto.getIdColorVestimentaCabeza()));
		}
		if (dto.getIdVestimentaCalzado() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_CALZADO", INTEGER, dto.getIdVestimentaCalzado()));
		}
		if (dto.getIdColorVestimentaCalzado() != null) {
			listaParametros
					.add(getParametro("ID_COLOR_VESTIMENTA_CALZADO", INTEGER, dto.getIdColorVestimentaCalzado()));
		}
		if (dto.getIdVestimentaInferior() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_INFERIOR", INTEGER, dto.getIdVestimentaInferior()));
		}
		if (dto.getIdColorVestimentaInferior() != null) {
			listaParametros
					.add(getParametro("ID_COLOR_VESTIMENTA_INFERIOR", INTEGER, dto.getIdColorVestimentaInferior()));
		}
		if (dto.getSeniaParticular() != null) {
			listaParametros.add(getParametro("SENIA_PARTICULAR", INTEGER, dto.getSeniaParticular()));
		}
		if (dto.getEstatusLectura() != null) {
			listaParametros.add(getParametro("ESTATUS_LECTURA", STRING, dto.getEstatusLectura()));
		}
		if (dto.getFechaLectura() != null) {
			listaParametros.add(getParametro("FECHA_LECTURA", STRING, dto.getFechaLectura()));
		}
		if (dto.getIdBoletin() != null) {
			listaParametros.add(getParametro("ID_BOLETIN", STRING, dto.getIdBoletin()));

		}

		parametrosDataDTO.setParam(listaParametros);	
		json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
		
		String[] parts = json.split(":");
		String part2 = parts[1];
		parts = part2.split("}");
		String id = parts[0];

		String[] parts1 = id.split(",");
		String id1 = parts1[0];

		String cadena = id1;
		char[] cadenaDiv = cadena.toCharArray();
		String idPersona = "";
		for (int i = 0; i < cadenaDiv.length; i++) {
			if (Character.isDigit(cadenaDiv[i])) {
				idPersona += cadenaDiv[i];
			}
		}
		insertarAccesorios(dto, idPersona);
		return  Mono.just(json);
	}

	@Override
	public Mono<String> obtenerAccesorios(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catAccesorios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consultaI(parametrosDataDTO, PersonasIncidenteDTO.class);

	}

	public Mono<String> insertarAccesorios(PersonasIncidenteDTO dto, String persona) {
		String idPersona = persona;
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, accesoriosPersonaInv);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdObjetos() != null) {
			listaParametros.add(getParametro("id_objetos", STRING, dto.getIdObjetos()));
		}
		if (dto.getIdEvento() != null) {
			listaParametros.add(getParametro(IDEVENTO, STRING, dto.getIdEvento()));
		}
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		if (dto.getNombre() != null) {
			listaParametros.add(getParametro("nombre", STRING, dto.getNombre()));
		}
		if (dto.getApellidoPaterno() != null) {
			listaParametros.add(getParametro("apellido_paterno", STRING, dto.getApellidoPaterno()));
		}
		if (dto.getApellidoMaterno() != null) {
			listaParametros.add(getParametro("apellido_materno", STRING, dto.getApellidoMaterno()));
		}
		listaParametros.add(getParametro(IDPERSONAINVOLUCRADA, STRING, idPersona));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);

	}

	public Mono<String> actualizarPersonaInvolucradas(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(telefonista, personaInvolucrada);
		List<ParametroDTO> listaParametros = new ArrayList<>();

		listaParametros.add(getParametro("UUID", STRING, dto.getUuid()));
		if (dto.getDirrecionIp() != null) {
			listaParametros.add(getParametro("DIRECCION_IP", STRING, dto.getDirrecionIp()));
		}
		if (dto.getIdEvento() != null) {
			listaParametros.add(getParametro("ID_EVENTO", INTEGER, dto.getIdEvento()));
		}
		listaParametros.add(getParametro("MODIFICADO_POR", INTEGER, dto.getModificadoPor()));
		if (dto.getNombre() != null) {
			listaParametros.add(getParametro("NOMBRE", STRING, dto.getNombre()));
		}
		if (dto.getApellidoPaterno() != null) {

			listaParametros.add(getParametro("APELLIDO_PATERNO", STRING, dto.getApellidoPaterno()));
		}
		if (dto.getApellidoMaterno() != null) {
			listaParametros.add(getParametro("APELLIDO_MATERNO", STRING, dto.getApellidoMaterno()));
		}
		if (dto.getEdad() != null) {
			listaParametros.add(getParametro("EDAD", INTEGER, dto.getEdad()));
		}
		if (dto.getIdColorCabello() != null) {
			listaParametros.add(getParametro("ID_COLOR_CABELLO", INTEGER, dto.getIdColorCabello()));
		}
		if (dto.getIdColorOjos() != null) {
			listaParametros.add(getParametro("ID_COLOR_OJOS", INTEGER, dto.getIdColorOjos()));
		}
		if (dto.getIdColorTez() != null) {
			listaParametros.add(getParametro("ID_COLOR_TEZ", INTEGER, dto.getIdColorTez()));
		}
		if (dto.getIdPersonaInvolucrada() != null) {
			listaParametros.add(getParametro("ID_PERSONA_INVOLUCRADA", INTEGER, dto.getIdPersonaInvolucrada()));
		}
		if (dto.getIdComplexion() != null) {
			listaParametros.add(getParametro("ID_COMPLEXION", INTEGER, dto.getIdComplexion()));
		}
		if (dto.getEstatura() != null) {
			listaParametros.add(getParametro("ESTATURA", STRING, dto.getEstatura()));
		}
		if (dto.getIdFormaCara() != null) {
			listaParametros.add(getParametro("ID_FORMA_CARA", INTEGER, dto.getIdFormaCara()));
		}
		if (dto.getIdTipoCabello() != null) {
			listaParametros.add(getParametro("ID_TIPO_CABELLO", INTEGER, dto.getIdTipoCabello()));
		}
		if (dto.getIdTipoNariz() != null) {
			listaParametros.add(getParametro("ID_TIPO_NARIZ", INTEGER, dto.getIdTipoNariz()));
		}
		if (dto.getIdTipoPersona() != null) {
			listaParametros.add(getParametro("ID_TIPO_PERSONA", INTEGER, dto.getIdTipoPersona()));
		}
		if (dto.getClave() != null) {
			listaParametros.add(getParametro("SEXO", INTEGER, dto.getClave()));
		}
		if (dto.getAlias() != null) {
			listaParametros.add(getParametro("ALIAS", STRING, dto.getAlias()));
		}
		if (dto.getSeniaParticular() != null) {
			listaParametros.add(getParametro("SENIA_PARTICULAR", STRING, dto.getSeniaParticular()));
		}
		if (dto.getIdVestimentaTalle() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_TALLE", INTEGER, dto.getIdVestimentaTalle()));
		}
		if (dto.getIdColorVestimentaTalle() != null) {
			listaParametros.add(getParametro("ID_COLOR_VESTIMENTA_TALLE", INTEGER, dto.getIdColorVestimentaTalle()));
		}
		if (dto.getIdVestimentaCabeza() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_CABEZA", INTEGER, dto.getIdVestimentaCabeza()));
		}
		if (dto.getIdColorVestimentaCabeza() != null) {
			listaParametros.add(getParametro("ID_COLOR_VESTIMENTA_CABEZA", INTEGER, dto.getIdColorVestimentaCabeza()));
		}
		if (dto.getIdVestimentaCalzado() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_CALZADO", INTEGER, dto.getIdVestimentaCalzado()));
		}
		if (dto.getIdColorVestimentaCalzado() != null) {
			listaParametros
					.add(getParametro("ID_COLOR_VESTIMENTA_CALZADO", INTEGER, dto.getIdColorVestimentaCalzado()));
		}
		if (dto.getIdVestimentaInferior() != null) {
			listaParametros.add(getParametro("ID_VESTIMENTA_INFERIOR", INTEGER, dto.getIdVestimentaInferior()));
		}
		if (dto.getIdColorVestimentaInferior() != null) {
			listaParametros
					.add(getParametro("ID_COLOR_VESTIMENTA_INFERIOR", INTEGER, dto.getIdColorVestimentaInferior()));
		}
		if (dto.getIdEstatura() != null) {
			listaParametros.add(getParametro("ID_ESTATURA", INTEGER, dto.getIdEstatura()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> eliminarAccesorio(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, accesoriosPersonaInv);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_objeto", STRING, dto.getIdObjeto()));
		listaParametros.add(getParametro(IDPERSONAINVOLUCRADA, INTEGER, dto.getIdPersonaInvolucrada()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));

		parametrosDataDTO.setParam(listaParametros);
		return elimina(parametrosDataDTO);

	}

	@Override
	public Mono<List<PersonasIncidenteDTO>> reporteListaPersonaEvento(PersonasIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, personaEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "String", dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, "Integer", dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<PersonasIncidenteDTO> lista = AnnotationService.getListData(json, PersonasIncidenteDTO.class);
		return Mono.just(lista);
	}

}