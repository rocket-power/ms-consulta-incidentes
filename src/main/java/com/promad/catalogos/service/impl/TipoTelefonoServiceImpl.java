package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.TipoTelefonoDTO;
import com.promad.catalogos.service.ITipoTelefonoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class TipoTelefonoServiceImpl extends BaseService implements ITipoTelefonoService{

	@Value("${spring.application.name}")
	private String nombreMS;
	
	@Value("${nombre.paquete.catalogo}")
	private String paquete;
	
	@Value("${nombre.store.procedure.cat_tipo_telefono}")
	private String tipotelefonos; 
	
	@Override
	public Mono<String> obtenerTipoTelefono(TipoTelefonoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(paquete, tipotelefonos);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);		
		return consulta(parametrosDataDTO, TipoTelefonoDTO.class );
	}
}
