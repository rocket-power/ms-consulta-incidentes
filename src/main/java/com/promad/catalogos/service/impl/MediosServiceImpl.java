package com.promad.catalogos.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.promad.catalogos.dto.MediosDTO;
import com.promad.catalogos.dto.ServidorDTO;
import com.promad.catalogos.service.IMediosService;
import com.promad.catalogos.utils.FTPUploader;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class MediosServiceImpl extends BaseService implements IMediosService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.store.procedure.medios}")
	private String medios;

	@Value("${nombre.store.asociados_ftp}")
	private String servidor;
	
	private static final String IDMEDIOASOCIADO ="id_medio_asociado";

	@Override
	public Mono<String> obtenerServidor(ServidorDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, servidor);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return Mono.just(consulta(parametrosDataDTO));
	}

	@Override
	public Mono<String> busquedaIdMedios(MediosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, medios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDMEDIOASOCIADO, INTEGER, dto.getIdMedioAsociado()));
		parametrosDataDTO.setParam(listaParametros);
		return Mono.just(consulta(parametrosDataDTO));
	}

	@Override
	public Mono<String> obtenerMedios(MediosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, medios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro("content_type", STRING, dto.getContentType()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, MediosDTO.class);
	}

	@Override
	public Mono<String> insertarMedios(MediosDTO dto) throws IOException {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, medios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		Mono<String> respuesta = null;
		for (MediosDTO i : dto.getMedios()) {
			listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
			listaParametros.add(getParametro("id_evento", STRING, dto.getIdEvento()));
			listaParametros.add(getParametro("nombre", STRING, i.getNombre()));
			listaParametros.add(getParametro("comentario", STRING, i.getDescripcion()));
			listaParametros.add(getParametro("creado_por", INTEGER, i.getCreadoPor()));

			String carpeta = null;
			String pathArchivo = null;
			String extension = i.getNombre().substring(i.getNombre().length() - 3, i.getNombre().length());
			String tipo = null;
			byte[] size = Base64.getDecoder().decode(i.getData());
			double bytes = ((size.length) / 1024);
			double megas = ((bytes) / 1024);
			String peso = megas + "";
			String tamanio = peso.substring(0, 4) + " MB";

			if (extension.equalsIgnoreCase("JPG") || extension.equalsIgnoreCase("PNG")
					|| extension.equalsIgnoreCase("GIF") || extension.equalsIgnoreCase("TIF")
					|| extension.equalsIgnoreCase("RAW") || extension.equalsIgnoreCase("PSD")
					|| extension.equalsIgnoreCase("CDR") || extension.equalsIgnoreCase("NEF")
					|| extension.equalsIgnoreCase("SVG") || extension.equalsIgnoreCase("DWG")
					|| extension.equalsIgnoreCase("BMP") || extension.equalsIgnoreCase("EPS")
					|| extension.equalsIgnoreCase("XCF") || extension.equalsIgnoreCase("PCX")
					|| extension.equalsIgnoreCase("DNG") || extension.equalsIgnoreCase("WMP")
					|| extension.equalsIgnoreCase("PSB") || extension.equalsIgnoreCase("JP2")) {
				tipo = "imagen";
			}

			else if (extension.equalsIgnoreCase("QPX") || extension.equalsIgnoreCase("QTP")
					|| extension.equalsIgnoreCase("QTS") || extension.equalsIgnoreCase("QTX")
					|| extension.equalsIgnoreCase("QUP") || extension.equalsIgnoreCase("AVI")
					|| extension.equalsIgnoreCase("MOV") || extension.equalsIgnoreCase("MP4")
					|| extension.equalsIgnoreCase("3GP") || extension.equalsIgnoreCase("WMV")
					|| extension.equalsIgnoreCase("ASF") || extension.equalsIgnoreCase("M4V")
					|| extension.equalsIgnoreCase("MPG") || extension.equalsIgnoreCase("SWF")
					|| extension.equalsIgnoreCase("MKV") || extension.equalsIgnoreCase("QTL")
					|| extension.equalsIgnoreCase("VOB") || extension.equalsIgnoreCase("DVD")) {
				tipo = "video";
			}

			else if (extension.equalsIgnoreCase("MP3") || extension.equalsIgnoreCase("WMA")
					|| extension.equalsIgnoreCase("WAV") || extension.equalsIgnoreCase("AIF")
					|| extension.equalsIgnoreCase("AMF") || extension.equalsIgnoreCase("ASF")
					|| extension.equalsIgnoreCase("CDA") || extension.equalsIgnoreCase("FAR")
					|| extension.equalsIgnoreCase("LWV") || extension.equalsIgnoreCase("MID")
					|| extension.equalsIgnoreCase("XMZ") || extension.equalsIgnoreCase("WAX")
					|| extension.equalsIgnoreCase("VOC") || extension.equalsIgnoreCase("ULT")
					|| extension.equalsIgnoreCase("STZ") || extension.equalsIgnoreCase("STM")
					|| extension.equalsIgnoreCase("SND") || extension.equalsIgnoreCase("RMI")
					|| extension.equalsIgnoreCase("OKT") || extension.equalsIgnoreCase("OGG")
					|| extension.equalsIgnoreCase("MTM") || extension.equalsIgnoreCase("MP2")
					|| extension.equalsIgnoreCase("MP1") || extension.equalsIgnoreCase("MIZ")) {
				tipo = "audio";
			} else {
				tipo = "archivo";
			}

			if (tipo.equalsIgnoreCase("AUDIO")) {
				carpeta = "audios";
			} else if (tipo.equalsIgnoreCase("VIDEO")) {
				carpeta = "videos";
			} else if (tipo.equalsIgnoreCase("IMAGEN")) {
				carpeta = "imagenes";
			}
			carpeta = dto.getIdEvento() + File.separator + carpeta;
			pathArchivo = File.separator + carpeta + File.separator + i.getNombre();
			listaParametros.add(getParametro("tamanio_archivo", STRING, tamanio));
			listaParametros.add(getParametro("extension", STRING, extension));
			listaParametros.add(getParametro("path", STRING, pathArchivo));
			listaParametros.add(getParametro("content_type", STRING, tipo));
			parametrosDataDTO.setParam(listaParametros);

			ServidorDTO ser = new ServidorDTO();
			ser.setUuid(dto.getUuid());
			Mono<String> server = obtenerServidor(ser);
			List<ServidorDTO> lista = AnnotationService.getListData(server.toString(), ServidorDTO.class);
			for (ServidorDTO s : lista) {
				boolean carga = cargarArchivo(s.getUrl(), s.getUser(), s.getPwd(), carpeta, i.getNombre(), i.getData());
				if (carga) {
					respuesta = inserta(parametrosDataDTO);
				} else {
					respuesta = Mono.just("Error al cargar");
				}

			}
		}
		return respuesta;
	}

	public boolean cargarArchivo(String url, String user, String pwd, String carpeta, String nombreArchivo, String data){
		boolean respuesta;
		FTPUploader.connectFTP(url, user, pwd);
		respuesta = FTPUploader.uploadFile(data, nombreArchivo, carpeta);
		return respuesta;
	}

	public void descargaArchivo(String url, String user, String pwd, String rutaDestino, String path) {
		FTPUploader.connectFTP(url, user, pwd);
		FTPUploader.downloadFile(rutaDestino, path);
	}

	@Override
	public Mono<String> descargaMedio(MediosDTO dto) {
		ServidorDTO ser = new ServidorDTO();
		ser.setUuid(dto.getUuid());
		Mono<String> server = obtenerServidor(ser);
		List<ServidorDTO> lista = AnnotationService.getListData(server.toString(), ServidorDTO.class);
		for (ServidorDTO s : lista) {
			FTPUploader.connectFTP(s.getUrl(), s.getUser(), s.getPwd());
		}

		Mono<String> archivo = busquedaIdMedios(dto);
		List<MediosDTO> list = AnnotationService.getListData(archivo.toString(), MediosDTO.class);

		for (MediosDTO a : list) {
			String home = System.getProperty("user.home") + File.separator + a.getIdEvento() + File.separator
					+ a.getContentType().toLowerCase();
			File destino = new File(home);
			if (!destino.exists()) {
				destino.mkdirs();
			}
			String ruta = destino + File.separator + dto.getNombre();
			FTPUploader.downloadFile(ruta, a.getPath());
		}
		return null;
	}

	@Override
	public Mono<String> actualizarMedios(MediosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, medios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDMEDIOASOCIADO, STRING, dto.getIdMedioAsociado()));
		listaParametros.add(getParametro("comentario", STRING, dto.getDescripcion()));
		listaParametros.add(getParametro("nombre", STRING, dto.getNombre()));
		listaParametros.add(getParametro("modificado_por", STRING, dto.getModificadoPor()));

		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> eliminarMedios(MediosDTO dto) {
		Mono<String> respuesta = null;
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, medios);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDMEDIOASOCIADO, INTEGER, dto.getIdMedioAsociado()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		ServidorDTO ser = new ServidorDTO();
		ser.setUuid(dto.getUuid());
		Mono<String> server = obtenerServidor(ser);
		List<ServidorDTO> lista = AnnotationService.getListData(server.toString(), ServidorDTO.class);
		for (ServidorDTO s : lista) {
			FTPUploader.connectFTP(s.getUrl(), s.getUser(), s.getPwd());
		}
		Mono<String> archivo = busquedaIdMedios(dto);
		List<MediosDTO> list = AnnotationService.getListData(archivo.toString(), MediosDTO.class);
		for (MediosDTO a : list) {
			boolean delete = FTPUploader.deleteFile(a.getPath());
			if (delete) {
				respuesta = elimina(parametrosDataDTO);
			}
		}
		return respuesta;

	}
}
