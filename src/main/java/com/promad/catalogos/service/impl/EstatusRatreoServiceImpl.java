package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.EstatusRastreoDTO;
import com.promad.catalogos.service.IEstatusRastreoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class EstatusRatreoServiceImpl extends BaseService implements IEstatusRastreoService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.cat_tipo_rastreo}")
	private String catalogoEstatusRastreo;

	@Override
	public Mono<String> obtenerCatEstatusRastreo(EstatusRastreoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoEstatusRastreo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return super.consulta(parametrosDataDTO, EstatusRastreoDTO.class);
	}

	@Override
	public Mono<String> obtenerCatEstatusRastreoPorId(EstatusRastreoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoEstatusRastreo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "String", dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = super.consulta(parametrosDataDTO);
		List<EstatusRastreoDTO> lista = AnnotationService.getListData(json, EstatusRastreoDTO.class);
		for (EstatusRastreoDTO item : lista) {
			if (item.getIdEstatusRastreo().equals(dto.getIdEstatusRastreo())) {
				return Mono.just( new Gson().toJson(item));
			}
		}
		return Mono.just("No se encontro estatus rastreo con id:" + dto.getIdEstatusRastreo());
	}

}
