package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.promad.catalogos.dto.ReporteDescriptivoDTO;
import com.promad.catalogos.service.IReporteDescriptivoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ReporteDescriptivoServiceImpl extends BaseService implements IReporteDescriptivoService {
	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.store.procedure.datos_incidente_reporte}")
	private String datosIncidenteReporte;

	@Value("${nombre.store.procedure.tiempos_incidente_reporte}")
	private String tiemposIncidenteReporte;
	
	private static final String IDEVENTO="id_evento";

	@Override
	public Mono<String> obtenerReporteDescriptivo(ReporteDescriptivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, datosIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ReporteDescriptivoDTO.class);
	}

	@Override
	public Mono<List<ReporteDescriptivoDTO>> datosLlamada(ReporteDescriptivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, datosIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, ReporteDescriptivoDTO.class));
	}

	@Override
	public Mono<String> obtenerTiemposIncidente(ReporteDescriptivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, tiemposIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ReporteDescriptivoDTO.class);
	}

	@Override
	public Mono<List<ReporteDescriptivoDTO>> datosTiemposLlamada(ReporteDescriptivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, tiemposIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, ReporteDescriptivoDTO.class));
	}
}