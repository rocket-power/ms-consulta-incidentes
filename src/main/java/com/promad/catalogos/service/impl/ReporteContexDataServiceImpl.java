package com.promad.catalogos.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aspectj.lang.annotation.SuppressAjWarnings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.BitacoraDTO;
import com.promad.catalogos.dto.CatLocalidadDTO;
import com.promad.catalogos.dto.DatosIncidenteDTO;
import com.promad.catalogos.dto.InstitucionesAsignadasDTO;
import com.promad.catalogos.dto.LlamadasAsociadasDTO;
import com.promad.catalogos.dto.ObjetosInvolucradosDTO;
import com.promad.catalogos.dto.PersonasIncidenteDTO;
import com.promad.catalogos.dto.VehiculosInvolucradosDTO;
import com.promad.catalogos.service.IBitacoraService;
import com.promad.catalogos.service.ICatLocalidadService;
import com.promad.catalogos.service.IConsultaPersonaService;
import com.promad.catalogos.service.IDatosIncidenteService;
import com.promad.catalogos.service.IInstitucionesAsignadasService;
import com.promad.catalogos.service.IObjetosInvolucradosService;
import com.promad.catalogos.service.IReporteContextDataService;
import com.promad.catalogos.service.IVehiculosInvolucradosService;
import com.promad.catalogos.service.LlamadasAsociadasService;

@Service
@PropertySource("classpath:application.properties")
public class ReporteContexDataServiceImpl implements IReporteContextDataService {

	@Autowired
	private IDatosIncidenteService iDatosIncidenteService;

	@Autowired
	private IBitacoraService iBitacoraService;

	@Autowired
	private IConsultaPersonaService iConsultaPersonaService;

	@Autowired
	private IVehiculosInvolucradosService iVehiculosInvolucradosService;

	@Autowired
	private IObjetosInvolucradosService iObjetosInvolucradosService;

	@Autowired
	private IInstitucionesAsignadasService iInstitucionesAsignadasService;

	@Autowired
	private LlamadasAsociadasService llamadasAsociadasService;

	@Autowired
	private ICatLocalidadService iCatLocalidadService;

	@SuppressAjWarnings
	@Override
	public Map<String, Object> parametrosDataReporte(String nombreReporte, Map<String, ?> parameters) {

		Map<String, Object> parametros = new HashMap<>();

		if (nombreReporte.equals("reporteDescriptivo")) {

			@SuppressWarnings("unchecked")
			List<DatosIncidenteDTO> datosLlamada = getData(nombreReporte, parameters, 1);
			@SuppressWarnings("unchecked")
			List<BitacoraDTO> tiemposLlamada = getData(nombreReporte, parameters, 8);
			if ((datosLlamada.size()) > 0) {
				parametros.put("ciudad", datosLlamada.get(0).getNombreMunicipio());
				parametros.put("Estado", datosLlamada.get(0).getNombreEstado());
				parametros.put("horaFechaInicio", datosLlamada.get(0).getFechaInicio());
				parametros.put("horaFechaFinal", datosLlamada.get(0).getFechaFin());
				parametros.put("folio", datosLlamada.get(0).getFolio());
				parametros.put("recibidaPor", datosLlamada.get(0).getRecibidaPor());
				parametros.put("Motivo", datosLlamada.get(0).getNombreMotivo());
				parametros.put("Gravedad", datosLlamada.get(0).getPrioridad());
				parametros.put("denunciante", datosLlamada.get(0).getDenunciante());
				parametros.put("direccion", datosLlamada.get(0).getDireccion());
			}
			parametros.put("descripcion", getData(nombreReporte, parameters, 7));
			if ((tiemposLlamada.size()) > 0) {
				parametros.put("horaRecepcion", tiemposLlamada.get(0).getHoraRecepcion());
				parametros.put("tiempoTranmision", tiemposLlamada.get(0).getHoraTransmision());
				parametros.put("horaCaptura", tiemposLlamada.get(0).getHoraCaptura());
				parametros.put("transmision", tiemposLlamada.get(0).getTiempoTransmision());
				parametros.put("tiempoCaptura", tiemposLlamada.get(0).getTiempoCaptura());
				parametros.put("origenDeLlamada", tiemposLlamada.get(0).getOrigen());
				parametros.put("Zona", tiemposLlamada.get(0).getZona());
				parametros.put("Subconjunto", tiemposLlamada.get(0).getSubConjunto());
			}
			parametros.put("datosPersonasInvolucradas", getData(nombreReporte, parameters, 2));
			parametros.put("datosVehiculosInvolucrados", getData(nombreReporte, parameters, 3));
			parametros.put("datosObjetosInvolucrados", getData(nombreReporte, parameters, 4));
			parametros.put("datosRecepcionTelelefonica", getData(nombreReporte, parameters, 5));
			parametros.put("datosInstituciones", getData(nombreReporte, parameters, 6));
			parametros.put("datosLllamadaRecurrente", getData(nombreReporte, parameters, 9));
			parametros.put("ciudad", "Mexico");
			parametros.put("logo1", "cdmx.png");
			parametros.put("logo2", "c5.png");
			parametros.put("bitacoraL", "bitacora.png");
			parametros.put("objetosL", "Objetos_involucrados.png");
			parametros.put("vehiculosL", "Vehiculos_involucrados.png");
			parametros.put("personasL", "Personas_involucradas.png");
			parametros.put("institucionesL", "Instituciones.png");
			parametros.put("llamadaRL", "llamadaR.png");
		}

		if (nombreReporte.equals("listas")) {
			parametros.put("datosLocalidades", getData(nombreReporte, parameters, 1));
		}

		return parametros;
	}

	@SuppressWarnings("rawtypes")
	public List getData(String nombreReporte, Map<String, ?> parameters, int bandera) {

		if (nombreReporte.equals("reporteDescriptivo")) {
			if (bandera == 1) {
				DatosIncidenteDTO datosIncidenteDto = (DatosIncidenteDTO) parameters.get("dtoDatosIncidente");
				return iDatosIncidenteService.reporteDatosIncidente(datosIncidenteDto).block();

			}
			if (bandera == 2) {
				PersonasIncidenteDTO datosPersonasInvolucradas = (PersonasIncidenteDTO) parameters.get("PersonasInvolucradas");
				return iConsultaPersonaService.reporteListaPersonaEvento(datosPersonasInvolucradas).block();

			}
			if (bandera == 3) {
				VehiculosInvolucradosDTO datosVehiculosInvolucrados = (VehiculosInvolucradosDTO) parameters.get("VehiculosInvolucrados");
				return iVehiculosInvolucradosService.reporteListaVehiculoInvolucrado(datosVehiculosInvolucrados).block();

			}
			if (bandera == 4) {
				ObjetosInvolucradosDTO datosObjetosInvolucrados = (ObjetosInvolucradosDTO) parameters.get("ObjetosInvolucrados");
				return iObjetosInvolucradosService.reporteListaObjetosInvolucrados(datosObjetosInvolucrados).block();

			}
			if (bandera == 5) {
				BitacoraDTO bitacoraData = (BitacoraDTO) parameters.get("bitacoraDTO");
				return iBitacoraService.reporteBitacora(bitacoraData).block();
			}
			if (bandera == 6) {
				InstitucionesAsignadasDTO datosInstituciones = (InstitucionesAsignadasDTO) parameters.get("InstitucionesAsignadas");
				return iInstitucionesAsignadasService.reporteInstitucionesAsignadas(datosInstituciones).block();
			}
			if (bandera == 7) {
				DatosIncidenteDTO datosIncidenteDto2 = (DatosIncidenteDTO) parameters.get("dtoDatosIncidenteDescripcion");
				return iDatosIncidenteService.reporteDatosIncidente(datosIncidenteDto2).block();
			}
			if (bandera == 8) {
				BitacoraDTO bitacoraData = (BitacoraDTO) parameters.get("bitacoraDTO");
				return iBitacoraService.reporteTiemposEvento(bitacoraData).block();

			}
			if (bandera == 9) {
				LlamadasAsociadasDTO llamadasAsociadas = (LlamadasAsociadasDTO) parameters.get("llamadasDTO");
				return llamadasAsociadasService.reporteLlamadasAsociadas(llamadasAsociadas).block();
			}
		}

		if (nombreReporte.equals("listas")) {
			CatLocalidadDTO dtoLocalidad = (CatLocalidadDTO) parameters.get("dtoLocalidad");
			return iCatLocalidadService.obtenerCatLocalidadreporte(dtoLocalidad).block();
		}
		return null;

	}
}
