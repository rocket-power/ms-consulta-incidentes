package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.LlamadasAsociadasDTO;
import com.promad.catalogos.service.LlamadasAsociadasService;
import reactor.core.publisher.Mono;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

@Service
@PropertySource("classpath:application.properties")
public class LlamadasAsociadasServiceImpl extends BaseService implements LlamadasAsociadasService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.store.procedure.llamadas_asociadas}")
	private String llamadas;

	@Override
	public Mono<String> obtenerLlamadasAsociadas(LlamadasAsociadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, llamadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		if (dto.getTipo() != null) {
			listaParametros.add(getParametro("tipo", STRING, dto.getTipo()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, LlamadasAsociadasDTO.class);
	}

	@Override
	public Mono<List<LlamadasAsociadasDTO>> reporteLlamadasAsociadas(LlamadasAsociadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, llamadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		if (dto.getTipo() != null) {
			listaParametros.add(getParametro("tipo", STRING, dto.getTipo()));
		}
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, LlamadasAsociadasDTO.class));
	}

}
