package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.DatosIncidenteDTO;
import com.promad.catalogos.service.IDatosIncidenteService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class DatosIncidenteServiceImpl extends BaseService implements IDatosIncidenteService {

	@Value("${nombre.paquete.telefonista}")
	private String telefonista;

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_origen}")
	private String origen;

	@Value("${nombre.store.procedure.descripcion_evento}")
	private String descripcionEvento;

	@Value("${nombre.store.procedure.datos_incidente}")
	private String datosIncidente;

	@Value("${nombre.store.procedure.historial_georeferencias}")
	private String historialGeoreferencias;

	@Value("${nombre.store.procedure.resultado_incidente}")
	private String resultadoIncidente;

	@Value("${nombre.store.procedure.descripcion_lugar_evento}")
	private String descripcionLugarEvento;
	
	private  static final String IDEVENTO = "id_evento";
	private static final String DESCRIPCION = "descripcion";
	private static final String DESCRIPCIONLUGAR = "descripcion_lugar";

	@Override
	public Mono<String> obtenerDatosIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, datosIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		if (dto.getDescripcion() != null) {
			listaParametros.add(getParametro(DESCRIPCION, STRING, dto.getDescripcion()));
		}
		if (dto.getDescripcionLugar() != null) {
			listaParametros.add(getParametro(DESCRIPCIONLUGAR, STRING, dto.getDescripcionLugar()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, DatosIncidenteDTO.class);
	}

	@Override
	public Mono<String> insertarDatosIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(telefonista, descripcionEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro(DESCRIPCION, STRING, dto.getDescripcion()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> actualizarDatosIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, datosIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		if (dto.getPrioridad() != null) {
			listaParametros.add(getParametro("prioridad", STRING, dto.getPrioridad()));
		}
		if (dto.getIdMotivo() != null) {
			listaParametros.add(getParametro("id_motivo", INTEGER, dto.getIdMotivo()));
		}
		if (dto.getLugar() != null) {
			listaParametros.add(getParametro("lugar", STRING, dto.getLugar()));
		}
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerHistorialGeoreferencias(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, historialGeoreferencias);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<DatosIncidenteDTO> lista = AnnotationService.getListData(json, DatosIncidenteDTO.class);
		for (DatosIncidenteDTO item : lista) {
			item.setDireccion("" + item.getNombreColonia() + " " + item.getEntreCalle1() + " " + item.getEntreCalle2()
					+ " " + item.getCalle() + " " + item.getNumero());
		}
		return convertToResultado(lista);
	}

	@Override
	public Mono<String> obtenerOrigen(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, origen);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, DatosIncidenteDTO.class);
	}

	@Override
	public Mono<String> obtenerResultadoIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, resultadoIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, DatosIncidenteDTO.class);
	}

	@Override
	public Mono<String> insertarDescripcionLugarIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, descripcionLugarEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro(DESCRIPCIONLUGAR, STRING, dto.getDescripcionLugar()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<List<DatosIncidenteDTO>> reporteDatosIncidente(DatosIncidenteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, datosIncidente);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		if (dto.getDescripcion() != null) {
			listaParametros.add(getParametro(DESCRIPCION, STRING, dto.getDescripcion()));
		}
		if (dto.getDescripcionLugar() != null) {
			listaParametros.add(getParametro(DESCRIPCIONLUGAR, STRING, dto.getDescripcionLugar()));
		}
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return  Mono.just(AnnotationService.getListData(consulta, DatosIncidenteDTO.class));
	}

}
