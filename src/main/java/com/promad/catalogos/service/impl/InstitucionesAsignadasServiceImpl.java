package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.InstitucionesAsignadasDTO;
import com.promad.catalogos.service.IInstitucionesAsignadasService;
import reactor.core.publisher.Mono;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

@Service
@PropertySource("classpath:application.properties")
public class InstitucionesAsignadasServiceImpl extends BaseService implements IInstitucionesAsignadasService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.instituciones_asignadas}")
	private String institucionesAsignadas;

	@Value("${nombre.store.procedure.instituciones_no_involucradas}")
	private String institucionesNoInvolucradas;
	
	private static final String  IDEVENTO = "id_evento";
	private static final String  IDINSTASIGNADA = "id_inst_asignada";

	@Override
	public Mono<String> obtenerInstitucionesAsignadas(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO,INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, InstitucionesAsignadasDTO.class);
	}

	@Override
	public Mono<String> obtenerDetalleInstitucion(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro("id_institucion", INTEGER, dto.getIdInstitucion()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, InstitucionesAsignadasDTO.class);
	}

	@Override
	public Mono<String> obtenerRecursosAsignados(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro(IDINSTASIGNADA, INTEGER, dto.getIdInstitucionAsignada()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, InstitucionesAsignadasDTO.class);
	}

	@Override
	public Mono<String> insertarNotasCierre(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDINSTASIGNADA, INTEGER, dto.getIdInstitucionAsignada()));
		listaParametros.add(getParametro("nota_cierre",STRING, dto.getNotaCierre()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerRecursosAsignadosId(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro(IDINSTASIGNADA, INTEGER, dto.getIdInstitucionAsignada()));
		listaParametros.add(getParametro("ID_RECURSO", INTEGER, dto.getIdRecurso()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, InstitucionesAsignadasDTO.class);
	}

	@Override
	public Mono<String> insertarInstitucionEvento(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro("id_institucion", STRING, dto.getIdInstitucionS()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerInstitucionesNoAsignadas(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesNoInvolucradas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, InstitucionesAsignadasDTO.class);
	}

	@Override
	public Mono<List<InstitucionesAsignadasDTO>> reporteInstitucionesAsignadas(InstitucionesAsignadasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, institucionesAsignadas);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO,INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, InstitucionesAsignadasDTO.class));
	}

}
