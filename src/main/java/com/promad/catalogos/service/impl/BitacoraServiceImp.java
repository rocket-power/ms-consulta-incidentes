package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.promad.catalogos.dto.BitacoraDTO;
import com.promad.catalogos.service.IBitacoraService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class BitacoraServiceImp extends BaseService implements IBitacoraService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidente;

	@Value("${nombre.store.procedure.bitacora_evento}")
	private String bitacoraEvento;

	@Value("${nombre.store.procedure.tiempos_incidente_reporte}")
	private String tiemposIncidenteReporte;

	private static final String IDEVENTO = "id_evento"; 
	
	@Override
	public Mono<String> obtenerBitacora(BitacoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidente, bitacoraEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, BitacoraDTO.class);
	}

	@Override
	public Mono<List<BitacoraDTO>> reporteBitacora(BitacoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidente, bitacoraEvento);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, BitacoraDTO.class));
	}

	@Override
	public Mono<String> obtenerTiemposEvento(BitacoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidente, tiemposIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, BitacoraDTO.class);
	}

	@Override
	public Mono<List<BitacoraDTO>> reporteTiemposEvento(BitacoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidente, tiemposIncidenteReporte);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(consulta, BitacoraDTO.class));
	}

}
