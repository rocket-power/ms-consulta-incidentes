package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatalogosVehiculosDTO;
import com.promad.catalogos.service.ICatalogoVehiculoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class VehiculoServiceImpl extends BaseService implements ICatalogoVehiculoService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.cat_color_vehiculo}")
	private String catalogoColorVehiculo;

	@Override
	public Mono<String> obtenerCatColorVehiculo(CatalogosVehiculosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoColorVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogosVehiculosDTO.class);
	}

	@Override
	public Mono<String> obtenerCatColorVehiculoPorId(CatalogosVehiculosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoColorVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<CatalogosVehiculosDTO> lista = AnnotationService.getListData(json, CatalogosVehiculosDTO.class);
		for (CatalogosVehiculosDTO item : lista) {
			if (item.getIdColor().equals(dto.getIdColor())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just(  "No se encontro color vehiculo con id:" + dto.getIdColor());
	}

}
