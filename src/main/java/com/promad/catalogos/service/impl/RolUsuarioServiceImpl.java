package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.RolUsuarioDTO;
import com.promad.catalogos.service.IRolUsuarioService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class RolUsuarioServiceImpl extends BaseService implements IRolUsuarioService {

	@Value("${nombre.paquete.despacho}")
	private String despacho;

	@Value("${nombre.store.procedure.rol_usuario_activo}")
	private String rolUsuarioActivo;

	@Override
	public Mono<String> obtenerRolUsuario(RolUsuarioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(despacho, rolUsuarioActivo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_usuario", INTEGER, dto.getIdUsuario()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, RolUsuarioDTO.class);
	}

}
