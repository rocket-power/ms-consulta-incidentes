package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatLocalidadDTO;
import com.promad.catalogos.service.ICatLocalidadService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class CatLocalidadServiceImpl extends BaseService implements ICatLocalidadService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_localidad}")
	private String catLocalidad;

	@Value("${spring.application.name}")
	private String nombreMS;

	private static final String IDMUNICIPIO = "id_municipio"; 
	private static final String LOCALIDAD = "id_localidad";
	private static final String TIPOLOCALIDAD = "tipo_localidad"; 

	@Override
	public Mono<String> obtenerCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdMunicipio() != null) {
			listaParametros.add(getParametro(IDMUNICIPIO, INTEGER, dto.getIdMunicipio()));
		}
		if (dto.getTipoLocalidad() != null) {
			listaParametros.add(getParametro(TIPOLOCALIDAD, STRING, dto.getTipoLocalidad()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatLocalidadDTO.class);
	}

	@Override
	public Mono<String> insertarCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDMUNICIPIO, INTEGER, dto.getIdMunicipio()));
		if (dto.getIdLocalidad() != null) {
			listaParametros.add(getParametro(LOCALIDAD, INTEGER, dto.getIdLocalidad()));
		}
		listaParametros.add(getParametro("nombre_localidad", STRING, dto.getNombreLocalidad()));
		listaParametros.add(getParametro(TIPOLOCALIDAD, STRING, dto.getTipoLocalidad()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> actualizarCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDMUNICIPIO, INTEGER, dto.getIdMunicipio()));
		listaParametros.add(getParametro(LOCALIDAD, INTEGER, dto.getIdLocalidad()));
		listaParametros.add(getParametro("nombre_localidad", STRING, dto.getNombreLocalidad()));
		listaParametros.add(getParametro(TIPOLOCALIDAD, STRING, dto.getTipoLocalidad()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> eliminarCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(LOCALIDAD, INTEGER, dto.getIdLocalidad()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));

		parametrosDataDTO.setParam(listaParametros);
		return elimina(parametrosDataDTO);
	}

	@Override
	public Mono<String> busquedaIdCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<CatLocalidadDTO> lista = AnnotationService.getListData(json, CatLocalidadDTO.class);
		for (CatLocalidadDTO item : lista) {
			if (item.getIdLocalidad().equals(dto.getIdLocalidad())) {
				return Mono.just(new Gson().toJson(item));
			}

		}
		return Mono.just("No se encontro localidad con id: " + dto.getIdLocalidad());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Mono<List<CatLocalidadDTO>> obtenerCatLocalidadreporte(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return  (Mono<List<CatLocalidadDTO>>) AnnotationService.getListData(consulta, CatLocalidadDTO.class);

	}

}
