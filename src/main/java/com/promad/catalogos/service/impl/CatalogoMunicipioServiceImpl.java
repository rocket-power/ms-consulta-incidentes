package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatLocalidadDTO;
import com.promad.catalogos.dto.CatalogoMunicipioDTO;
import com.promad.catalogos.service.ICatalogoMunicipioService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class CatalogoMunicipioServiceImpl extends BaseService implements ICatalogoMunicipioService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_municipio}")
	private String catMunicipio;
	@Value("${nombre.store.procedure.cat_centro}")
	private String catCentro;

	@Value("${nombre.store.procedure.cat_estado}")
	private String catEstado;

	@Value("${nombre.store.procedure.cat_localidad}")
	private String catLocalidad;

	@Value("${spring.application.name}")
	private String nombreMS;

	private static final String IDESTADO = "id_estado"; 
	
	@Override
	public Mono<String> obtenerCatMunicipio(CatalogoMunicipioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catMunicipio);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdEstado() != null) {
			listaParametros.add(getParametro(IDESTADO, INTEGER, dto.getIdEstado()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoMunicipioDTO.class);
	}

	@Override
	public Mono<String> busquedaIdCatMunicipio(CatalogoMunicipioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catMunicipio);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "String", dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<CatalogoMunicipioDTO> lista = AnnotationService.getListData(json, CatalogoMunicipioDTO.class);
		for (CatalogoMunicipioDTO item : lista) {
			if (item.getIdMunicipio().equals(dto.getIdMunicipio())) {
				return Mono.just(new Gson().toJson(item));
			}

		}
		return Mono.just("No se encontro municipio con id: " + dto.getIdMunicipio());
	}

	@Override
	public Mono<String> obtenerCatEstado(CatalogoMunicipioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catEstado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDESTADO, INTEGER, dto.getIdEstado()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoMunicipioDTO.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Mono<List<CatalogoMunicipioDTO>> obtenerCatMunicipioReporte(CatalogoMunicipioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catMunicipio);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdEstado() != null) {
			listaParametros.add(getParametro(IDESTADO, INTEGER, dto.getIdEstado()));
		}
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		List<CatalogoMunicipioDTO> lista = AnnotationService.getListData(consulta, CatalogoMunicipioDTO.class);

		for (int i = 1; i < lista.size(); i++) {

			CatLocalidadDTO localidadDTO = new CatLocalidadDTO();
			localidadDTO.setUuid(dto.getUuid());
			localidadDTO.setIdMunicipio(lista.get(i).getIdMunicipio());

		}

		for (int i = 1; i < lista.size(); i++) {
			CatLocalidadDTO localidadDTO = new CatLocalidadDTO();
			localidadDTO.setUuid(dto.getUuid());
			localidadDTO.setIdMunicipio(lista.get(i).getIdMunicipio());

		}
		return (Mono<List<CatalogoMunicipioDTO>>) lista;
	}

	@SuppressWarnings("unchecked")
	public Mono<List<CatLocalidadDTO>> obtenerCatLocalidad(CatLocalidadDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catLocalidad);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdMunicipio() != null) {
			listaParametros.add(getParametro("id_municipio", INTEGER, dto.getIdMunicipio()));
		}
		if (dto.getTipoLocalidad() != null) {
			listaParametros.add(getParametro("tipo_localidad", STRING, dto.getTipoLocalidad()));
		}
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		List<CatLocalidadDTO> lista = AnnotationService.getListData(consulta, CatLocalidadDTO.class);
		return (Mono<List<CatLocalidadDTO>>) lista;
	}

	@Override
	public Mono<String> obtenerCatalogoCentro(CatalogoMunicipioDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catCentro);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoMunicipioDTO.class);
	}

}
