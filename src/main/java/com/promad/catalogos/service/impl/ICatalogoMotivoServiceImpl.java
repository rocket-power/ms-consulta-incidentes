package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatalogoMotivoDTO;
import com.promad.catalogos.service.ICatalogoMotivoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ICatalogoMotivoServiceImpl extends BaseService implements ICatalogoMotivoService {
	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_motivo}")
	private String catmotivo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Override
	public Mono<String> obtenerMotivo(CatalogoMotivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catmotivo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("UUID", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return super.consulta(parametrosDataDTO, CatalogoMotivoDTO.class);
	}

	public Mono<String> obtenerIdMotivo(CatalogoMotivoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catmotivo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("UUID", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<CatalogoMotivoDTO> lista = AnnotationService.getListData(json, CatalogoMotivoDTO.class);

		for (CatalogoMotivoDTO item : lista) {
			if (item.getIdMotivo().equals(dto.getIdMotivo())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se encontro ningun  motivo baja lista blanca con el id:" + dto.getIdMotivo());
	}

}
