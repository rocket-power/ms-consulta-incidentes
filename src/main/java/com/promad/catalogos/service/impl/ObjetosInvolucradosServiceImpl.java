package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.ObjetosInvolucradosDTO;
import com.promad.catalogos.service.IObjetosInvolucradosService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ObjetosInvolucradosServiceImpl extends BaseService implements IObjetosInvolucradosService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String incidentes;

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.objetos_involucrados}")
	private String objetosInvolucrados;

	@Value("${nombre.store.procedure.cat_tipo_moneda}")
	private String catTipoMoneda;

	@Value("${nombre.paquete.telefonista}")
	private String telefonista;

	@Value("${nombre.store.procedure.objeto_involucrado}")
	private String objetoInvolucrado;
	
	private static final String IDEVENTO ="id_evento";

	@Override
	public Mono<String> obtenerObjetosInvolucrados(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(incidentes, objetosInvolucrados);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ObjetosInvolucradosDTO.class);
	}

	@Override
	public Mono<String> obtenerCatalogoTipoMoneda(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catTipoMoneda);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ObjetosInvolucradosDTO.class);
	}

	@Override
	public Mono<String> insertarObjetosInvolucrados(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(incidentes, objetosInvolucrados);
		List<ParametroDTO> listaParametros = new ArrayList<>();

		if (dto.getRevision() != null) {
			listaParametros.add(getParametro("revision", INTEGER, dto.getRevision()));
		}
		if (dto.getIdEvento() != null) {
			listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		}
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		if (dto.getCantidad() != null) {
			listaParametros.add(getParametro("cantidad", INTEGER, dto.getCantidad()));
		}
		listaParametros.add(getParametro("id_objeto", INTEGER, dto.getIdObjeto()));
		if (dto.getEstimado() != null) {
			listaParametros.add(getParametro("valor_estimado", INTEGER, dto.getEstimado()));
		}
		if (dto.getDescripcion() != null) {
			listaParametros.add(getParametro("descripcion", STRING, dto.getDescripcion()));
		}
		if (dto.getIdCategoria() != null) {
			listaParametros.add(getParametro("id_categoria", INTEGER, dto.getIdCategoria()));
		}

		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> actualizarObjetosInvolucrados(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(telefonista, objetoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		if (dto.getCantidad() != null) {
			listaParametros.add(getParametro("cantidad", INTEGER, dto.getCantidad()));
		}
		if (dto.getIdObjeto() != null) {
			listaParametros.add(getParametro("id_objeto", INTEGER, dto.getIdObjeto()));
		}
		if (dto.getEstimado() != null) {
			listaParametros.add(getParametro("valor_estimado", INTEGER, dto.getEstimado()));
		}
		if (dto.getDescripcion() != null) {
			listaParametros.add(getParametro("observaciones", STRING, dto.getDescripcion()));
		}
		if (dto.getIdObjetoInvolucrado() != null) {
			listaParametros.add(getParametro("id_objeto_involucrado", INTEGER, dto.getIdObjetoInvolucrado()));
		}
		if (dto.getIdObjetoInvolucrado() != null) {
			listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		}

		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerObjetosInvolucradosId(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(incidentes, objetosInvolucrados);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));

		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<ObjetosInvolucradosDTO> lista = AnnotationService.getListData(json, ObjetosInvolucradosDTO.class);
		for (ObjetosInvolucradosDTO item : lista) {
			if (item.getIdObjetoInvolucrado().equals(dto.getIdObjetoInvolucrado())) {
				String result = new Gson().toJson(item);
				if(result==null) {
					return null;
				}else {
					return Mono.just(result);
				}
			}
		}
		return Mono.just("No se encontro el objeto con id:" + dto.getIdObjetoInvolucrado());
	}

	

	@Override
	public Mono<List<ObjetosInvolucradosDTO>> reporteListaObjetosInvolucrados(ObjetosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(incidentes, objetosInvolucrados);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));

		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		return Mono.just(AnnotationService.getListData(json, ObjetosInvolucradosDTO.class));
	}

}
