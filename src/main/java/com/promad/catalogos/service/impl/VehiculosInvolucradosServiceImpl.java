
package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.VehiculosInvolucradosDTO;
import com.promad.catalogos.service.IVehiculosInvolucradosService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class VehiculosInvolucradosServiceImpl extends BaseService implements IVehiculosInvolucradosService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.store.procedure.vehiculo_involucrado}")
	private String vehiculoInvolucrado;

	private static final String IDEVENTO ="id_evento";
	private static final String NAVERIGUACION ="NUMERO_AVERIGUACION";

	@Override
	public Mono<String> obtenerVehiculoInvolucrado(VehiculosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, vehiculoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		listaParametros.add(getParametro(NAVERIGUACION, INTEGER, dto.getNumeroAveriguacion()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, VehiculosInvolucradosDTO.class);
	}

	@Override

	public Mono<String> insertarVehiculoInvolucrado(VehiculosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, vehiculoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();

		if (dto.getRevision() != null) {
			listaParametros.add(getParametro("revision", INTEGER, dto.getRevision()));
		}
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdEvento() != null) {
			listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		}
		if (dto.getNumeroPlaca() != null) {
			listaParametros.add(getParametro("numero_placa", STRING, dto.getNumeroPlaca()));
		}
		if (dto.getNumeroSerie() != null) {
			listaParametros.add(getParametro("numero_serie", STRING, dto.getNumeroSerie()));
		}
		if (dto.getAnio() != null) {
			listaParametros.add(getParametro("anio", STRING, dto.getAnio()));
		}
		if (dto.getIdMarca() != null) {
			listaParametros.add(getParametro("id_marca", INTEGER, dto.getIdMarca()));
		}
		if (dto.getIdModelo() != null) {
			listaParametros.add(getParametro("id_modelo", INTEGER, dto.getIdModelo()));
		}
		if (dto.getIdColor() != null) {
			listaParametros.add(getParametro("id_color", INTEGER, dto.getIdColor()));
		}
		if (dto.getIdTipoVehiculo() != null) {
			listaParametros.add(getParametro("id_tipo_vehiculo", INTEGER, dto.getIdTipoVehiculo()));
		}
		if (dto.getObservacionesVeh() != null) {
			listaParametros.add(getParametro("observacionesveh", STRING, dto.getObservacionesVeh()));
		}
		if (dto.getIdEstatusRastreo() != null) {
			listaParametros.add(getParametro("id_estatus_rastreo", INTEGER, dto.getIdEstatusRastreo()));
		}
		if (dto.getNiv() != null) {
			listaParametros.add(getParametro("vin_vehicle_identification_num", STRING, dto.getNiv()));
		}
		if (dto.getNumeroMotor() != null) {
			listaParametros.add(getParametro("numero_motor", STRING, dto.getNumeroMotor()));
		}
		if (dto.getIdEstado() != null) {
			listaParametros.add(getParametro("id_estado_origen", INTEGER, dto.getIdEstado()));
		}
		if (dto.getListaNegra() != null) {
			listaParametros.add(getParametro("lista_negra", STRING, dto.getListaNegra()));
		}
		if(dto.getNumeroAveriguacion()!= null) {
		listaParametros.add(getParametro(NAVERIGUACION, INTEGER, dto.getNumeroAveriguacion()));
		}
		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> busquedaIdVehiculoInvolucrado(VehiculosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, vehiculoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<VehiculosInvolucradosDTO> lista = AnnotationService.getListData(json, VehiculosInvolucradosDTO.class);
		for (VehiculosInvolucradosDTO item : lista) {
			if (item.getIdVehiculoInvolucrado().equals(dto.getIdVehiculoInvolucrado())) {
				return Mono.just( new Gson().toJson(item));
			}
		}
		return Mono.just("No se encontro vehiculo involucrado con id: " + dto.getIdVehiculoInvolucrado());
	}

	@Override
	public Mono<String> obtenerAnios(VehiculosInvolucradosDTO dto) {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		List<Integer> anio = new ArrayList<>();
		for (int i = year - 100; i <= year + 1; i++) {
			anio.add(i);
		}
		return convertToResultado(anio);
	}

	@Override
	public Mono<String> actualizarVehiculoInvolucrado(VehiculosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, vehiculoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		if (dto.getRevision() != null) {
			listaParametros.add(getParametro("revision", INTEGER, dto.getRevision()));
		}
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("modificado_por", INTEGER, dto.getModificadoPor()));
		listaParametros.add(getParametro("id_vehiculo_involucrado", INTEGER, dto.getIdVehiculoInvolucrado()));
		if (dto.getIdEvento() != null) {
			listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		}
		if (dto.getNumeroPlaca() != null) {
			listaParametros.add(getParametro("numero_placa", STRING, dto.getNumeroPlaca()));
		}
		if (dto.getNumeroSerie() != null) {
			listaParametros.add(getParametro("numero_serie", STRING, dto.getNumeroSerie()));
		}
		if (dto.getAnio() != null) {
			listaParametros.add(getParametro("anio", STRING, dto.getAnio()));
		}
		if (dto.getIdMarca() != null) {
			listaParametros.add(getParametro("id_marca", INTEGER, dto.getIdMarca()));
		}
		if (dto.getIdModelo() != null) {
			listaParametros.add(getParametro("id_modelo", INTEGER, dto.getIdModelo()));
		}
		if (dto.getIdColor() != null) {
			listaParametros.add(getParametro("id_color", INTEGER, dto.getIdColor()));
		}
		if (dto.getIdTipoVehiculo() != null) {
			listaParametros.add(getParametro("id_tipo_vehiculo", INTEGER, dto.getIdTipoVehiculo()));
		}
		if (dto.getObservacionesVeh() != null) {
			listaParametros.add(getParametro("observacionesveh", STRING, dto.getObservacionesVeh()));
		}
		if (dto.getIdEstatusRastreo() != null) {
			listaParametros.add(getParametro("id_estatus_rastreo", INTEGER, dto.getIdEstatusRastreo()));
		}
		if (dto.getNiv() != null) {
			listaParametros.add(getParametro("vin_vehicle_identification_num", STRING, dto.getNiv()));
		}
		if (dto.getNumeroMotor() != null) {
			listaParametros.add(getParametro("numero_motor", STRING, dto.getNumeroMotor()));
		}
		if (dto.getIdEstado() != null) {
			listaParametros.add(getParametro("id_estado_origen", INTEGER, dto.getIdEstado()));
		}
		if (dto.getListaNegra() != null) {
			listaParametros.add(getParametro("lista_negra", STRING, dto.getListaNegra()));
		}
		if (dto.getIdMotivoCambioRastreo() != null) {
			listaParametros.add(getParametro("id_motivo_cambio_rastreo", INTEGER, dto.getIdMotivoCambioRastreo()));
		}
		if(dto.getNumeroAveriguacion()!= null) {
			listaParametros.add(getParametro(NAVERIGUACION, INTEGER, dto.getNumeroAveriguacion()));
			}

		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<List<VehiculosInvolucradosDTO>> reporteListaVehiculoInvolucrado(VehiculosInvolucradosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, vehiculoInvolucrado);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro(IDEVENTO, INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		 
		return Mono.just(AnnotationService.getListData(json, VehiculosInvolucradosDTO.class));
	}

}
