package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.SuscriptorDeProgramasEspecialesDTO;
import com.promad.catalogos.service.ISuscriptorDeProgramasEspecialesService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class SuscriptorServiceImpl extends BaseService implements ISuscriptorDeProgramasEspecialesService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.programas_especiales_informacion}")
	private String programasEspeciales;

	@Value("${nombre.store.procedure.programas_especiales_suscriptores}")
	private String programasSuscriptores;

	@Override
	public Mono<String> obtenerSuscriptor(SuscriptorDeProgramasEspecialesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, programasEspeciales);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "Sring", dto.getUuid()));
		listaParametros.add(getParametro("id_evento", "Integer", dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<SuscriptorDeProgramasEspecialesDTO> lista = AnnotationService.getListData(json,
				SuscriptorDeProgramasEspecialesDTO.class);
		for (SuscriptorDeProgramasEspecialesDTO item : lista) {
			item.setNombreCompleto(
					(item.getNombre() + " " + item.getApellidoPaterno() + " " + item.getApellidoMaterno()));
			item.setNombreCompletoEncargado((item.getNombreEncargado() + " " + item.getApellidoPaternoEncargado() + " "
					+ item.getApellidoMaternoEncargado()));

		}
		return Mono.just(new Gson().toJson(lista));
	}

	@Override
	public Mono<String> obtenerProgramasEspeciales(SuscriptorDeProgramasEspecialesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, programasSuscriptores);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "Sring", dto.getUuid()));
		listaParametros.add(getParametro("id_evento", "Integer", dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<SuscriptorDeProgramasEspecialesDTO> lista = AnnotationService.getListData(json,
				SuscriptorDeProgramasEspecialesDTO.class);
		for (SuscriptorDeProgramasEspecialesDTO item : lista) {
			item.setNombreCompleto(
					(item.getNombre() + " " + item.getApellidoPaterno() + " " + item.getApellidoMaterno()));

		}
		return Mono.just(new Gson().toJson(lista));

	}
}
