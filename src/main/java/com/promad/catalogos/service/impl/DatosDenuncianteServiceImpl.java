package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.DatosDenuncianteDTO;
import com.promad.catalogos.service.IDatosDenuncianteService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class DatosDenuncianteServiceImpl extends BaseService implements IDatosDenuncianteService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.cat_contacto}")
	private String catContacto;

	@Override
	public Mono<String> obtenerDatosDenunciante(DatosDenuncianteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catContacto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<DatosDenuncianteDTO> lista = AnnotationService.getListData(json, DatosDenuncianteDTO.class);
		for (DatosDenuncianteDTO item : lista) {
			item.setDireccionCompleta("calle " + item.getCalle() + " Numero: " + item.getNumero() + " CP."
					+ item.getCodigoPostal() + ", Colonia:" + item.getNombreColonia() + ", Localidad: "
					+ item.getNombreLocalidad() + ", Municipio: " + item.getNombreMunicipio() + ", Edo. "
					+ item.getNombreEstado() + ". entre Calle: " + item.getEntreCalle1() + " y calle "
					+ item.getEntreCalle2() + ". Punto de Interes: " + item.getNombrePuntoInteres());

		}
		return Mono.just(new Gson().toJson(lista));
	}

	@Override
	public Mono<String> actualizarDatosDenunciante(DatosDenuncianteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catContacto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("UUID", STRING, dto.getUuid()));
		listaParametros.add(getParametro("MODIFICADO_POR", INTEGER, dto.getModificadoPor()));
		listaParametros.add(getParametro("id_contacto", INTEGER, dto.getIdContacto()));
		if (dto.getNombre() != null) {
			listaParametros.add(getParametro("nombre", STRING, dto.getNombre()));
		}
		if (dto.getPaterno() != null) {
			listaParametros.add(getParametro("paterno", STRING, dto.getPaterno()));
		}
		if (dto.getMaterno() != null) {
			listaParametros.add(getParametro("materno", STRING, dto.getMaterno()));
		}
		if (dto.getTelefono() != null) {
			listaParametros.add(getParametro("telefono", STRING, dto.getTelefono()));
		}
		if (dto.getTipoTelefono() != null) {
			listaParametros.add(getParametro("tipo_telefono", INTEGER, dto.getTipoTelefono()));
		}
		if (dto.getIdEstado() != null) {
			listaParametros.add(getParametro("id_estado", INTEGER, dto.getIdEstado()));
		}
		if (dto.getIdMunicipio() != null) {
			listaParametros.add(getParametro("id_municipio", INTEGER, dto.getIdMunicipio()));
		}
		if (dto.getIdColonia() != null) {
			listaParametros.add(getParametro("id_colonia", INTEGER, dto.getIdColonia()));
		}
		if (dto.getCalle() != null) {
			listaParametros.add(getParametro("CALLE", STRING, dto.getCalle()));
		}
		if (dto.getNumero() != null) {
			listaParametros.add(getParametro("numero", STRING, dto.getNumero()));
		}
		if (dto.getCodigoPostal() != null) {
			listaParametros.add(getParametro("CODIGO_POSTAL", INTEGER, dto.getCodigoPostal()));
		}
		if (dto.getEntreCalle1() != null) {
			listaParametros.add(getParametro("entre_calle_1", STRING, dto.getEntreCalle1()));
		}
		if (dto.getEntreCalle2() != null) {
			listaParametros.add(getParametro("entre_calle_2", STRING, dto.getEntreCalle2()));
		}
		if (dto.getLatitud() != null) {
			listaParametros.add(getParametro("latitud", "Double", dto.getLatitud()));
		}
		if (dto.getLongitud() != null) {
			listaParametros.add(getParametro("longitud", "Double", dto.getLongitud()));
		}
		if (dto.getEdad() != null) {
			listaParametros.add(getParametro("EDAD", INTEGER, dto.getEdad()));
		}
		if (dto.getEdificio() != null) {
			listaParametros.add(getParametro("EDIFICIO", STRING, dto.getEdificio()));
		}
		if (dto.getFechaInsercion() != null) {
			listaParametros.add(getParametro("fecha_insercion", STRING, dto.getFechaInsercion()));
		}
		if (dto.getIdDireccion() != null) {
			listaParametros.add(getParametro("id_direccion", INTEGER, dto.getIdDireccion()));
		}
		if (dto.getIdLocalidad() != null) {
			listaParametros.add(getParametro("id_localidad", INTEGER, dto.getIdLocalidad()));
		}
		if (dto.getIdPuntoInteres() != null) {
			listaParametros.add(getParametro("id_punto_interes", INTEGER, dto.getIdPuntoInteres()));
		}
		if (dto.getTipoDenunciante() != null) {
			listaParametros.add(getParametro("id_tipo_denunciante", INTEGER, dto.getTipoDenunciante()));
		}
		if (dto.getPiso() != null) {
			listaParametros.add(getParametro("piso", STRING, dto.getPiso()));
		}
		if (dto.getIdSexo() != null) {
			listaParametros.add(getParametro("sexo", INTEGER, dto.getIdSexo()));
		}
		if (dto.getNumeroInterior() != null) {
			listaParametros.add(getParametro("numero_interior", STRING, dto.getNumeroInterior()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<List<DatosDenuncianteDTO>> reporteDatosDenunciante(DatosDenuncianteDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catContacto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", STRING, dto.getIdEvento2()));
		parametrosDataDTO.setParam(listaParametros);
		String consulta = consulta(parametrosDataDTO);
		return (Mono<List<DatosDenuncianteDTO>>) AnnotationService.getListData(consulta, DatosDenuncianteDTO.class);
	}

}
