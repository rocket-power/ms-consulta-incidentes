package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.CatalogoTipoCabelloDTO;
import com.promad.catalogos.dto.CatalogoTipoIdentificacionDTO;
import com.promad.catalogos.dto.CatalogoTipoNarizDTO;
import com.promad.catalogos.dto.CatalogoTipoPersonaInvolucradaDTO;
import com.promad.catalogos.dto.ColorCabelloDTO;
import com.promad.catalogos.dto.ColorOjosDTO;
import com.promad.catalogos.dto.ColorTezDTO;
import com.promad.catalogos.dto.ColorVestimentaDTO;
import com.promad.catalogos.dto.ComplexionDTO;
import com.promad.catalogos.dto.EstaturaDTO;
import com.promad.catalogos.dto.FormaDeCaraDTO;
import com.promad.catalogos.dto.PersonaInvolucradasDTO;
import com.promad.catalogos.dto.PrendaVestimentaDTO;
import com.promad.catalogos.service.IPersonasInvolucradasService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class PersonaInvoServiceImpl extends BaseService implements IPersonasInvolucradasService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.cat_accesorios}")
	private String catPerInvo;

	@Value("${nombre.store.procedure.cat_color_ojos}")
	private String catalogoPersonaColorOjos;

	@Value("${nombre.store.procedure.cat_color_cabello}")
	private String catalogoPersonaColorCabello;

	@Value("${nombre.store.procedure.cat_color_tez}")
	private String catalogoPersonaColorTez;

	@Value("${nombre.store.procedure.cat_color_vestimenta}")
	private String catalogoPersonaColorVestimenta;

	@Value("${nombre.store.procedure.cat_complexion}")
	private String catalogoPersonaComplexion;

	@Value("${nombre.store.procedure.cat_estatura}")
	private String catalogoPersonaEstatura;

	@Value("${nombre.store.procedure.cat_forma_cara}")
	private String catalogoPersonaFormaCara;

	@Value("${nombre.store.procedure.cat_vestimenta}")
	private String catalogoVestimenta;

	@Value("${nombre.store.procedure.cat_sexo}")
	private String catalogoSexo;

	@Value("${nombre.store.procedure.cat_tipo_cabello}")
	private String catalogoTipoCabello;

	@Value("${nombre.store.procedure.cat_tipo_nariz}")
	private String catalogoTipoNariz;

	@Value("${nombre.store.procedure.cat_tipo_identificacion}")
	private String catalogoTipoIdentificacion;

	@Value("${nombre.store.procedure.cat_tipo_persona}")
	private String catalogoTipoPersonaInvolucrada;

	@Override
	public Mono<String> obteneAccesorio(PersonaInvolucradasDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catPerInvo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, PersonaInvolucradasDTO.class);
	}

	@Override
	public Mono<String> obtenerColorOjos(ColorOjosDTO dto) {

		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaColorOjos);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ColorOjosDTO.class);
	}

	@Override
	public Mono<String> obtenerColorCabello(ColorCabelloDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaColorCabello);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ColorCabelloDTO.class);
	}

	@Override
	public Mono<String> obtenerColorVestimenta(ColorVestimentaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaColorVestimenta);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ColorVestimentaDTO.class);
	}

	@Override
	public Mono<String> obtenerComplexion(ComplexionDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaComplexion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ComplexionDTO.class);
	}

	@Override
	public Mono<String> obtenerEstatura(EstaturaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaEstatura);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, EstaturaDTO.class);
	}

	@Override
	public Mono<String> obtenerColorTez(ColorTezDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaColorTez);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ColorTezDTO.class);
	}

	@Override
	public Mono<String> obtenerFormaCara(FormaDeCaraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoPersonaFormaCara);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, FormaDeCaraDTO.class);
	}

	@Override
	public Mono<String> obtenerPrenda(PrendaVestimentaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoVestimenta);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getPorcionVestimenta() != null) {
			listaParametros.add(getParametro("porcion_vestimenta", STRING, dto.getPorcionVestimenta()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, PrendaVestimentaDTO.class);
	}

	@Override
	public Mono<String> obtenerSexo() {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoSexo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		parametrosDataDTO.setParam(listaParametros);
		return Mono.just(consulta(parametrosDataDTO));
	}

	@Override
	public Mono<String> obtenerTipoPersonaInvolucrada(CatalogoTipoPersonaInvolucradaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoPersonaInvolucrada);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoTipoPersonaInvolucradaDTO.class);
	}

	@Override
	public Mono<String> obtenerTipoIdentificacion(CatalogoTipoIdentificacionDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoIdentificacion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoTipoIdentificacionDTO.class);
	}

	@Override
	public Mono<String> obtenerTipoNariz(CatalogoTipoNarizDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoNariz);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoTipoNarizDTO.class);
	}

	@Override
	public Mono<String> obtenerTipoCabello(CatalogoTipoCabelloDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoCabello);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoTipoCabelloDTO.class);
	}

}
