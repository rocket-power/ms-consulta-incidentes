package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatalogoVehiculoMarcaDTO;
import com.promad.catalogos.service.ICatalogoMarcaVehiculo;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class CatalogoMarcaVehiculoImpl extends BaseService implements ICatalogoMarcaVehiculo {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_marca}")
	private String catalogoMarca;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Override
	public Mono<String> obtenerCatalogoMarcaVehiculo(CatalogoVehiculoMarcaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoMarca);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoVehiculoMarcaDTO.class);
	}

	@Override
	public Mono<String> obtenerCatalogoMarcaVehiculoPorId(CatalogoVehiculoMarcaDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoMarca);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = super.consulta(parametrosDataDTO);
		List<CatalogoVehiculoMarcaDTO> lista = AnnotationService.getListData(json, CatalogoVehiculoMarcaDTO.class);
		for (CatalogoVehiculoMarcaDTO item : lista) {
			if (item.getIdMarca().equals(dto.getIdMarca())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just( "No se encontro marca vehiculo con id:" + dto.getIdMarca() );
	}

}
