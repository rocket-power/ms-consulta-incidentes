package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatInstitucionesDTO;
import com.promad.catalogos.service.ICatInstitucionesService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class CatInstitucionesServiceImpl extends BaseService implements ICatInstitucionesService {

	@Value("${nombre.paquete.catalogo}")

	private String catalogo;
	@Value("${nombre.paquete.reportes}")
	private String reportes;

	@Value("${spring.application.name}")
	private String nombreMS;
	@Value("${nombre.store.procedure.cat_institucion}")
	private String catInstitucion;

	@Value("${nombre.store.procedure.dependencias}")
	private String dependencias;

	@Override
	public Mono<String> obtenerCatalogoInstituciones(CatInstitucionesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catInstitucion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatInstitucionesDTO.class);
	}

	@Override
	public Mono<String> insertarCatalogoInstituciones(CatInstitucionesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catInstitucion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("nombre_institucion", STRING, dto.getNombreInstitucion()));
		listaParametros.add(getParametro("url_imagen", STRING, dto.getUrlImagen()));
		listaParametros.add(getParametro("atiende_eventos", STRING, dto.getAtiendeEventos()));
		listaParametros.add(getParametro("atiende_lesionados", STRING, dto.getAtiendeLesionados()));
		listaParametros.add(getParametro("procesa_detenidos", STRING, dto.getProcesaDetenidos()));
		listaParametros.add(getParametro("traslada_personas", STRING, dto.getTrasladaPersonas()));
		listaParametros.add(getParametro("rastrea_vehiculos", STRING, dto.getRastreaVehiculos()));
		listaParametros.add(getParametro("id_municipio", INTEGER, dto.getIdMunicipio()));
		listaParametros.add(getParametro("id_estado", INTEGER, dto.getIdEstado()));
		listaParametros.add(getParametro("id_localidad", INTEGER, dto.getIdLocalidad()));
		listaParametros.add(getParametro("entre_calle_1", STRING, dto.getEntreCalle1()));
		listaParametros.add(getParametro("entre_calle_2", STRING, dto.getEntreCalle2()));
		listaParametros.add(getParametro("numero_interior", STRING, dto.getNumeroInterior()));

		listaParametros.add(getParametro("creado_por", INTEGER, dto.getCreadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return inserta(parametrosDataDTO);
	}

	@Override
	public Mono<String> actualizarCatalogoInstituciones(CatInstitucionesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catInstitucion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_institucion", INTEGER, dto.getIdInstitucion()));
		listaParametros.add(getParametro("nombre_institucion", STRING, dto.getNombreInstitucion()));
		listaParametros.add(getParametro("url_imagen", STRING, dto.getUrlImagen()));
		listaParametros.add(getParametro("atiende_eventos", STRING, dto.getAtiendeEventos()));
		listaParametros.add(getParametro("atiende_lesionados", STRING, dto.getAtiendeLesionados()));
		listaParametros.add(getParametro("procesa_detenidos", STRING, dto.getProcesaDetenidos()));
		listaParametros.add(getParametro("traslada_personas", STRING, dto.getTrasladaPersonas()));
		listaParametros.add(getParametro("rastrea_vehiculos", STRING, dto.getRastreaVehiculos()));
		listaParametros.add(getParametro("id_municipio", INTEGER, dto.getIdMunicipio()));
		listaParametros.add(getParametro("id_estado", INTEGER, dto.getIdEstado()));
		listaParametros.add(getParametro("id_localidad", "integer", dto.getIdLocalidad()));
		listaParametros.add(getParametro("entre_calle_1", STRING, dto.getEntreCalle1()));
		listaParametros.add(getParametro("entre_calle_2", STRING, dto.getEntreCalle2()));
		listaParametros.add(getParametro("numero_interior", STRING, dto.getNumeroInterior()));

		listaParametros.add(getParametro("modificado_por", "Integer", dto.getModificadoPor()));
		parametrosDataDTO.setParam(listaParametros);
		return actualiza(parametrosDataDTO);
	}

	@Override
	public Mono<String> obtenerCatInstitucionId(CatInstitucionesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catInstitucion);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<CatInstitucionesDTO> lista = AnnotationService.getListData(json, CatInstitucionesDTO.class);

		for (CatInstitucionesDTO item : lista) {
			if (item.getIdInstitucion().equals(dto.getIdInstitucion())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se encontro escolaridad con el Id: " + dto.getIdInstitucion());
	}

	@Override
	public Mono<String> obtenerCatalogoInstitucionesAtienden(CatInstitucionesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(reportes, dependencias);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatInstitucionesDTO.class);
	}

}
