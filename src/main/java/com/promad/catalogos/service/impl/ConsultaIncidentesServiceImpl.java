package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.ConsultaIncidentesDTO;
import com.promad.catalogos.service.IConsultaIncidentesService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ConsultaIncidentesServiceImpl extends BaseService implements IConsultaIncidentesService {

	@Value("${nombre.paquete.consulta_incidentes}")
	private String consultaIncidentes;

	@Value("${nombre.store.procedure.consulta_evento}")
	private String consulta;

	@Override
	public Mono <String> obtenerConsultaIncidentes(ConsultaIncidentesDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(consultaIncidentes, consulta);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("id_rol_usuario", INTEGER, dto.getIdRolUsuario()));
		if (dto.getUuid() != null) {
			listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		}
		if (dto.getTiempo() != null) {
			listaParametros.add(getParametro("tiempo", INTEGER, dto.getTiempo()));
		}
		if (dto.getFolioDesde() != null) {
			listaParametros.add(getParametro("folio_desde", STRING, dto.getFolioDesde()));
			if (dto.getFolioHasta() != null) {
				listaParametros.add(getParametro("folio_hasta", STRING, dto.getFolioHasta()));
			} else {
				listaParametros.add(getParametro("folio_hasta", STRING, dto.getFolioDesde()));
			}
		}
		if (dto.getPageNumber() != null) {
			listaParametros.add(getParametro("pageNumber", INTEGER, dto.getPageNumber()));
		}
		if (dto.getPageSize() != null) {
			listaParametros.add(getParametro("pageSize", INTEGER, dto.getPageSize()));
		}
		if (dto.getFechaDesde() != null) {
			listaParametros.add(getParametro("fecha_inicio", STRING, dto.getFechaDesde()));
		}
		if (dto.getFechaHasta() != null) {
			listaParametros.add(getParametro("fecha_fin", STRING, dto.getFechaHasta()));
		}
		if (dto.getA() != null) {
			listaParametros.add(getParametro("a", STRING, dto.getA()));
			if (dto.getNombreColonia() != null) {
				listaParametros.add(getParametro("nombre_colonia", STRING, dto.getNombreColonia()));
			}
			if (dto.getCalle() != null) {
				listaParametros.add(getParametro("calle", STRING, dto.getCalle()));
			}
			if (dto.getIdMotivo() != null) {
				listaParametros.add(getParametro("id_motivo", STRING, dto.getIdMotivo().toString()));
			}
			if (dto.getClaveOperativa() != null) {
				listaParametros.add(getParametro("clave_operativa", STRING, dto.getClaveOperativa()));
			}
			if (dto.getIdOrigen() != null) {
				listaParametros.add(getParametro("id_origen", STRING, dto.getIdOrigen().toString()));
			}
			if (dto.getPrioridad() != null) {
				listaParametros.add(getParametro("prioridad", STRING, dto.getPrioridad()));
			}
			if (dto.getTelefono() != null && dto.getTelefono().length() >= 4) {
				listaParametros.add(getParametro("telefono", STRING, dto.getTelefono()));
			}
			if (dto.getIdInstitucion() != null) {
				listaParametros.add(getParametro("id_institucion", STRING, dto.getIdInstitucion()));
			}
			if (dto.getIdMunicipio() != null) {
				listaParametros.add(getParametro("id_municipio", STRING, dto.getIdMunicipio()));
			}
			if (dto.getIdLocalidad() != null) {
				listaParametros.add(getParametro("id_localidad", STRING, dto.getIdLocalidad()));
			}
			if (dto.getEstatus() != null) {
				listaParametros.add(getParametro("estatus", STRING, dto.getEstatus()));
			}
			if (dto.getLugar() != null) {
				listaParametros.add(getParametro("lugar", STRING, dto.getLugar()));
			}
			if (dto.getDescripcion() != null) {
				listaParametros.add(getParametro("descripcion", STRING, dto.getDescripcion()));
			}
		}

		if (dto.getB() != null) {
			listaParametros.add(getParametro("b", STRING, dto.getB()));
			if (dto.getNombre() != null) {
				listaParametros.add(getParametro("nombre", STRING, dto.getNombre()));
			}
			if (dto.getApellidoPaterno() != null) {
				listaParametros.add(getParametro("paterno", STRING, dto.getApellidoPaterno()));
			}
			if (dto.getApellidoMaterno() != null) {
				listaParametros.add(getParametro("materno", STRING, dto.getApellidoMaterno()));
			}
			if (dto.getAlias() != null) {
				listaParametros.add(getParametro("alias_pi", STRING, dto.getAlias()));
			}
			if (dto.getSeniasParticulares() != null) {
				listaParametros.add(getParametro("senia_p", STRING, dto.getSeniasParticulares()));
			}
			if (dto.getEdad() != null) {
				listaParametros.add(getParametro("edad", STRING, dto.getEdad()));
			}
			if (dto.getEstatura() != null) {
				listaParametros.add(getParametro("estatura", STRING, dto.getEstatura()));
			}
			if (dto.getIdTipoPersona() != null) {
				listaParametros.add(getParametro("tipo_persona", STRING, dto.getIdTipoPersona().toString()));
			}
			if (dto.getIdComplexion() != null) {
				listaParametros.add(getParametro("complexion", STRING, dto.getIdComplexion().toString()));
			}
			if (dto.getIdTipoCabello() != null) {
				listaParametros.add(getParametro("tipo_cabello", STRING, dto.getIdTipoCabello().toString()));
			}
			if (dto.getIdColorCabello() != null) {
				listaParametros.add(getParametro("color_cabello", STRING, dto.getIdColorCabello().toString()));
			}
			if (dto.getGenero() != null) {
				listaParametros.add(getParametro("sexo", STRING, dto.getGenero()));
			}
			if (dto.getIdFormaCara() != null) {
				listaParametros.add(getParametro("forma_cara", STRING, dto.getIdFormaCara().toString()));
			}
			if (dto.getIdColorTez() != null) {
				listaParametros.add(getParametro("color_tez", STRING, dto.getIdColorTez().toString()));
			}
			if (dto.getIdColorOjos() != null) {
				listaParametros.add(getParametro("color_ojos", STRING, dto.getIdColor().toString()));
			}
			if (dto.getIdTipoNariz() != null) {
				listaParametros.add(getParametro("tipo_nariz", STRING, dto.getIdTipoNariz().toString()));
			}
			if (dto.getCabezaTipo() != null) {
				listaParametros.add(getParametro("cabeza_tipo", STRING, dto.getCabezaTipo()));
			}
			if (dto.getIdCabezaColor() != null) {
				listaParametros.add(getParametro("cabeza_color", STRING, dto.getIdCabezaColor()));
			}
			if (dto.getTalleTipo() != null) {
				listaParametros.add(getParametro("talle_tipo", STRING, dto.getTalleTipo()));
			}
			if (dto.getIdTalleColor() != null) {
				listaParametros.add(getParametro("talle_color", STRING, dto.getIdTalleColor()));
			}
			if (dto.getInferiorTipo() != null) {
				listaParametros.add(getParametro("inferior_tipo", STRING, dto.getInferiorTipo()));
			}
			if (dto.getIdInferiorColor() != null) {
				listaParametros.add(getParametro("inferior_color", STRING, dto.getIdInferiorColor()));
			}
			if (dto.getCalzadoTipo() != null) {
				listaParametros.add(getParametro("calzado_tipo", STRING, dto.getCalzadoTipo()));
			}
			if (dto.getIdCalzadoColor() != null) {
				listaParametros.add(getParametro("calzado_color", STRING, dto.getIdCalzadoColor()));
			}
		}
		if (dto.getC() != null) {
			listaParametros.add(getParametro("c", STRING, dto.getC()));
			if (dto.getIdTipoVehiculo() != null) {
				listaParametros.add(getParametro("id_tipo_vehiculo", STRING, dto.getIdTipoVehiculo().toString()));
			}
			if (dto.getIdMarca() != null) {
				listaParametros.add(getParametro("id_marca", STRING, dto.getIdMarca().toString()));
			}
			if (dto.getIdModelo() != null) {
				listaParametros.add(getParametro("id_modelo", STRING, dto.getIdModelo().toString()));
			}
			if (dto.getNumeroPlaca() != null) {
				listaParametros.add(getParametro("numero_placa", STRING, dto.getNumeroPlaca()));
			}
			if (dto.getIdEstado() != null) {
				listaParametros.add(getParametro("id_origen_v", STRING, dto.getIdEstado()));
			}
			if (dto.getNumeroSerie() != null) {
				listaParametros.add(getParametro("numero_serie", STRING, dto.getNumeroSerie()));
			}
			if (dto.getNumeroMotor() != null) {
				listaParametros.add(getParametro("numero_motor", STRING, dto.getNumeroMotor()));
			}

			if (dto.getNiv() != null) {
				listaParametros.add(getParametro("vin_num", STRING, dto.getNiv()));
			}
			if (dto.getIdColor() != null) {
				listaParametros.add(getParametro("id_color", STRING, dto.getIdColor().toString()));
			}
			if (dto.getAnio() != null) {
				listaParametros.add(getParametro("anio", STRING, dto.getAnio()));
			}

			if (dto.getIdEstatusRastreo() != null) {
				listaParametros.add(getParametro("id_estatus_rastreo", STRING, dto.getIdEstatusRastreo().toString()));
			}

			if (dto.getObservacionesveh() != null) {
				listaParametros.add(getParametro("observaciones", STRING, dto.getObservacionesveh()));
			}
			if (dto.getListaNegra() != null) {
				listaParametros.add(getParametro("lista_negra", STRING, dto.getListaNegra()));
			}
		}
		if (dto.getD() != null) {
			listaParametros.add(getParametro("d", STRING, dto.getD()));
			if (dto.getCantidad() != null) {
				listaParametros.add(getParametro("cantidad", STRING, dto.getCantidad()));
			}
			if (dto.getEstimado() != null) {
				listaParametros.add(getParametro("estimado", STRING, dto.getEstimado()));
			}
			if (dto.getIdCategoria() != null) {
				listaParametros.add(getParametro("tipo", STRING, dto.getIdCategoria().toString()));
			}
			if (dto.getIdTipoObjeto() != null) {
				listaParametros.add(getParametro("sub_tipo", STRING, dto.getIdObjeto()));
			}
			if (dto.getDescripcion() != null) {
				listaParametros.add(getParametro("descripcion", STRING, dto.getDescripcion()));
			}
		}
		if (dto.getHistorico() != null) {
			listaParametros.add(getParametro("historico", STRING, dto.getHistorico()));
		}

		if (dto.getNumeroAveriguacion() != null) {
			listaParametros.add(getParametro("NUMERO_AVERIGUACION", INTEGER, dto.getNumeroAveriguacion()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ConsultaIncidentesDTO.class);
	}

}
