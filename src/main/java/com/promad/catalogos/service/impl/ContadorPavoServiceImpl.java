package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.promad.catalogos.dto.ContadorPavoDTO;
import com.promad.catalogos.service.IContadorPavoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ContadorPavoServiceImpl extends BaseService implements IContadorPavoService {

	@Value("${nombre.paquete.despacho}")
	private String despacho;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.contador_involucrados}")
	private String contadorInvolucrados;

	@Override
	public Mono<String> obtenerContadorPavo(ContadorPavoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(despacho, contadorInvolucrados);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		listaParametros.add(getParametro("id_evento", INTEGER, dto.getIdEvento()));
		parametrosDataDTO.setParam(listaParametros);
		return super.consultaTotales(parametrosDataDTO, ContadorPavoDTO.class);
	}

}
