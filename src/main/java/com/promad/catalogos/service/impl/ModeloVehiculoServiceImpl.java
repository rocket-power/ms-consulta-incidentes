package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.ModeloVehiculoDTO;
import com.promad.catalogos.service.IModeloVehiculoService;
import reactor.core.publisher.Mono;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

@Service
@PropertySource("classpath:application.properties")

public class ModeloVehiculoServiceImpl extends BaseService implements IModeloVehiculoService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_modelo}")
	private String modeloVehiculo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Override
	public Mono<String> obtenerModeloVehiculo(ModeloVehiculoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, modeloVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		return consulta(parametrosDataDTO, ModeloVehiculoDTO.class);
	}

	public Mono<String> obtenerModeloVehiculoId(ModeloVehiculoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, modeloVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		String json = consulta(parametrosDataDTO);
		List<ModeloVehiculoDTO> lista = AnnotationService.getListData(json, ModeloVehiculoDTO.class);

		for (ModeloVehiculoDTO item : lista) {
			if (item.getIdModelo().equals(dto.getIdModelo())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se pudo encontrar el modelo vehiculo id  :" + dto.getIdModelo());
	}

}
