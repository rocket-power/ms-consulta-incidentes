package com.promad.catalogos.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.promad.catalogos.dto.BitacoraDTO;
import com.promad.catalogos.dto.CatLocalidadDTO;
import com.promad.catalogos.dto.DatosIncidenteDTO;
import com.promad.catalogos.dto.InstitucionesAsignadasDTO;
import com.promad.catalogos.dto.LlamadasAsociadasDTO;
import com.promad.catalogos.dto.ObjetosInvolucradosDTO;
import com.promad.catalogos.dto.PersonasIncidenteDTO;
import com.promad.catalogos.dto.VehiculosInvolucradosDTO;
import com.promad.catalogos.service.IParametrosReporteService;

import reports.BReportException;

@Service
@PropertySource("classpath:application.properties")
public class ParametrosReporteServiceImpl implements IParametrosReporteService {

	@Override
	public Map<String, Object> parametrosReportes(String nombreReporte, String uuid, Integer idEvento) {
		if (nombreReporte == null) {
			throw new BReportException("Reporte es requerido.");
		}
		if (uuid == null) {
			throw new BReportException("Uuid es requerido.");
		}
		if (idEvento == null) {
			throw new BReportException("idEvento es requerido.");
		}
		Map<String, Object> params = new HashMap<>();

		if (nombreReporte.equals("reporteDescriptivo")) {

			DatosIncidenteDTO dto = new DatosIncidenteDTO();
			dto.setUuid(uuid);
			dto.setIdEvento(idEvento);
			params.put("dtoDatosIncidente", dto);

			DatosIncidenteDTO dto1 = new DatosIncidenteDTO();
			dto1.setUuid(uuid);
			dto1.setIdEvento(idEvento);
			dto1.setDescripcion("TRUE");
			params.put("dtoDatosIncidenteDescripcion", dto1);

			BitacoraDTO bitacoraDTO = new BitacoraDTO();
			bitacoraDTO.setUuid(uuid);
			bitacoraDTO.setIdEvento(idEvento);
			params.put("bitacoraDTO", bitacoraDTO);

			LlamadasAsociadasDTO llamadasDTO = new LlamadasAsociadasDTO();
			llamadasDTO.setUuid(uuid);
			llamadasDTO.setIdEvento(idEvento);
			params.put("llamadasDTO", llamadasDTO);

			LlamadasAsociadasDTO llamadasDuplicadasDTO = new LlamadasAsociadasDTO();
			llamadasDuplicadasDTO.setUuid(uuid);
			llamadasDuplicadasDTO.setIdEvento(idEvento);
			llamadasDuplicadasDTO.setTipo("DUPLICADO");
			params.put("llamadasDuplicadasDTO", llamadasDuplicadasDTO);

			InstitucionesAsignadasDTO instituciones = new InstitucionesAsignadasDTO();
			instituciones.setUuid(uuid);
			instituciones.setIdEvento(idEvento);
			params.put("InstitucionesAsignadas", instituciones);

			PersonasIncidenteDTO personas = new PersonasIncidenteDTO();
			personas.setUuid(uuid);
			personas.setIdEvento(idEvento);
			params.put("PersonasInvolucradas", personas);

			VehiculosInvolucradosDTO vehiculos = new VehiculosInvolucradosDTO();
			vehiculos.setUuid(uuid);
			vehiculos.setIdEvento(idEvento);
			params.put("VehiculosInvolucrados", vehiculos);

			ObjetosInvolucradosDTO objetos = new ObjetosInvolucradosDTO();
			objetos.setUuid(uuid);
			objetos.setIdEvento(idEvento);
			params.put("ObjetosInvolucrados", objetos);
		}

		if (nombreReporte.equals("listas")) {
			CatLocalidadDTO dtoLocalidad = new CatLocalidadDTO();
			dtoLocalidad.setUuid(uuid);
			params.put("dtoLocalidad", dtoLocalidad);
		}

		return params;
	}

}
