package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.ObjetosDTO;
import com.promad.catalogos.service.ICatalogoObjetosService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;
import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class ObjetoServiceImpl extends BaseService implements ICatalogoObjetosService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_objeto}")
	private String catalogoObjeto;

	@Value("${nombre.store.procedure.cat_tipo_objeto}")
	private String catalogoTipoObjeto;

	@Value("${nombre.store.procedure.cat_categoria}")
	private String categoria;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Override
	public Mono<String> obtenerObjeto(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoObjeto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdTipoObjeto() != null) {
			listaParametros.add(getParametro("id_tipo_objeto", INTEGER, dto.getIdTipoObjeto()));
		}
		if (dto.getIdCategoria() != null) {
			listaParametros.add(getParametro("id_categoria", INTEGER, dto.getIdCategoria()));
		}
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ObjetosDTO.class);
	}

	@Override
	public Mono<String> obtenerObjetoId(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoObjeto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", "String", dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<ObjetosDTO> lista = AnnotationService.getListData(json, ObjetosDTO.class);

		for (ObjetosDTO item : lista) {
			if (item.getIdObjeto().equals(dto.getIdObjeto())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se pudo encontrar  iobjeto :" + dto.getIdObjeto());
	}

	@Override
	public Mono<String> obtenerTipoObjeto(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoObjeto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ObjetosDTO.class);
	}

	@Override
	public Mono<String> obtenerIdTipoObjeto(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoObjeto);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<ObjetosDTO> lista = AnnotationService.getListData(json, ObjetosDTO.class);

		for (ObjetosDTO item : lista) {
			if (item.getIdTipoObjeto().equals(dto.getIdTipoObjeto())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se pudo encontrar tipo objeto id :" + dto.getIdTipoObjeto());
	}

	@Override
	public Mono<String> obtenerCategoria(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, categoria);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		if (dto.getIdTipoObjeto() != null) {
			listaParametros.add(getParametro("id_tipo_objeto", INTEGER, dto.getIdTipoObjeto()));
		}

		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, ObjetosDTO.class);
	}

	@Override
	public Mono<String> obtenerIdCategoria(ObjetosDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, categoria);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<ObjetosDTO> lista = AnnotationService.getListData(json, ObjetosDTO.class);

		for (ObjetosDTO item : lista) {
			if (item.getIdCategoria().equals(dto.getIdCategoria())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se pudo encontrar el id Categoria :" + dto.getIdCategoria());
	}

}
