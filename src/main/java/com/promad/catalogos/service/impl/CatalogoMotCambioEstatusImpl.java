package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatalogoSupervisoraDTO;
import com.promad.catalogos.service.ICatalogoMotCambioEstatusService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class CatalogoMotCambioEstatusImpl extends BaseService implements ICatalogoMotCambioEstatusService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${nombre.store.procedure.cat_motivo_cambio_rastreo}")
	private String catMotivoCambioRastreo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Override
	public Mono<String> obtenerCatalogoMotCambioEst(CatalogoSupervisoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catMotivoCambioRastreo);

		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);

		return consulta(parametrosDataDTO, CatalogoSupervisoraDTO.class);
	}

	@Override
	public Mono<String> obtenerMotCambioEstatRastreoId(CatalogoSupervisoraDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catMotivoCambioRastreo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<CatalogoSupervisoraDTO> lista = AnnotationService.getListData(json, CatalogoSupervisoraDTO.class);

		for (CatalogoSupervisoraDTO item : lista) {
			if (item.getIdMotivoCambioRastreo().equals(dto.getIdMotivoCambioRastreo())) {
				return Mono.just(new Gson().toJson(item));
			}
		}
		return Mono.just("No se encontro motivo cambio estatus con id:" + dto.getIdMotivoCambioRastreo());
	}

}
