package com.promad.catalogos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.promad.catalogos.dto.CatalogoTipoVehiculoDTO;
import com.promad.catalogos.service.ICatalogoTipoVehiculoService;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;
import com.promad.service.AnnotationService;
import com.promad.service.BaseService;

import reactor.core.publisher.Mono;

@Service
@PropertySource("classpath:application.properties")
public class TipoVehiculoServiceImpl extends BaseService implements ICatalogoTipoVehiculoService {

	@Value("${nombre.paquete.catalogo}")
	private String catalogo;

	@Value("${spring.application.name}")
	private String nombreMS;

	@Value("${nombre.store.procedure.cat_tipo_vehiculo}")
	private String catalogoTipoVehiculo;

	@Override
	public Mono<String> obtenerCatTipoVehiculo(CatalogoTipoVehiculoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		return consulta(parametrosDataDTO, CatalogoTipoVehiculoDTO.class);
	}

	@Override
	public Mono<String> obtenerCatTipoVehiculoPorId(CatalogoTipoVehiculoDTO dto) {
		ParametrosDataDTO parametrosDataDTO = createParametrosData(catalogo, catalogoTipoVehiculo);
		List<ParametroDTO> listaParametros = new ArrayList<>();
		listaParametros.add(getParametro("uuid", STRING, dto.getUuid()));
		parametrosDataDTO.setParam(listaParametros);
		String json = consulta(parametrosDataDTO);
		List<CatalogoTipoVehiculoDTO> lista = AnnotationService.getListData(json, CatalogoTipoVehiculoDTO.class);
		for (CatalogoTipoVehiculoDTO item : lista) {
			if (item.getIdTipoVehiculo().equals(dto.getIdTipoVehiculo())) {
				return Mono.just( new Gson().toJson(item));
			}
		}
		return Mono.just( "No se encontro Tipo Vehiculo con id:" + dto.getIdTipoVehiculo());
	}

}
