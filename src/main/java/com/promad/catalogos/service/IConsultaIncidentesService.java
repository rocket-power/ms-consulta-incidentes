package com.promad.catalogos.service;

import com.promad.catalogos.dto.ConsultaIncidentesDTO;

import reactor.core.publisher.Mono;

public interface IConsultaIncidentesService {

	public Mono<String> obtenerConsultaIncidentes(ConsultaIncidentesDTO dto);

}
