package com.promad.catalogos.service;

import com.promad.catalogos.dto.SuscriptorDeProgramasEspecialesDTO;

import reactor.core.publisher.Mono;

public interface ISuscriptorDeProgramasEspecialesService {
	
	public Mono<String> obtenerSuscriptor(SuscriptorDeProgramasEspecialesDTO dto);

	public Mono<String> obtenerProgramasEspeciales(SuscriptorDeProgramasEspecialesDTO dto);

}
