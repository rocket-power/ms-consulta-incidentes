package com.promad.catalogos.service;

import com.promad.catalogos.dto.ReactivarIncidenteDTO;

import reactor.core.publisher.Mono;

public interface IReactivarIncidenteService {

	public Mono<String> obtenerReactivarIncidente(ReactivarIncidenteDTO dto);

	public Mono<String> insertarReactivarIncidente(ReactivarIncidenteDTO dto);

	public Mono<String> obtenerTipoBitacora(ReactivarIncidenteDTO dto);

}
