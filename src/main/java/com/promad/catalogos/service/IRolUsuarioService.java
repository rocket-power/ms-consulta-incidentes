package com.promad.catalogos.service;

import com.promad.catalogos.dto.RolUsuarioDTO;

import reactor.core.publisher.Mono;

public interface IRolUsuarioService {

	public Mono<String> obtenerRolUsuario(RolUsuarioDTO dto);
}
