package com.promad.catalogos.service;

import com.promad.catalogos.dto.ReactivarIncidenteDTO;

public interface IReasctivarIncidenteService {

	public String obtenerReactivarIncidente(ReactivarIncidenteDTO dto);

	public String insertarReactivarIncidente(ReactivarIncidenteDTO dto);

	public String obtenerTipoBitacora(ReactivarIncidenteDTO dto);

}
