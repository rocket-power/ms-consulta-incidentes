package com.promad.catalogos.service;

import java.util.Map;

public interface IReporteContextDataService {

	public Map<String, Object> parametrosDataReporte(String nombreReporte, Map<String, ?> parameters);

}
