package com.promad.catalogos.service;

import java.util.List;
import com.promad.catalogos.dto.ReporteDescriptivoDTO;

import reactor.core.publisher.Mono;

public interface IReporteDescriptivoService {

	public Mono<String> obtenerReporteDescriptivo(ReporteDescriptivoDTO dto);

	public Mono<List<ReporteDescriptivoDTO>> datosLlamada(ReporteDescriptivoDTO dto);

	public Mono<String> obtenerTiemposIncidente(ReporteDescriptivoDTO dto);

	public Mono<List<ReporteDescriptivoDTO>> datosTiemposLlamada(ReporteDescriptivoDTO dto);

}