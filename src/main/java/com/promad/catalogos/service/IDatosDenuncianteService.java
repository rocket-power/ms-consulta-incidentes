package com.promad.catalogos.service;

import java.util.List;
import com.promad.catalogos.dto.DatosDenuncianteDTO;

import reactor.core.publisher.Mono;

public interface IDatosDenuncianteService {
	public Mono<String> obtenerDatosDenunciante(DatosDenuncianteDTO dto);

	public Mono<String> actualizarDatosDenunciante(DatosDenuncianteDTO dto);

	public Mono<List<DatosDenuncianteDTO>> reporteDatosDenunciante(DatosDenuncianteDTO dto);
}
