package com.promad.catalogos.service;

import com.promad.catalogos.dto.EstatusRastreoDTO;

import reactor.core.publisher.Mono;

public interface IEstatusRastreoService {

	public Mono<String> obtenerCatEstatusRastreoPorId(EstatusRastreoDTO dto);

	public Mono<String> obtenerCatEstatusRastreo(EstatusRastreoDTO dto);
}
