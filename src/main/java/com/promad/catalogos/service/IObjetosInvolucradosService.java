package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.ObjetosInvolucradosDTO;

import reactor.core.publisher.Mono;

public interface IObjetosInvolucradosService {

	public Mono<String> obtenerObjetosInvolucrados(ObjetosInvolucradosDTO dto);

	public Mono<String> insertarObjetosInvolucrados(ObjetosInvolucradosDTO dto);

	public Mono<String> actualizarObjetosInvolucrados(ObjetosInvolucradosDTO dto);

	public Mono<String> obtenerObjetosInvolucradosId(ObjetosInvolucradosDTO dto);

	public Mono<String> obtenerCatalogoTipoMoneda(ObjetosInvolucradosDTO dto);

	public Mono<List<ObjetosInvolucradosDTO>> reporteListaObjetosInvolucrados(ObjetosInvolucradosDTO dto);

}
