package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatalogoVehiculoMarcaDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoMarcaVehiculo {

	public Mono<String> obtenerCatalogoMarcaVehiculoPorId(CatalogoVehiculoMarcaDTO dto);

	public Mono<String> obtenerCatalogoMarcaVehiculo(CatalogoVehiculoMarcaDTO dto);

}
