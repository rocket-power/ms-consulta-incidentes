package com.promad.catalogos.service;

import com.promad.catalogos.dto.ModeloVehiculoDTO;

import reactor.core.publisher.Mono;

public interface IModeloVehiculoService {

	public Mono<String> obtenerModeloVehiculo(ModeloVehiculoDTO dto);

	public Mono<String> obtenerModeloVehiculoId(ModeloVehiculoDTO dto);
}
