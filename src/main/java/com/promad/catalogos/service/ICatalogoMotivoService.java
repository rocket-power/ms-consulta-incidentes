package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatalogoMotivoDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoMotivoService {
	
	public Mono<String> obtenerMotivo(CatalogoMotivoDTO dto);

	public Mono<String> obtenerIdMotivo(CatalogoMotivoDTO dto);
}
