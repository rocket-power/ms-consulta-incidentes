package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.CatLocalidadDTO;

import reactor.core.publisher.Mono;

public interface ICatLocalidadService {

	public Mono<String>obtenerCatLocalidad(CatLocalidadDTO dto);

	public Mono<String>insertarCatLocalidad(CatLocalidadDTO dto);

	public Mono<String>actualizarCatLocalidad(CatLocalidadDTO dto);

	public Mono<String>eliminarCatLocalidad(CatLocalidadDTO dto);

	public Mono<String>busquedaIdCatLocalidad(CatLocalidadDTO dto);

	public Mono<List<CatLocalidadDTO>> obtenerCatLocalidadreporte(CatLocalidadDTO dto);

}
