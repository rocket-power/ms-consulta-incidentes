package com.promad.catalogos.service;

import java.util.Map;

public interface IParametrosReporteService {

	public Map<String, Object> parametrosReportes(String nombreReporte, String uuid, Integer idEvento);
}
