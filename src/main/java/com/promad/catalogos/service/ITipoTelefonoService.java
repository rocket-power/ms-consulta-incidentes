package com.promad.catalogos.service;

import com.promad.catalogos.dto.TipoTelefonoDTO;

import reactor.core.publisher.Mono;

public interface ITipoTelefonoService {

	public Mono<String> obtenerTipoTelefono(TipoTelefonoDTO dto);
}
