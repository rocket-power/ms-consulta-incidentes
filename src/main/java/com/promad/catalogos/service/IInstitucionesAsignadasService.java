package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.InstitucionesAsignadasDTO;

import reactor.core.publisher.Mono;

public interface IInstitucionesAsignadasService {

	public Mono<String> obtenerInstitucionesAsignadas(InstitucionesAsignadasDTO dto);

	public Mono<String> obtenerDetalleInstitucion(InstitucionesAsignadasDTO dto);

	public Mono<String> obtenerRecursosAsignados(InstitucionesAsignadasDTO dto);

	public Mono<String> obtenerRecursosAsignadosId(InstitucionesAsignadasDTO dto);

	public Mono<String> insertarNotasCierre(InstitucionesAsignadasDTO dto);

	public Mono<String> insertarInstitucionEvento(InstitucionesAsignadasDTO dto);

	public Mono<String> obtenerInstitucionesNoAsignadas(InstitucionesAsignadasDTO dto);

	public Mono<List<InstitucionesAsignadasDTO>> reporteInstitucionesAsignadas(InstitucionesAsignadasDTO dto);

}
