package com.promad.catalogos.service;

import com.promad.catalogos.dto.ContadorPavoDTO;

import reactor.core.publisher.Mono;

public interface IContadorPavoService {

	public Mono<String> obtenerContadorPavo(ContadorPavoDTO dto);
}
