package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatalogoTipoCabelloDTO;
import com.promad.catalogos.dto.CatalogoTipoIdentificacionDTO;
import com.promad.catalogos.dto.CatalogoTipoNarizDTO;
import com.promad.catalogos.dto.CatalogoTipoPersonaInvolucradaDTO;
import com.promad.catalogos.dto.ColorCabelloDTO;
import com.promad.catalogos.dto.ColorOjosDTO;
import com.promad.catalogos.dto.ColorTezDTO;
import com.promad.catalogos.dto.ColorVestimentaDTO;
import com.promad.catalogos.dto.ComplexionDTO;
import com.promad.catalogos.dto.EstaturaDTO;
import com.promad.catalogos.dto.FormaDeCaraDTO;
import com.promad.catalogos.dto.PersonaInvolucradasDTO;
import com.promad.catalogos.dto.PrendaVestimentaDTO;

import reactor.core.publisher.Mono;

public interface IPersonasInvolucradasService {

	public Mono<String> obteneAccesorio(PersonaInvolucradasDTO dto);

	public Mono<String> obtenerColorOjos(ColorOjosDTO dto);

	public Mono<String> obtenerColorCabello(ColorCabelloDTO dto);

	public Mono<String> obtenerColorTez(ColorTezDTO dto);

	public Mono<String> obtenerColorVestimenta(ColorVestimentaDTO dto);

	public Mono<String> obtenerComplexion(ComplexionDTO dto);

	public Mono<String> obtenerEstatura(EstaturaDTO dto);

	public Mono<String> obtenerFormaCara(FormaDeCaraDTO dto);

	public Mono<String> obtenerPrenda(PrendaVestimentaDTO dto);

	public Mono<String> obtenerSexo();

	public Mono<String> obtenerTipoCabello(CatalogoTipoCabelloDTO dto);

	public Mono< String> obtenerTipoNariz(CatalogoTipoNarizDTO dto);

	public Mono<String> obtenerTipoIdentificacion(CatalogoTipoIdentificacionDTO dto);

	public Mono<String> obtenerTipoPersonaInvolucrada(CatalogoTipoPersonaInvolucradaDTO dto);

}
