package com.promad.catalogos.service;

import java.util.List;
import com.promad.catalogos.dto.LlamadasAsociadasDTO;
import reactor.core.publisher.Mono;

public interface LlamadasAsociadasService {

	public Mono<String> obtenerLlamadasAsociadas(LlamadasAsociadasDTO dto);

	public Mono<List<LlamadasAsociadasDTO>> reporteLlamadasAsociadas(LlamadasAsociadasDTO dto);

}
