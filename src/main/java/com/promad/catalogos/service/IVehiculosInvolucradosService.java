
package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.VehiculosInvolucradosDTO;

import reactor.core.publisher.Mono;

public interface IVehiculosInvolucradosService {

	public Mono<String> obtenerVehiculoInvolucrado(VehiculosInvolucradosDTO dto);

	public Mono<String> insertarVehiculoInvolucrado(VehiculosInvolucradosDTO dto);

	public Mono<String> busquedaIdVehiculoInvolucrado(VehiculosInvolucradosDTO dto);

	public Mono<String> obtenerAnios(VehiculosInvolucradosDTO dto);

	public Mono<String> actualizarVehiculoInvolucrado(VehiculosInvolucradosDTO dto);

	public Mono<List<VehiculosInvolucradosDTO>> reporteListaVehiculoInvolucrado(VehiculosInvolucradosDTO dto);

}
