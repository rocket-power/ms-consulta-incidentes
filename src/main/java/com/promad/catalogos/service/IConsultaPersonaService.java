package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.PersonasIncidenteDTO;

import reactor.core.publisher.Mono;

public interface IConsultaPersonaService {

	public Mono<String> obtenerPersonaEvento(PersonasIncidenteDTO dto);

	public Mono<String> obtenerPersonaInvolucradas(PersonasIncidenteDTO dto);

	public Mono<String> insertarPersonaInvolucradas(PersonasIncidenteDTO dto);

	public Mono<String> actualizarPersonaInvolucradas(PersonasIncidenteDTO dto);

	public Mono<String> obtenerAccesorios(PersonasIncidenteDTO dto);

	public Mono<String> eliminarAccesorio(PersonasIncidenteDTO dto);

	public Mono<List<PersonasIncidenteDTO>> reporteListaPersonaEvento(PersonasIncidenteDTO dto);

}
