package com.promad.catalogos.service;

import java.util.Map;

import com.promad.catalogos.dto.ReporteTemplateDTO;

public interface ITemplateService {

	public String obtenerTemplate(ReporteTemplateDTO dto);

	public String guardarTemplate(ReporteTemplateDTO dto);

	public Map<String, Object> parametrosReportesOperativos(String template, String uuid, String turno);

	public Map<String, Object> parametrosDetalleEstatus(String uuid, Integer idSupervisor, Integer idUsuario);

	public Map<String, Object> reporteDescriptivo(String template, String uuid, String idEvento);

	public Map<String, Object> reporteDescriptivo(String template, String uuid, Integer idEvento);

}
