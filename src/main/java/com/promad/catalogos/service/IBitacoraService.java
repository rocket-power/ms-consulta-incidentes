package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.BitacoraDTO;

import reactor.core.publisher.Mono;

public interface IBitacoraService {
	public Mono<String> obtenerBitacora(BitacoraDTO dto);

	public Mono<List<BitacoraDTO>> reporteBitacora(BitacoraDTO dto);

	public Mono<String> obtenerTiemposEvento(BitacoraDTO dto);

	public Mono<List<BitacoraDTO>> reporteTiemposEvento(BitacoraDTO dto);

}
