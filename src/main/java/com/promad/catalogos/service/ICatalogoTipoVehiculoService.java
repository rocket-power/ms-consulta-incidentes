package com.promad.catalogos.service;

import org.springframework.http.ResponseEntity;

import com.promad.catalogos.dto.CatalogoTipoVehiculoDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoTipoVehiculoService {

	public Mono<String> obtenerCatTipoVehiculoPorId(CatalogoTipoVehiculoDTO dto);

	public Mono<String> obtenerCatTipoVehiculo(CatalogoTipoVehiculoDTO dto);
}
