package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatalogosVehiculosDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoVehiculoService {

	public Mono<String> obtenerCatColorVehiculoPorId(CatalogosVehiculosDTO dto);

	public Mono<String> obtenerCatColorVehiculo(CatalogosVehiculosDTO dto);
}
