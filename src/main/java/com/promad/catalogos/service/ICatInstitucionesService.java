package com.promad.catalogos.service;

import com.promad.catalogos.dto.CatInstitucionesDTO;

import reactor.core.publisher.Mono;

public interface ICatInstitucionesService {

	public Mono<String> obtenerCatalogoInstituciones(CatInstitucionesDTO dto);

	public Mono<String> insertarCatalogoInstituciones(CatInstitucionesDTO dto);

	public Mono<String> actualizarCatalogoInstituciones(CatInstitucionesDTO dto);

	public Mono<String> obtenerCatInstitucionId(CatInstitucionesDTO dto);

	public Mono<String> obtenerCatalogoInstitucionesAtienden(CatInstitucionesDTO dto);

}
