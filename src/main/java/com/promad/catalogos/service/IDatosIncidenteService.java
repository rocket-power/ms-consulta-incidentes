package com.promad.catalogos.service;

import java.util.List;

import com.promad.catalogos.dto.DatosIncidenteDTO;

import reactor.core.publisher.Mono;

public interface IDatosIncidenteService {

	public Mono<String> obtenerDatosIncidente(DatosIncidenteDTO dto);

	public Mono<String> insertarDatosIncidente(DatosIncidenteDTO dto);

	public Mono<String> actualizarDatosIncidente(DatosIncidenteDTO dto);

	public Mono<String> obtenerHistorialGeoreferencias(DatosIncidenteDTO dto);

	public Mono<String> obtenerOrigen(DatosIncidenteDTO dto);

	public Mono<String> obtenerResultadoIncidente(DatosIncidenteDTO dto);

	public Mono<String> insertarDescripcionLugarIncidente(DatosIncidenteDTO dto);

	public Mono<List<DatosIncidenteDTO>>reporteDatosIncidente(DatosIncidenteDTO dto);

}
