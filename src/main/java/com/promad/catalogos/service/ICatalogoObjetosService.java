package com.promad.catalogos.service;

import com.promad.catalogos.dto.ObjetosDTO;

import reactor.core.publisher.Mono;

public interface ICatalogoObjetosService {

	public Mono<String> obtenerObjeto(ObjetosDTO dto);

	public Mono<String> obtenerObjetoId(ObjetosDTO dto);

	public Mono<String> obtenerTipoObjeto(ObjetosDTO dto);

	public Mono<String> obtenerIdTipoObjeto(ObjetosDTO dto);

	public Mono<String> obtenerCategoria(ObjetosDTO dto);

	public Mono<String> obtenerIdCategoria(ObjetosDTO dto);
}
