package com.promad.catalogos.exception;

public class CatalogoException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public CatalogoException() {
		super();
	}
	
	
	public CatalogoException(String message) {
		super(message);
	}
	
	public CatalogoException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CatalogoException(Throwable throwable) {
		super(throwable);
	}

}
