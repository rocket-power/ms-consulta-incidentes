package com.promad.catalogos.cliente;

import java.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import com.promad.repository.dto.ParametrosDataDTO;
import reactor.core.publisher.Mono;
@Service
public class ClienteWeb {
	private Logger logger = LoggerFactory.getLogger(ClienteWeb.class);
	private final String msqServiceUrl;
	private final String mscqServiceUrl;
	private final WebClient.Builder webClientBuilder;
	private WebClient webClient;
	private final int msDataQServiceTimeoutSec;
	public ClienteWeb(WebClient.Builder webClientBuilder,
            @Value("${app.product-service.timeoutSec}") int msDataQServiceTimeoutSec,
            @Value("${url.micro.msdataq}")  String msqServiceUrl ,
            @Value("${url.micro.msdatacq}") String mscqServiceUrl){
	this.webClientBuilder = webClientBuilder;
	this.msDataQServiceTimeoutSec=msDataQServiceTimeoutSec;
	this.mscqServiceUrl=mscqServiceUrl;
	this.msqServiceUrl=msqServiceUrl;
	}
	public String getRequestDataCQ(ParametrosDataDTO parametrosData  ){
		   Mono<String> respuestaMono = getWebClient().post().uri(mscqServiceUrl)
           		.header("jsonDatos", parametrosData.toString())
           		.contentType(MediaType.APPLICATION_JSON)
           		.accept(MediaType.APPLICATION_JSON)
           		.syncBody(parametrosData)		
                   .retrieve().bodyToMono(String.class)
                    .log()
                   .onErrorMap(WebClientResponseException.class, ex -> handleException(ex))
                   .timeout(Duration.ofSeconds(msDataQServiceTimeoutSec));		
          return respuestaMono.block();
	}
	public String getRequestDataQ(ParametrosDataDTO parametrosData  ){
		   Mono<String> respuestaMono = getWebClient().post().uri(msqServiceUrl)
        		.header("jsonDatos", parametrosData.toString())
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON)
        		.syncBody(parametrosData)		
                .retrieve().bodyToMono(String.class)
                 .log()
                .onErrorMap(WebClientResponseException.class, ex -> handleException(ex))
                .timeout(Duration.ofSeconds(msDataQServiceTimeoutSec));		
       return respuestaMono.block();
	}
	public String getRequestDataVariable(String parametrosData, String uri  ){
		   Mono<String> respuestaMono = getWebClient().post().uri(uri)
  		.contentType(MediaType.APPLICATION_JSON_UTF8)
  		.accept(MediaType.APPLICATION_JSON_UTF8)
  		.syncBody(parametrosData)		
          .retrieve().bodyToMono(String.class)
           .log()
          .onErrorMap(WebClientResponseException.class, ex -> handleException(ex))
          .timeout(Duration.ofSeconds(msDataQServiceTimeoutSec));		
		 return respuestaMono.block();
	}
	private WebClient getWebClient() {
        if (webClient == null) {
            webClient = webClientBuilder.build();
        }
        return webClient;
    }
    private Throwable handleException(Throwable ex) {
        if (!(ex instanceof WebClientResponseException)) {
     	   logger.warn("Got a unexpected error: {}, will rethrow it", ex);
            return ex;
        }
        WebClientResponseException wcre = (WebClientResponseException)ex;
        switch (wcre.getStatusCode()) {
        case NOT_FOUND:
        case UNPROCESSABLE_ENTITY :
        default:
     	   logger.warn("Got a unexpected HTTP error: {}, will rethrow it", wcre.getStatusCode());
     	   logger.warn("Error body: {}", wcre.getResponseBodyAsString());
            return ex;
        }
    }
}










