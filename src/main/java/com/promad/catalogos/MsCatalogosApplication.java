package com.promad.catalogos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MsCatalogosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCatalogosApplication.class, args);
	}
	
}