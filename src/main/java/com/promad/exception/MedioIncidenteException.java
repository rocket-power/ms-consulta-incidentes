package com.promad.exception;

public class MedioIncidenteException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public MedioIncidenteException() {
		super();
	}
	
	public MedioIncidenteException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public MedioIncidenteException(Throwable throwable) {
		super(throwable);
	}

}
