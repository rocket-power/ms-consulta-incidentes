package com.promad.service;

import java.util.List;

public class ResultadoDTO {

	private List<?> items;
	private Integer total;
	 private Object items1;
	 private Object items2;
	 private List<?> contador;
	    
	   public Object getItems1() {
	        return items1;
	    }
	    public void setItems1(Object items1) {
	        this.items1 = items1;
	    }
 public Object getItems2() {
	        return items2;
	    }
  
 public void setItems2(Object items2) {
	        this.items2 = items2;
 }

	public ResultadoDTO(List<?> items) {
		super();
		this.items = items;
	}

	public List<?> getItems() {
		return items;
	}

	public void setItems(List<?> items) {
		this.items = items;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	public List<?> getContador() {
		return contador;
	}
	public void setContador(List<?> contador) {
		this.contador = contador;
	}
}
