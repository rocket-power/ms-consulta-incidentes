package com.promad.service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.promad.annotations.Element;

public class AnnotationService {
	
	private static final Logger logger = LoggerFactory.getLogger( AnnotationService.class );
	private static boolean printable = false;
	
	public static void log( String message ) {
		if( printable ) {
			logger.info( message );
		}
	}
	
	public static void setPrintable( boolean value ) {
		printable = value;
	}
	
	public static <T> List<T> getListData(String json, Class<T> clazz) {
		log( "Enter Json: " + json );
		log( "Enter Class: " + clazz.getSimpleName() );
		Map<String, Method> elements = getClassElements( clazz );
		JsonArray items = getItems( json );
		List<T> lista = new ArrayList<>();
		
		if( items != null && items.size() > 0 ) {
			for( JsonElement e: items ) {
				JsonObject item = e.getAsJsonObject();
				T obj = getData( item, elements, clazz );
				lista.add( obj );
			}
		}
		else {
			log( "No se encontraron elementos en la lista" );
		}
		return lista;
	}
	
	private static <T> T getData(JsonObject item, Map<String, Method> elements, Class<T> clazz) {
		try {
			T instance = clazz.newInstance();
			for(  String key: elements.keySet() ) {
				loadData( instance, item.get( key ), elements.get( key ) );
			}
			return instance;
		} 
		catch (Exception e) {
			logger.error("Error", e);
		}
		return null;
	}

	private static void loadData(Object instance, JsonElement jsonElement, Method method) 
	throws IllegalAccessException, InvocationTargetException {
		
		if( instance == null || jsonElement == null || method == null
				|| jsonElement instanceof JsonNull ) {
			return;
		}
		
		Class<?>[] types = method.getParameterTypes();
		if( types == null || types.length != 1 ) {
			return;
		}
		
		if( types[ 0 ].equals( Integer.class ) ) {
			method.invoke( instance, jsonElement.getAsInt() );
		}
		if( types[ 0 ].equals( String.class ) ) {
			method.invoke( instance, jsonElement.getAsString() );
		}
	}

	public static Map<String, Method> getClassElements( Class<?> clazz ) {
		Map<String, Method> elements = new HashMap<>();
		
		Method[] methods = clazz.getMethods();
		Field[] fields = clazz.getDeclaredFields();
		
		for( Field f: fields ) {
			Element e = f.getAnnotation( Element.class );
			if( e != null ) {
				elements.put( e.value().toLowerCase(),  
						getMethod( f.getName(), methods ) );
			}
		}
		return elements;
	}
	
	private static Method getMethod(String name, Method[] methods) {
		String setname = "set" + name;
		for( Method m: methods ) {
			if( m.getName().equalsIgnoreCase( setname ) ) {
				return m;
			}
		}
		return null;
	}

	private static JsonArray getItems( String json ) {
		JsonParser parser = new JsonParser();
		JsonElement data = parser.parse( json );
		JsonObject obj = data.getAsJsonObject();
		return obj.getAsJsonArray( "items" );
	}
	
//	contadores PVO
	public static <T> List<T> getListData1(String json, Class<T> clazz) {
		log( "Enter Json: " + json );
		log( "Enter Class: " + clazz.getSimpleName() );
		Map<String, Method> elements = getClassElements( clazz );
		JsonArray items = getItems1( json );
		List<T> lista = new ArrayList<>();
		
		if( items != null && items.size() > 0 ) {
			for( JsonElement e: items ) {
				JsonObject item = e.getAsJsonObject();
				T obj = getData( item, elements, clazz );
				lista.add( obj );
			}
		}
		else {
			log( "No se encontraron elementos en la lista" );
		}
		return lista;
	}
	
	private static JsonArray getItems1( String json ) {
		JsonParser parser = new JsonParser();
		JsonElement data = parser.parse( json );
		JsonObject obj = data.getAsJsonObject();
		return obj.getAsJsonArray( "contador" );
	}

	
}
