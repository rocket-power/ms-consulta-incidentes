package com.promad.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.promad.catalogos.cliente.ClienteWeb;
import com.promad.catalogos.exception.CatalogoException;
import com.promad.repository.dto.ParametroDTO;
import com.promad.repository.dto.ParametrosDataDTO;

import reactor.core.publisher.Mono;

public abstract class BaseService {

	Logger logger = LoggerFactory.getLogger(BaseService.class);

	@Autowired
	ClienteWeb clienteWeb;
	protected static final String POST = "POST";
	protected static final String PUT = "PUT";
	protected static final String DELETE = "DELETE";
	protected static final String GET = "GET";
	protected static final String INTEGER = "Integer";
	protected static final String STRING = "String";
	protected static final String INESPERADO = "Ocurrio un Error Inesperado ";
	protected static final String NOMBRE_MS = "MS-Catalogos";
	protected String mensaje = " ";

	protected ParametrosDataDTO createParametrosData(String paquete, String storeProcedure) {
		ParametrosDataDTO parametrosDataDTO = new ParametrosDataDTO();
		parametrosDataDTO.setNombrePaquete(paquete);
		parametrosDataDTO.setNombreStoreProcedure(storeProcedure);
		parametrosDataDTO.setNombreMs(BaseService.NOMBRE_MS);
		return parametrosDataDTO;
	}

	protected ParametroDTO getParametro(String nombreParametro, String tipo, Object valor) {
		return new ParametroDTO(nombreParametro, tipo, valor);
	}

	protected String consulta(ParametrosDataDTO parametrosDataDTO) {
		logger.info(new Gson().toJson(parametrosDataDTO));
		try {
			return clienteWeb.getRequestDataQ(parametrosDataDTO);
		} catch (Exception e) {
			logger.error("Error al consultar: \n", e);
			throw new CatalogoException("Ocurrio un Error Inesperado", e);
		}
	}

	protected Mono<String> consulta(ParametrosDataDTO parametrosDataDTO, Class<?> clazz) {
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataQ(parametrosDataDTO);
			ResultadoDTO resultado = new ResultadoDTO(AnnotationService.getListData(json, clazz));
                        resultado.setTotal(getTotal(json));
			json = new Gson().toJson(resultado).toString();
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_Q");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}

	private Integer getTotal(String consulta) {
		JsonArray items = JsonHelper.getAsJsonArray(consulta, "total");
		Integer[] total = JsonHelper.getIntegerValue(items, "total");
		if (total != null && total.length > 0) {
			return total[0];
		}
		return null;
	}

	protected Mono<String> inserta(ParametrosDataDTO parametrosDataDTO) {
		parametrosDataDTO.setTipo(POST);
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_CQ");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}

	protected Mono<String> consultaI(ParametrosDataDTO parametrosDataDTO, Class<?> clazz) {
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataQ(parametrosDataDTO);
			ResultadoDTO resultado = new ResultadoDTO(AnnotationService.getListData(json, clazz));
			json = new Gson().toJson(resultado).toString();
			resultado.setItems1(getItem1(json));
			resultado.setItems2(getItem2(json));
			json = new Gson().toJson(resultado).toString();
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_Q");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}

	private Integer getItem1(String consulta) {
		JsonArray items = JsonHelper.getAsJsonArray(consulta, "items1");
		Integer[] items1 = JsonHelper.getIntegerValue(items, "items1");
		if (items1 != null && items1.length > 0) {
			return items1[0];
		}
		return null;
	}

	private Integer getItem2(String consulta) {
		JsonArray items = JsonHelper.getAsJsonArray(consulta, "items2");
		Integer[] items2 = JsonHelper.getIntegerValue(items, "items2");
		if (items2 != null && items2.length > 0) {
			return items2[0];
		}
		return null;
	}

	protected Mono<String> actualiza(ParametrosDataDTO parametrosDataDTO) {
		parametrosDataDTO.setTipo(PUT);
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_CQ");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}

	protected Mono<String> elimina(ParametrosDataDTO parametrosDataDTO) {
		parametrosDataDTO.setTipo(DELETE);
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_CQ");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}

	protected Mono<String> convertToResultado(List<?> items) {
		ResultadoDTO resultado = new ResultadoDTO(items);
		return Mono.just(new Gson().toJson(resultado));
	}

	protected Mono<String> convertToResult(List<?> items) {
		ResultadoDTO resultado = new ResultadoDTO(items);
		String result = new Gson().toJson(resultado);
		if (result == null) {
			return null;
		} else {
			return Mono.just(result);
		}
	}
/*
	protected Mono<String> insertaPersona(ParametrosDataDTO parametrosDataDTO) {
		parametrosDataDTO.setTipo(POST);
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		String m;
		try {
			json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
			String[] parts = json.split(":");
			String part2 = parts[1];
			parts = part2.split("}");
			String id = parts[0];

			String[] parts1 = id.split(",");
			String id1 = parts1[0];

			String cadena = id1;
			char[] cadenaDiv = cadena.toCharArray();
			String n = "";
			for (int i = 0; i < cadenaDiv.length; i++) {
				if (Character.isDigit(cadenaDiv[i])) {
					n += cadenaDiv[i];
				}
			}
			System.out.println("IMPRIME EL RESULTADO DE N ****** "+n);
			m = n;
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_CQ");
		}
		if (json == null) {
			return null;
		} else {
			System.out.println("IMPRIME EL RESULTADO DE  ****** "+Mono.just(m));
			return Mono.just(m);
		}
	}
*/
	protected String insertaPersona(ParametrosDataDTO parametrosDataDTO) {
		parametrosDataDTO.setTipo(POST);
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataCQ(parametrosDataDTO);
			String[] parts = json.split(":");
			String part2 = parts[1];
			parts = part2.split("}");
			String id = parts[0];

			String[] parts1 = id.split(",");
			String id1 = parts1[0];

			String cadena = id1;
			char[] cadenaDiv = cadena.toCharArray();
			String n = "";
			for (int i = 0; i < cadenaDiv.length; i++) {
				if (Character.isDigit(cadenaDiv[i])) {
					n += cadenaDiv[i];
				}
			}
			System.out.println("IMPRIME EL RESULTADO DE N ****** "+n);

			System.out.println("***************************************"+n+json);
			return n;
		} catch (Exception e) {
			throw new CatalogoException(INESPERADO+"Error al insertar: \n", e);
		}
	}
	
	protected Mono<String> consultaTotales(ParametrosDataDTO parametrosDataDTO, Class<?> clazz) {
		mensaje = new Gson().toJson(parametrosDataDTO);
		logger.info(mensaje);
		String json;
		try {
			json = clienteWeb.getRequestDataQ(parametrosDataDTO);
			ResultadoDTO resultado = new ResultadoDTO(AnnotationService.getListData1(json, clazz));
			json = new Gson().toJson(resultado).toString();
		} catch (Exception e) {
			throw new CatalogoException("Error en comunicacion con servicio MS_Q");
		}
		if (json == null) {
			return null;
		} else {
			return Mono.just(json);
		}
	}
}
