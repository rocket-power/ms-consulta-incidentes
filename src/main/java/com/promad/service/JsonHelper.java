package com.promad.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonHelper {

	public static JsonObject getAsJsonObject( String json, String property ) {

        JsonParser parser = new JsonParser();

        JsonElement element = parser.parse( json );

        if( element.isJsonObject() ) {

               if( property != null ) {

                      return getAsJsonObject( element.getAsJsonObject(), property );

               }

               return element.getAsJsonObject();

        }

        return null;

  }

 

  public static JsonArray getAsJsonArray( String json, String property ) {

        JsonParser parser = new JsonParser();

        JsonElement element = parser.parse( json );

        if( element.isJsonObject() && property != null ) {

               return getAsJsonArray( element.getAsJsonObject(), property );

        }

        if( element.isJsonArray() ) {

               return element.getAsJsonArray();

        }

        return null;

  }

 

  public static JsonArray getAsJsonArray( JsonObject obj, String property ) {

        if( obj != null ) {

               JsonElement element = obj.get( property );

               if( element != null && element.isJsonArray() ) {

                      return obj.getAsJsonArray( property );

               }

        }

        return null;

  }

 

  public static JsonObject getAsJsonObject( JsonObject obj, String property ) {

        if( obj != null ) {

               JsonElement element = obj.get( property );

               if( element != null && element.isJsonObject() ) {

                      return obj.getAsJsonObject( property );

               }

        }

        return null;

  }

 

  public static Integer[] getIntegerValue(JsonArray items, String propertyName) {

        if( items != null ) {

               Integer[] result = new Integer[ items.size() ];

               int index = 0;

               for( JsonElement item: items ) {

                      if( propertyName != null && item.isJsonObject() ) {

                             result[ index++ ] = getIntegerValue( item.getAsJsonObject(), propertyName );

                      }

                      else if( item.isJsonPrimitive() ) {

                             result[ index++ ] = item.getAsInt();

                      }

               }

               return result;

        }

        return null;

  }

 

  public static Integer getIntegerValue(JsonObject obj, String propertyName) {

        if( obj != null ) {
               JsonElement element = obj.get( propertyName );
               if( element != null ) {
                      if( element.isJsonPrimitive() ) {
                             return element.getAsInt();
                      }
               }
        }
        return null;

  }
	
}
