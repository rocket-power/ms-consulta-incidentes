FROM  adoptopenjdk/openjdk8-openj9
RUN apt-get update
RUN apt-get install -y 
COPY build/libs/MS-Consulta-Incidentes-1.0.jar ms-consulta-incidentes.jar
ENTRYPOINT ["java","-jar","ms-consulta-incidentes.jar"]